package de.unibonn.creedo.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.logging.Logger;

import javax.activation.FileTypeMap;
import javax.activation.MimetypesFileTypeMap;

/**
 * <p>
 * Provides a map for resolving MIME types by filename based on the method of
 * Java {@link MimetypesFileTypeMap}. However, since only a very limited set of
 * MIME types is present in the standard table, additional extension/type
 * mappings are read from {@link src/main/resource/mime.types}.
 * </p>
 * 
 * @author Mario Boley
 * 
 * @since 0.3.0
 * 
 * @version 0.3.0
 * 
 */
public class Mimetypes {

	private static final Logger LOGGER = Logger.getLogger(Mimetypes.class.getName());

	private static final FileTypeMap map;

	static {
		FileTypeMap m;
		try {
			String file = Mimetypes.class.getClassLoader().getResource("mime.types").getPath();
			m = new MimetypesFileTypeMap(new FileInputStream(file));
		} catch (NullPointerException | IOException e) {
			LOGGER.warning("Could not load mime.types file (" + e.toString()
					+ "); using standard mime type map (this has unexpected results for many filenames)");
			m = MimetypesFileTypeMap.getDefaultFileTypeMap();
		}
		map = m;
	}

	public static FileTypeMap getFileTypeMap() {
		return map;
	}

}
