package de.unibonn.creedo.utils;

import java.io.IOException;
import java.nio.file.CopyOption;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

/**
 * Simple file visitor that, when invoked with {@link Files#walkFileTree},
 * recursively copies files from the start argument to the targetPath used to
 * construct visitor; creating directories in the target directories as needed.
 * 
 * Source: http://stackoverflow.com/a/10068306/4340425
 * 
 */
public class SimpleCopyFileVisitor extends SimpleFileVisitor<Path> {

	private final Path targetPath;
	private final CopyOption[] options;
	private Path sourcePath = null;

	public SimpleCopyFileVisitor(Path targetPath, CopyOption... options) {
		this.targetPath = targetPath;
		this.options = options;
	}

	@Override
	public FileVisitResult preVisitDirectory(final Path dir,
			final BasicFileAttributes attrs) throws IOException {
		if (sourcePath == null) {
			sourcePath = dir;
		} else {
			Files.createDirectories(targetPath.resolve(sourcePath
					.relativize(dir)));
		}
		return FileVisitResult.CONTINUE;
	}

	@Override
	public FileVisitResult visitFile(final Path file,
			final BasicFileAttributes attrs) throws IOException {
		Files.copy(file, targetPath.resolve(sourcePath.relativize(file)),
				options);
		return FileVisitResult.CONTINUE;
	}

}
