package de.unibonn.creedo.admin.options;

import de.unibonn.creedo.repositories.Repository;
import de.unibonn.creedo.repositories.mybatisimpl.MyBatisRepository;
import de.unibonn.realkd.common.parameter.ParameterContainer;

public class Options {

	public static final Repository<String, ParameterContainer> REPOSITORY = new MyBatisRepository<ParameterContainer>(
			"Options", "admin__options", "admin__options_parameters");

	public static Repository<String, ParameterContainer> getRepository() {
		return REPOSITORY;
	}

}
