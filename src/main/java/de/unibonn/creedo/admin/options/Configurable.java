package de.unibonn.creedo.admin.options;

import de.unibonn.realkd.common.parameter.ParameterContainer;

/**
 * Interface for classes that allow for some global user-provided configuration.
 * The available configuration options are determined by a specific sub-type of
 * {@link ParameterContainer}. Configuration is stored in a special persistent
 * repository accessible via {@link Options}.
 * 
 * @author Mario Boley
 *
 * @param <OptionsClass>
 *            type of configuration object
 * 
 */
public interface Configurable<OptionsClass extends ParameterContainer> {

	public OptionsClass getDefaultOptions();

	@SuppressWarnings("unchecked")
	public default OptionsClass getOptions() {
		String key = this.getOptionsSerializationId();
		if (!Options.REPOSITORY.getAllIds().contains(key)) {
			Options.REPOSITORY.add(key, getDefaultOptions());
		}

		return (OptionsClass) Options.REPOSITORY.get(key);
	}

	public default String getOptionsSerializationId() {
		return this.getClass().getSimpleName();
	}

}
