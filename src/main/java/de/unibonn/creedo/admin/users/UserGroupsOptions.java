package de.unibonn.creedo.admin.users;

import static de.unibonn.realkd.common.parameter.Parameters.stringParameter;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.ParameterContainer;
import de.unibonn.realkd.common.parameter.ValueValidator;
import de.unibonn.realkd.util.Strings;

public class UserGroupsOptions implements ParameterContainer {

	private static class CustomUserGroup implements UserGroup {

		private String id;

		public CustomUserGroup(String id) {
			this.id = id;
		}

		@Override
		public String toString() {
			return id;
		}

		@Override
		public boolean equals(Object other) {
			return (other instanceof CustomUserGroup && ((CustomUserGroup) other).id.equals(this.id));
		}

		@Override
		public int hashCode() {
			return id.hashCode();
		}

	}

	private final Parameter<String> customGroupsParameter;

	public UserGroupsOptions() {
		this.customGroupsParameter = stringParameter("Custom groups",
				"Additional optional groups that users can be assigned to; provide in the form '[group1, group2, ...]'.",
				"[]", new ValueValidator<String>() {
					@Override
					public boolean valid(String value) {
						if (value.length() < 2 || !value.startsWith("[") || !value.endsWith("]")) {
							return false;
						}
						try {
							Strings.jsonArrayToStringList(value);
						} catch (Exception e) {
							return false;
						}
						return true;
					}
				}, "");
	}

	@Override
	public List<Parameter<?>> getTopLevelParameters() {
		return ImmutableList.of(customGroupsParameter);
	}

	public Set<UserGroup> getCustomUserGroups() {
		if (!customGroupsParameter.isValid()) {
			return ImmutableSet.of();
		}
		List<String> list = Strings.jsonArrayToStringList(customGroupsParameter.current());
		return list.stream().map(s -> new CustomUserGroup(s)).collect(Collectors.toSet());
	}

}
