package de.unibonn.creedo.admin.users;

import java.util.Set;

import com.google.common.collect.ImmutableSet;

import de.unibonn.creedo.common.BCrypt;

public class AnonymousUserDetails implements UserDetails {

	@Override
	public Set<UserGroup> groups() {
		return ImmutableSet.of(DefaultUserGroup.ANONYMOUS);
	}

	@Override
	public String hashedPassword() {
		return BCrypt.hashpw("", BCrypt.gensalt(12));
	}

}
