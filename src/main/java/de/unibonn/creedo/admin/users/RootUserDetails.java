package de.unibonn.creedo.admin.users;

import java.util.List;
import java.util.Set;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import de.unibonn.creedo.common.parameters.SecureParameter;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.ParameterContainer;

public class RootUserDetails implements UserDetails, ParameterContainer {

	private final SecureParameter<String> password;

	public RootUserDetails() {
		this.password = new PasswordParameter();
	}

	public RootUserDetails(String clearPassword) {
		this();
		password.setByClearValue(clearPassword);
	}

	@Override
	public List<Parameter<?>> getTopLevelParameters() {
		return ImmutableList.of(password);
	}

	@Override
	public Set<UserGroup> groups() {
		return ImmutableSet.of(DefaultUserGroup.ADMINISTRATOR);
	}

	@Override
	public String hashedPassword() {
		return password.current();
	}

}
