package de.unibonn.creedo.admin.users;

import java.util.Set;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;

import de.unibonn.creedo.admin.options.Configurable;

/**
 * Singleton that defines certain collections of user groups. Can be configured
 * to provide custom user groups.
 * 
 * @author Mario Boley
 * 
 * @since 0.3.0
 * 
 * @version 0.3.1
 * 
 * @see UserGroupsOptions
 *
 */
public class UserGroups implements Configurable<UserGroupsOptions> {

	private static final UserGroups INSTANCE = new UserGroups();

	private UserGroups() {
		;
	}

	public static UserGroups get() {
		return INSTANCE;
	}

	public Set<UserGroup> all() {
		return Sets.union(ImmutableSet.copyOf(DefaultUserGroup.values()), getOptions().getCustomUserGroups());
	}

	/**
	 * Collection of user groups that ordinary users can be part of.
	 */
	public Set<UserGroup> optional() {
		return Sets.union(ImmutableSet.of(DefaultUserGroup.ADMINISTRATOR, DefaultUserGroup.DEVELOPER),
				getOptions().getCustomUserGroups());
	}

	@Override
	public UserGroupsOptions getDefaultOptions() {
		return new UserGroupsOptions();
	}

}
