package de.unibonn.creedo.admin.users;

import java.util.Set;

public interface CreedoUser {

	public String id();

	public Set<UserGroup> groups();

	public String hashedPassword();
	
	public boolean active();

}
