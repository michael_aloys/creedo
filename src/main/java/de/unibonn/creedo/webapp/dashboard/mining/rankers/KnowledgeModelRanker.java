package de.unibonn.creedo.webapp.dashboard.mining.rankers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import de.unibonn.realkd.common.workspace.Workspace;
import de.unibonn.realkd.discoveryprocess.DiscoveryProcess;
import de.unibonn.realkd.knowledgemodeling.UserKnowledgeModel;
import de.unibonn.realkd.knowledgemodeling.training.KnowledgeModelTrainer;
import de.unibonn.realkd.knowledgemodeling.training.KnowledgeModelTrainerFactory;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.QualityMeasureId;

public class KnowledgeModelRanker implements Ranker {
	private KnowledgeModelTrainer knowledgeModelTrainer;
	
	private UserKnowledgeModel userKnowledgeModel;
	
//	public KnowledgeModelRanker(DataTable dataTable,
//			DiscoveryProcess discoveryProcess) {
	public KnowledgeModelRanker(Workspace dataWorkspace,
			DiscoveryProcess discoveryProcess) {
		 // this.knowledgeModelTrainer = new KnowledgeModelTrainer(dataTable, discoveryProcess.getDiscoveryProcessState());
		this.knowledgeModelTrainer = KnowledgeModelTrainerFactory.INSTANCE.createKnowledgeModelTrainer(dataWorkspace, discoveryProcess.getDiscoveryProcessState());
	     this.userKnowledgeModel = this.knowledgeModelTrainer.getKnowledgeModelLearner().getUserKnowledgeModel();
	}

	@Override
	public List<Pattern> rank(List<Pattern> patterns) {
		List<Double> unexpectednessList = new ArrayList<>();
		List<Pattern> results = new ArrayList<>();
		
		for (Pattern p : patterns) {
			unexpectednessList.add(computSubjectiveUnexpectedness(p));
		}
		List<Double> sortedUnexpectednessList = new ArrayList<>(unexpectednessList);
		Collections.sort(sortedUnexpectednessList, Collections.reverseOrder());
		
		for (Double unexpectedness : sortedUnexpectednessList) {
			results.add(patterns.get(unexpectednessList.indexOf(unexpectedness)));
		}
		return results;
	}
	
	private double computSubjectiveUnexpectedness(Pattern p) {
		if (p.hasMeasure(QualityMeasureId.FREQUENCY)) {
			if (userKnowledgeModel.getExpectedMeasurement(p) != 0) {
				return  1. - Math.min(p.value(QualityMeasureId.FREQUENCY)/userKnowledgeModel.getExpectedMeasurement(p), userKnowledgeModel.getExpectedMeasurement(p)/p.value(QualityMeasureId.FREQUENCY));
			}
		} 
		return 1.;
	}
}
