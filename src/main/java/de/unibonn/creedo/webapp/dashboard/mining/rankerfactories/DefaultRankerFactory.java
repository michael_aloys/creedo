package de.unibonn.creedo.webapp.dashboard.mining.rankerfactories;

import de.unibonn.creedo.webapp.dashboard.mining.rankers.DefaultRanker;
import de.unibonn.creedo.webapp.dashboard.mining.rankers.Ranker;
import de.unibonn.realkd.common.workspace.Workspace;
import de.unibonn.realkd.discoveryprocess.DiscoveryProcess;

public class DefaultRankerFactory implements RankerFactory {

	@Override
//	public Ranker getRanker(DataTable dataTable,
//			DiscoveryProcess discoveryProcess) {
	public Ranker getRanker(Workspace dataWorkspace,
			DiscoveryProcess discoveryProcess) {
		return new DefaultRanker();
	}
	
	@Override
	public boolean equals(Object other) {
		return (other instanceof DefaultRankerFactory);
	}
	
	@Override
	public String toString() {
		return "Default ranker";
	}

}
