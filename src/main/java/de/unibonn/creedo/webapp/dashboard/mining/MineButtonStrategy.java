package de.unibonn.creedo.webapp.dashboard.mining;

import java.io.IOException;
import java.util.List;

import com.google.gson.Gson;

import de.unibonn.creedo.webapp.utils.ParameterTransfer;
import de.unibonn.realkd.algorithms.MiningAlgorithm;

/**
 * @author bjacobs
 * 
 */
public enum MineButtonStrategy {

	ONECLICKMINING {

		@Override
		public String getClientJSFileName() {
			return "mineButtonOCM.js";
		}

		@Override
		public void clicked(MiningSystem miningSystem,
				List<MiningAlgorithm> algorithms) {

			MiningAlgorithm metaMiningAlgorithm = algorithms.get(0);

			miningSystem.requestStopOfAlgorithm();
			miningSystem.startNextDiscoveryRoundWithCurrentAlgorithmResult();
			miningSystem.startAlgorithm(metaMiningAlgorithm);

			// return miningDashboard.getCandidatePatternsAsHTML(session);
		}

		@Override
		public String getName() {
			return "One-Click Mining";
		}
	},

	MANUALMINING {
		@Override
		public String getClientJSFileName() {
			return "mineButtonREF.js?v3";
		}

		@Override
		public String getName() {
			return "Manual Mining";
		}

		@Override
		public void clicked(MiningSystem miningSystem,
				List<MiningAlgorithm> algorithms) throws RuntimeException {

			MiningAlgorithmLaunchDialog manualMiningModel = miningSystem
					.getManualMiningModel();

			miningSystem.startAlgorithm(manualMiningModel
					.getSelectedAlgorithm());
			miningSystem.startNextDiscoveryRoundWithCurrentAlgorithmResult();
		}
	};

	public abstract void clicked(MiningSystem miningSystem,
			List<MiningAlgorithm> algorithms) throws IllegalStateException;

	public abstract String getClientJSFileName();

	public abstract String getName();

}
