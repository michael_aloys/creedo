package de.unibonn.creedo.webapp.dashboard.multistep;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import de.unibonn.creedo.Creedo;
import de.unibonn.creedo.ui.core.PreparedFrame;
import de.unibonn.creedo.ui.core.UiRegister.FrameCloser;
import de.unibonn.creedo.ui.core.UiRegister.IdGenerator;
import de.unibonn.creedo.webapp.dashboard.DashboardModel;

/**
 * @author bjacobs
 */
public class MultiStepDashboardModelBuilder implements PreparedFrame {

	private static final String DEFAULT_CLOSE_NAME = "Close";

	private static final String DEFAULT_NEXT_NAME = "Next";

	private final List<PreparedFrame> builders;

	private List<String> nextAndCloseNames;

	public MultiStepDashboardModelBuilder(List<PreparedFrame> builders) {
		this.builders = builders;
		this.nextAndCloseNames = new ArrayList<>();
		for (int i = 0; i < builders.size() - 1; i++) {
			this.nextAndCloseNames.add(DEFAULT_NEXT_NAME);
		}
		this.nextAndCloseNames.add(DEFAULT_CLOSE_NAME);
	}

	/**
	 * Sets the names used by the multistep dashboard for the next button on
	 * each step (respectively for the closed button on the last step).
	 */
	public void setNextAndCloseNames(List<String> names) {
		this.nextAndCloseNames = names;
	}

	@Override
	public DashboardModel build(IdGenerator idGenerator, FrameCloser closer,
			HttpSession session) {
//		fixCloseHandler();
		return new MultiStepDashboardModel(idGenerator.getNextId(), Creedo
				.getCreedoSession(session).getUiRegister(), builders,
				nextAndCloseNames, session, closer);
	}
}
