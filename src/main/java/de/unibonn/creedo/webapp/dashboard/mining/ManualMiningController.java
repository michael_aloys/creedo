package de.unibonn.creedo.webapp.dashboard.mining;

import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;

import de.unibonn.creedo.Creedo;
import de.unibonn.creedo.common.parameters.AbstractTransferableParameterState;
import de.unibonn.creedo.common.parameters.TransferableParameterState;
import de.unibonn.creedo.webapp.utils.ParameterTransfer;
import de.unibonn.realkd.algorithms.AlgorithmCategory;

@Controller
@RequestMapping(value = "/manualmining")
public class ManualMiningController {

	private static final Logger LOGGER = Logger
			.getLogger(ManualMiningController.class.getName());

	public ManualMiningController() {
		;
	}

	/**
	 * @return All algorithm categories in a JSON object
	 * @throws Exception
	 */
	@RequestMapping(value = "/getcategories.json", method = RequestMethod.POST)
	public @ResponseBody List<MiningAlgorithmLaunchDialog.CategoryResultContainer> getCategories(
			HttpSession httpSession,
			@RequestParam("miningLauncherId") int miningSystemId)
			throws Exception {
		LOGGER.info("Algorithm categories requested");

		MiningSystem miningSystem = (MiningSystem) Creedo.getCreedoSession(
				httpSession).getUiComponent(miningSystemId);
		return miningSystem.getManualMiningModel().getCategories();

		// MiningDashboardModel dashboard = getMiningDashboardModel(httpSession,
		// dashboardId);
		// return dashboard.getManualMiningModel().getCategories();
	}

	@RequestMapping(value = "/getcategoryalgorithms.json", method = RequestMethod.POST)
	public @ResponseBody List<MiningAlgorithmLaunchDialog.AlgorithmResultContainer> getCategoryAlgorithms(
			@RequestParam("algorithmCategory") AlgorithmCategory category,
			@RequestParam("miningLauncherId") int miningSystemId,
			HttpSession httpSession) throws IllegalAccessException {
		LOGGER.info("Algorithms of category '" + category.toString()
				+ "' requested");

		MiningSystem miningSystem = (MiningSystem) Creedo.getCreedoSession(
				httpSession).getUiComponent(miningSystemId);

		return miningSystem.getManualMiningModel().getCategoryAlgorithms(
				category);
		// MiningDashboardModel dashboard = getMiningDashboardModel(httpSession,
		// dashboardId);
		//
		// return
		// dashboard.getManualMiningModel().getCategoryAlgorithms(category);
	}

	/**
	 * Gets called when an algorithm is newly selected from the RefControlBox
	 *
	 * @param algorithmName
	 *            String identifier of algorithm
	 * @return List of parameters for this algorithm with default values set
	 */
	@RequestMapping(value = "/getalgorithmparameters.json", method = RequestMethod.POST)
	public @ResponseBody List<AbstractTransferableParameterState> getAlgorithmParams(
			@RequestParam("algorithmName") String algorithmName,
			HttpSession httpSession,
			@RequestParam("miningSystemId") int miningSystemId)
			throws IllegalAccessException {

		MiningSystem miningSystem = (MiningSystem) Creedo.getCreedoSession(
				httpSession).getUiComponent(miningSystemId);

		miningSystem.getManualMiningModel().setCurrentAlgorithmByName(
				algorithmName);
		return miningSystem.getManualMiningModel()
				.getCurrentAlgorithmParameters();
	}

	// @RequestMapping(value = "/applyandgetalgorithmparameters.json", method =
	// RequestMethod.POST)
	// public @ResponseBody List<JsonTransferParameterContainer>
	// applyAndGetAlgorithmParameters(
	// HttpSession httpSession, HttpServletRequest request)
	// throws IOException, IllegalAccessException {
	//
	// ParameterTransfer execParamSetup = new Gson().fromJson(
	// request.getReader(), ParameterTransfer.class);
	//
	// MiningDashboardModel dashboard = getMiningDashboardModel(httpSession,
	// execParamSetup.getDashboardId());
	//
	// dashboard.getManualMiningModel().applyAlgorithmSettings(execParamSetup);
	//
	// return dashboard.getManualMiningModel().getCurrentAlgorithmParameters();
	// }

	// @RequestMapping(value = "/applyandgetalgorithmparameters.json", method =
	// RequestMethod.POST)
	// public @ResponseBody List<AbstractTransferableParameterState>
	// applyAndGetAlgorithmParameters(
	// HttpSession httpSession,
	// @RequestParam("miningSystemId") int miningSystemId,
	// @RequestParam("parameterPackageAsString") String paramPackage)
	@RequestMapping(value = "/applyandgetalgorithmparameters.json", method = RequestMethod.POST)
	public @ResponseBody List<AbstractTransferableParameterState> applyAndGetAlgorithmParameters(
			HttpSession httpSession,
			@RequestParam("miningSystemId") int miningSystemId,
			@RequestParam("parameterName") String paramName,
			@RequestParam("parameterValue") String paramValue)

	throws IOException, IllegalAccessException {

//		ParameterTransfer execParamSetup = new Gson().fromJson(paramPackage,
//				ParameterTransfer.class);

		MiningSystem miningSystem = (MiningSystem) Creedo.getCreedoSession(
				httpSession).getUiComponent(miningSystemId);

		miningSystem.getManualMiningModel().applyAlgorithmSettings(paramName,
				paramValue);
		return miningSystem.getManualMiningModel()
				.getCurrentAlgorithmParameters();
	}

}
