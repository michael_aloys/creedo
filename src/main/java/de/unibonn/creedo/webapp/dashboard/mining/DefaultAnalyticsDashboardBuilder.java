package de.unibonn.creedo.webapp.dashboard.mining;

import static de.unibonn.creedo.DynamicUrlController.frameUrl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;

import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import de.unibonn.creedo.Creedo;
import de.unibonn.creedo.admin.users.DefaultUserGroup;
import de.unibonn.creedo.boot.DefaultPageFrameBuilder;
import de.unibonn.creedo.ui.IntrospectionPage;
import de.unibonn.creedo.ui.core.Action;
import de.unibonn.creedo.ui.core.ActionLink;
import de.unibonn.creedo.ui.core.DefaultFrame;
import de.unibonn.creedo.ui.core.Frame;
import de.unibonn.creedo.ui.core.PreparedFrame;
import de.unibonn.creedo.ui.core.PageContainer;
import de.unibonn.creedo.ui.core.UiComponent;
import de.unibonn.creedo.ui.core.UiRegister.FrameCloser;
import de.unibonn.creedo.ui.core.UiRegister.IdGenerator;
import de.unibonn.creedo.ui.mining.AnalyticsDashboard;
import de.unibonn.creedo.ui.mining.AnalyticsDashboardDependentComponentBuilder;
import de.unibonn.creedo.ui.standard.CloseAction;
import de.unibonn.creedo.ui.standard.Navbar;
import de.unibonn.creedo.webapp.handler.TimeoutHandler;
import de.unibonn.creedo.webapp.viewmodels.DeveloperViewModel;
import de.unibonn.realkd.common.workspace.Workspace;

/**
 * <p>
 * Constructs a frame containing an analytics dashboard with a configurable
 * mining system based on a configurable realKD workspace.
 * </p>
 * 
 * @author Björn Jacobs
 * @author Mario Boley
 * 
 * @since 0.1.1
 * 
 * @version 0.4.0
 *
 */
public class DefaultAnalyticsDashboardBuilder implements PreparedFrame {

	private final Supplier<Workspace> workspaceSupplier;

	private final MiningSystemBuilder<?> systemBuilder;

	private List<AnalyticsDashboardDependentComponentBuilder> dashboardDependentBuilders;

	private final Optional<Runnable> onOpenAction;

	public DefaultAnalyticsDashboardBuilder(Supplier<Workspace> workspaceSupplier,
			MiningSystemBuilder<?> systemBuilder) {
		this(workspaceSupplier, systemBuilder, Optional.empty());
	}

	public DefaultAnalyticsDashboardBuilder(Supplier<Workspace> workspaceSupplier, MiningSystemBuilder<?> systemBuilder,
			Optional<Runnable> onOpenAction) {
		this.workspaceSupplier = workspaceSupplier;
		this.systemBuilder = systemBuilder;
		this.dashboardDependentBuilders = new ArrayList<>();
		this.onOpenAction = onOpenAction;
	}

	@Override
	public void notifyAboutToOpen() {
		onOpenAction.ifPresent(a -> {
			a.run();
		});
	}

	public DefaultAnalyticsDashboardBuilder addDashboardDependentComponentBuilder(
			AnalyticsDashboardDependentComponentBuilder componentBuilder) {
		this.dashboardDependentBuilders.add(componentBuilder);
		return this;
	}

	@Override
	public Frame build(IdGenerator idGenerator, FrameCloser closer, HttpSession session) {

		int frameId = idGenerator.getNextId();
		List<UiComponent> components = new ArrayList<>();

		DeveloperViewModel developerViewModel = new DeveloperViewModel();
		Workspace workspace = workspaceSupplier.get();
		MiningSystem miningSystem = systemBuilder.build(idGenerator, developerViewModel, workspace);
		AnalyticsDashboard analyticsDashboard = new AnalyticsDashboard(idGenerator.getNextId(), session, workspace,
				miningSystem);

		Frame helpFrame = new DefaultPageFrameBuilder().optionalNavbarContent(miningSystem.helpPages())
				.build(Creedo.getCreedoSession(session));

		Navbar navbar = buildNavbar(idGenerator, session, frameId, developerViewModel, helpFrame, closer);

		TimeoutHandler timeoutHandler = new TimeoutHandler(idGenerator.getNextId(), closer);

		components.add(navbar);
		components.add(analyticsDashboard);
		components.add(timeoutHandler);

		dashboardDependentBuilders.forEach(b -> {
			components.add(b.build(idGenerator, closer, analyticsDashboard));
		});

		return new DefaultFrame(frameId, components, "dashboard");
	}

	private Navbar buildNavbar(IdGenerator idGenerator, HttpSession session, int frameId,
			DeveloperViewModel developerViewModel, Frame helpFrame, FrameCloser closer) {
		List<ActionLink> navbarLinks = new ArrayList<>();
		navbarLinks.add(new ActionLink(new Action() {

			private final int id = idGenerator.getNextId();

			@Override
			public String getReferenceName() {
				return "Help";
			}

			@Override
			public ResponseEntity<String> activate(String... params) {
				return new ResponseEntity<String>(frameUrl(helpFrame.getId()),
						HttpStatus.OK);
			}

			@Override
			public ClientWindowEffect getEffect() {
				return Action.ClientWindowEffect.POPUP;
			}

			@Override
			public int getId() {
				return id;
			}
		}));

		if (Creedo.getCreedoSession(session).getUser().groups().contains(DefaultUserGroup.DEVELOPER)) {
			navbarLinks.add(new ActionLink(new Action() {

				private final int id = idGenerator.getNextId();

				@Override
				public String getReferenceName() {
					return "Introspection";
				}

				@Override
				public ResponseEntity<String> activate(String... params) {
					List<Action> empty = Arrays.asList();
					PageContainer pageContainer = new PageContainer(idGenerator.getNextId());
					Frame introFrame = Creedo.getCreedoSession(session).getUiRegister()
							.createDefaultFrame(pageContainer, empty, empty);
					pageContainer.loadPage(new IntrospectionPage(developerViewModel));
					return new ResponseEntity<String>(frameUrl(introFrame.getId()), HttpStatus.OK);
				}

				@Override
				public ClientWindowEffect getEffect() {
					return Action.ClientWindowEffect.POPUP;
				}

				@Override
				public int getId() {
					return id;
				}
			}));
		}

		navbarLinks.add(new ActionLink(new CloseAction(idGenerator.getNextId(), closer)));

		Navbar navbar = new Navbar(idGenerator.getNextId(), navbarLinks);
		return navbar;
	}

}
