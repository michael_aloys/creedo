package de.unibonn.creedo.webapp.dashboard.mining.rankerfactories;

import de.unibonn.creedo.webapp.dashboard.mining.rankers.Ranker;
import de.unibonn.realkd.common.workspace.Workspace;
import de.unibonn.realkd.discoveryprocess.DiscoveryProcess;

public interface RankerFactory {
//	public Ranker getRanker(DataTable dataTable,
//							DiscoveryProcess discoveryProcess);
	public Ranker getRanker(Workspace dataWorkspace,
			DiscoveryProcess discoveryProcess);
}
