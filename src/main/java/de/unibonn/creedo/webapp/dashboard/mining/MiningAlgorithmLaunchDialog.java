package de.unibonn.creedo.webapp.dashboard.mining;

import static com.google.common.base.Preconditions.checkArgument;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;

import com.google.common.collect.ImmutableList;

import de.unibonn.creedo.common.parameters.AbstractTransferableParameterState;
import de.unibonn.creedo.ui.core.Action;
import de.unibonn.creedo.ui.core.ActionProvider;
import de.unibonn.creedo.ui.core.UiComponent;
import de.unibonn.creedo.ui.core.UiRegister.IdGenerator;
import de.unibonn.creedo.webapp.utils.ParameterToJsonConverter;
import de.unibonn.realkd.algorithms.AlgorithmCategory;
import de.unibonn.realkd.algorithms.MiningAlgorithm;

/**
 * Dialog for launching mining algorithms.
 * 
 * @author Björn Jacobs
 * @author Mario Boley
 * 
 * @since 0.1.0
 * 
 * @version 0.2.1
 * 
 */
public class MiningAlgorithmLaunchDialog implements UiComponent, ActionProvider {

	private static final Logger LOGGER = Logger
			.getLogger(MiningAlgorithmLaunchDialog.class.getName());

	private class UpdateAlgorithmParameterAction implements Action {

		private final int id;

		public UpdateAlgorithmParameterAction(int id) {
			this.id = id;
		}

		@Override
		public String getReferenceName() {
			return "Update parameter";
		}

		@Override
		public ResponseEntity<String> activate(String... params) {
			checkArgument(params.length == 2);
			checkArgument(params[0] != null);

			if (currentSelectedMiningAlgorithm == null) {
				return new ResponseEntity<>(HttpStatus.PRECONDITION_FAILED);
			}

			currentSelectedMiningAlgorithm.findParameterByName(params[0])
					.setByString(params[1]);
			return new ResponseEntity<>(HttpStatus.OK);
		}

		@Override
		public ClientWindowEffect getEffect() {
			return ClientWindowEffect.NONE;
		}

		@Override
		public int getId() {
			return id;
		}

	}

	private final int id;
	private final List<MiningAlgorithm> algorithms;
	private final List<AlgorithmCategory> algorithmCategories;

	private MiningAlgorithm currentSelectedMiningAlgorithm;
	private final Action updateParameterAction;

	public MiningAlgorithmLaunchDialog(IdGenerator idGenerator,
			List<MiningAlgorithm> algorithms) {
		this.id = idGenerator.getNextId();
		this.algorithms = algorithms;
		this.algorithmCategories = compileAlgorithmCategories();
		this.updateParameterAction = new UpdateAlgorithmParameterAction(
				idGenerator.getNextId());
	}

	public List<CategoryResultContainer> getCategories() {
		// Create result list
		List<CategoryResultContainer> result = new ArrayList<>();
		for (AlgorithmCategory cat : algorithmCategories) {
			String fullName = cat.getName();
			CategoryResultContainer newResult = new CategoryResultContainer(
					fullName, cat);
			result.add(newResult);
		}

		return result;
	}

	public List<AlgorithmResultContainer> getCategoryAlgorithms(
			AlgorithmCategory category) {
		List<MiningAlgorithm> tmpResults = getAlgorithmsByCategory(category);

		List<AlgorithmResultContainer> results = new ArrayList<>();
		for (MiningAlgorithm ad : tmpResults) {
			results.add(new AlgorithmResultContainer(ad.getName(), ad
					.getDescription()));
		}

		return results;
	}

	public void setCurrentAlgorithmByName(String algorithmName) {
		MiningAlgorithm algorithm = getMiningAlgorithmByName(algorithmName);
		LOGGER.info("Set current mining algorithm to '" + algorithm.getName()
				+ "'");

		currentSelectedMiningAlgorithm = algorithm;
	}

	public List<AbstractTransferableParameterState> getCurrentAlgorithmParameters() {
		return ParameterToJsonConverter
				.convertRecursively(currentSelectedMiningAlgorithm
						.getTopLevelParameters());
	}

	public MiningAlgorithm getMiningAlgorithmByName(String name) {
		for (MiningAlgorithm algo : algorithms) {
			if (algo.getName().equals(name)) {
				return algo;
			}
		}
		throw new IllegalArgumentException("No algorithm with name " + name
				+ " found in the set of documented algorithms.");
	}

	public List<AlgorithmCategory> getAlgorithmCategories() {
		return algorithmCategories;
	}

	/**
	 * @return list of all available algorithm categories (in deterministic
	 *         order given by category enum)
	 */
	private List<AlgorithmCategory> compileAlgorithmCategories() {
		//
		List<AlgorithmCategory> categories = new ArrayList<>();

		for (AlgorithmCategory category : AlgorithmCategory.values()) {
			for (MiningAlgorithm algorithm : algorithms) {
				if (algorithm.getCategory().equals(category)) {
					categories.add(category);
					break;
				}
			}
		}
		return categories;
	}

	public List<MiningAlgorithm> getAlgorithmsByCategory(AlgorithmCategory cat) {
		List<MiningAlgorithm> result = new ArrayList<>();

		// Filter by category
		for (MiningAlgorithm algo : algorithms) {
			if (algo.getCategory() == cat) {
				result.add(algo);
			}
		}
		return result;
	}

	public void applyAlgorithmSettings(String parameterName, String value) {
		currentSelectedMiningAlgorithm.findParameterByName(parameterName)
				.setByString(value);
	}

	public MiningAlgorithm getSelectedAlgorithm() {
		return currentSelectedMiningAlgorithm;
	}

	/*
	 * ResultContainer classes are used for the selection of certain properties
	 * that should be displayed to the client application through the
	 * JSON-interface. The JSON-parser only regards the public fields of
	 * classes.
	 */
	static class CategoryResultContainer {
		public String description;
		public AlgorithmCategory category;

		CategoryResultContainer(String description, AlgorithmCategory category) {
			this.description = description;
			this.category = category;
		}
	}

	static class AlgorithmResultContainer {
		public String name;
		public String description;

		AlgorithmResultContainer(String name, String description) {
			this.name = name;
			this.description = description;
		}
	}

	@Override
	public int getId() {
		return id;
	}

	@Override
	public String getView() {
		throw new UnsupportedOperationException();
	}

	@Override
	public Model getModel() {
		throw new UnsupportedOperationException();
	}

	@Override
	public Collection<Integer> getActionIds() {
		return ImmutableList.of(updateParameterAction.getId());
	}

	@Override
	public ResponseEntity<String> performAction(int id, String... params) {
		return updateParameterAction.activate(params);
	}

}
