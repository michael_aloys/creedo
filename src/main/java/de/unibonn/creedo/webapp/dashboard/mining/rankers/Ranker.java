package de.unibonn.creedo.webapp.dashboard.mining.rankers;

import java.util.List;

import de.unibonn.realkd.patterns.Pattern;

public interface Ranker {
	public List<Pattern> rank(List<Pattern> patterns);
}
