package de.unibonn.creedo.webapp.dashboard.mining;

import java.util.ArrayList;
import java.util.List;

import com.google.common.collect.ImmutableList;

import de.unibonn.creedo.ui.core.UiRegister.IdGenerator;
import de.unibonn.creedo.webapp.viewmodels.DeveloperViewModel;
import de.unibonn.realkd.algorithms.MiningAlgorithm;
import de.unibonn.realkd.algorithms.MiningAlgorithmFactory;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.Parameters;
import de.unibonn.realkd.common.workspace.Workspace;
import de.unibonn.realkd.discoveryprocess.DiscoveryProcess;

/**
 * Builder creating mining system instance configured as manual mining system
 * with a dialog for algorithm selection and parameter setting.
 *
 * @author Bjoern Jacobs
 * 
 * @since 0.1.0
 * 
 * @version 0.4.0
 * 
 */
public class ManualMiningSystemBuilder extends MiningSystemBuilder<ManualMiningSystemBuilder> {

	private final Parameter<Boolean> showLaunchDialog;

	public ManualMiningSystemBuilder() {
		super();
		showLaunchDialog = Parameters.rangeEnumerableParameter("Show dialog",
				"Determines whether launch dialog is shown before execution of mining algorithm ('false' only possible with single consistently initialized algorithm).",
				Boolean.class, () -> algorithms.current().size() == 1
						? ImmutableList.of(new Boolean(true), new Boolean(false)) : ImmutableList.of(new Boolean(true)),
				algorithms);
		parameterContainer.addParameter(showLaunchDialog);
	}

	@Override
	protected MiningSystem concreteBuild(IdGenerator idGenerator, DeveloperViewModel developerViewModel,
			Workspace dataWorkspace) {

		List<MiningAlgorithm> algorithms = new ArrayList<>();
		for (MiningAlgorithmFactory factory : getAlgorithmFactories()) {
			algorithms.add(factory.create(dataWorkspace));
		}

		DiscoveryProcess discoveryProcess = new DiscoveryProcess();

		// return new MiningSystem(idGenerator, algorithms, discoveryProcess,
		// MineButtonStrategy.MANUALMINING, super.getRankerFactory()
		// .getRanker(dataWorkspace.getAllDatatables().get(0),
		// discoveryProcess),
		// showLaunchDialog.getCurrentValue());

		return new MiningSystem(idGenerator, algorithms, discoveryProcess, MineButtonStrategy.MANUALMINING,
				super.getRankerFactory().getRanker(dataWorkspace, discoveryProcess), showLaunchDialog.current(),
				helpPages.current(), defaultDataView.current(), usePCAasDefaultVisualization.current());

	}

	@Override
	public String toString() {
		return "Manual mining system";
	}

}
