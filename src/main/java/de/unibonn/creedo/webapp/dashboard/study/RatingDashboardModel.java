package de.unibonn.creedo.webapp.dashboard.study;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.validation.support.BindingAwareModelMap;
import org.springframework.web.servlet.ModelAndView;

import com.google.common.collect.ImmutableList;

import de.unibonn.creedo.Creedo;
import de.unibonn.creedo.admin.options.Configurable;
import de.unibonn.creedo.studies.assignmentschemes.EvaluationAssignment;
import de.unibonn.creedo.studies.state.StudyState;
import de.unibonn.creedo.studies.ui.EvaluationSessionTracker;
import de.unibonn.creedo.ui.core.ActionProvider;
import de.unibonn.creedo.ui.core.DefaultFrame;
import de.unibonn.creedo.ui.core.DefaultFrameOptions;
import de.unibonn.creedo.ui.core.UiComponent;
import de.unibonn.creedo.ui.core.UiRegister.FrameCloser;
import de.unibonn.creedo.ui.core.UiRegister.IdGenerator;
import de.unibonn.creedo.ui.mining.DataViewContainer;
import de.unibonn.creedo.ui.mining.OpenDetailedPatternViewAction;
import de.unibonn.creedo.ui.mining.PatternSupportSetProvider;
import de.unibonn.creedo.ui.standard.Navbar;
import de.unibonn.creedo.webapp.dashboard.DashboardModel;
import de.unibonn.creedo.webapp.dashboard.patterncontainer.WebPattern;
import de.unibonn.creedo.webapp.handler.TimeoutHandler;
import de.unibonn.creedo.webapp.patternviews.RatingBoardPatternHTMLGenerator;
import de.unibonn.creedo.webapp.studies.rating.RatingMetric;
import de.unibonn.realkd.common.workspace.Workspace;

/**
 * Represents all state of a dashboard for rating patterns.
 * 
 * @author bjacobs, mboley
 */
public class RatingDashboardModel implements DashboardModel, ActionProvider, Configurable<DefaultFrameOptions> {

	private final ModelAndView view;
	private final int id;

	// private final DataWorkspace dataWorkspace;
	private final HttpSession httpSession;
	private final Map<Integer, WebPattern> patterns;
	private final RatingSystem ratingSystem;
	private final BindingAwareModelMap model;

	private final DataViewContainer dataViewContainer;
	private final Navbar navbar;
	private final PatternSupportSetProvider supportSetProvider;
	private final EvaluationSessionTracker evaluationSessionTracker;
	private final TimeoutHandler timeoutHandler;

	private final OpenDetailedPatternViewAction openDetailedPatternViewAction;

	RatingDashboardModel(IdGenerator idGenerator, List<RatingMetric> metrics, HttpSession session,
			Workspace dataWorkspace, StudyState studyState, EvaluationAssignment assignment, FrameCloser closer) {

		this.httpSession = session;
		this.id = idGenerator.getNextId();
		// this.id = session.getId() + System.currentTimeMillis();

		this.patterns = new LinkedHashMap<>();
		this.ratingSystem = new RatingSystem(metrics);

		/*
		 * navbar empty since it should never be relevant (this frame is only
		 * displayed as a nesteds frame in a multi step frame which generates
		 * its own navbar)
		 */
		this.navbar = new Navbar(Creedo.getCreedoSession(session).getUiRegister().getNextId(), Arrays.asList());

		this.dataViewContainer = new DataViewContainer(
				Creedo.getCreedoSession(session).getUiRegister().getIdGenerator(), session, dataWorkspace, true);

		this.supportSetProvider = new PatternSupportSetProvider(
				Creedo.getCreedoSession(httpSession).getUiRegister().getNextId(), patterns);

		this.openDetailedPatternViewAction = new OpenDetailedPatternViewAction(
				Creedo.getCreedoSession(httpSession).getUiRegister().getNextId(), patterns, httpSession);

		for (Integer resultId : assignment.getResultIds()) {
			WebPattern webPattern = new WebPattern(resultId,
					// result.getPatternBuilder().build(dataTable.getPropositionStore()),
					studyState.getResultRepository().get(resultId).getPatternBuilder().build(dataWorkspace),
					RatingBoardPatternHTMLGenerator.INSTANCE, supportSetProvider.getId(), getId(),
					openDetailedPatternViewAction.getId());

			patterns.put(resultId, webPattern);
		}

		List<String> resultPatternsHtml = new ArrayList<>();
		for (WebPattern pattern : patterns.values()) {
			resultPatternsHtml.add(pattern.getHtml(session));
		}
		// this.metrics = metrics;

		initRatings();

		this.evaluationSessionTracker = new EvaluationSessionTracker(idGenerator, ratingSystem, assignment, closer);

		this.timeoutHandler = new TimeoutHandler(idGenerator.getNextId(), closer);

		model = new BindingAwareModelMap();

		model.addAttribute("frameId", getId());
		model.addAllAttributes(navbar.getModel().asMap());

		model.addAttribute("resultPatterns", resultPatternsHtml);
		model.addAttribute(DashboardModel.DASHBOARD_ID_ATTRIBUTENAME, id); // Legacy
																			// -
																			// to
																			// be
																			// removed!
		model.addAttribute("ratingMetrics", metrics);

		model.addAllAttributes(dataViewContainer.getModel().asMap());
		model.addAllAttributes(timeoutHandler.getModel().asMap());

		// String datasetName =
		// dataWorkspace.getAllDatatables().get(0).getName();
		model.addAttribute("title", "Result Evaluation");
		model.addAttribute("controllerName", "dashboard");
		model.addAttribute("applicationTitlePrefix", getOptions().titlePrefix());
		model.addAttribute("customStylesheet", getOptions().getCustomCssFileName().orElse(null));

		model.addAttribute("components", ImmutableList.of(navbar, new UiComponent() {

			private final int id = idGenerator.getNextId();

			private final Model model = new BindingAwareModelMap();

			@Override
			public String getView() {
				return "ratingDashboard.jsp";
			}

			@Override
			public Model getModel() {
				return model;
			}

			@Override
			public int getId() {
				return id;
			}
		}, timeoutHandler));
		model.addAttribute("scriptFilenames", getAllScriptFilenames());

		this.view = new ModelAndView(getView(), model);
	}

	private void initRatings() {
		for (WebPattern webPattern : patterns.values()) {
			ratingSystem.init(webPattern.getId());
		}
	}

	@Override
	public void tearDown() {
		;
	}

	@Override
	public int getId() {
		return id;
	}

	@Override
	public ModelAndView getModelAndView() {
		return view;
	}

	public void updateRating(int patternId, String metricName, int optionId) {
		ratingSystem.updateRating(patternId, metricName, optionId);
	}

	public List<Integer> getPatternRating(int patternId) {
		return ratingSystem.getPatternRatings(patternId);
	}

	@Override
	public String getView() {
		return "static/contentPageFrame";
	}

	@Override
	public Model getModel() {
		return model;
	}

	@Override
	public List<UiComponent> getComponents() {
		return Arrays.asList((UiComponent) navbar, (UiComponent) dataViewContainer, supportSetProvider,
				evaluationSessionTracker, timeoutHandler);
	}

	@Override
	public Collection<Integer> getActionIds() {
		return ImmutableList.of(openDetailedPatternViewAction.getId());
	}

	@Override
	public ResponseEntity<String> performAction(int id, String... params) {
		if (id != openDetailedPatternViewAction.getId()) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return openDetailedPatternViewAction.activate(params);
	}

	/**
	 * Override in order to use options of DefaultFrame
	 * 
	 */
	@Override
	public String getOptionsSerializationId() {
		return DefaultFrame.class.getSimpleName();
	}

	@Override
	public DefaultFrameOptions getDefaultOptions() {
		return new DefaultFrameOptions();
	}

}
