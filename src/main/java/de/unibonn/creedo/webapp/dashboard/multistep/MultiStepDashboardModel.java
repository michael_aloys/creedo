package de.unibonn.creedo.webapp.dashboard.multistep;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.servlet.ModelAndView;

import com.google.common.collect.ImmutableList;

import de.unibonn.creedo.Creedo;
import de.unibonn.creedo.ui.core.Action;
import de.unibonn.creedo.ui.core.ActionLink;
import de.unibonn.creedo.ui.core.Frame;
import de.unibonn.creedo.ui.core.PreparedFrame;
import de.unibonn.creedo.ui.core.UiComponent;
import de.unibonn.creedo.ui.core.UiRegister;
import de.unibonn.creedo.ui.core.UiRegister.FrameCloser;
import de.unibonn.creedo.ui.standard.Navbar;
import de.unibonn.creedo.webapp.dashboard.DashboardModel;

/**
 * Dashboard that aggregates a number of sub-dashboard through which the user
 * can progress linearly.
 * 
 * @author bjacobs, mboley
 * 
 */
public class MultiStepDashboardModel implements DashboardModel {
	private final List<PreparedFrame> builders;
	private final List<String> nextAndCloseNames;
	private final HttpSession session;
	private final int id;
	private final FrameCloser myCloser;
	private int step = -1;
	private Frame current;
	private final UiRegister uiRegister;
	private Navbar navbar;

	/**
	 * 
	 * @param builders
	 *            list of dashboard builders for each step
	 * @param nextAndCloseNames
	 *            list of names to be used for next button in each step
	 *            (respectively for close button on last step)
	 * @param session
	 *            httpSession in which dashboard is to be displayed
	 */
	public MultiStepDashboardModel(int id, UiRegister frameFactory,
			List<PreparedFrame> builders, List<String> nextAndCloseNames,
			HttpSession session, FrameCloser closer) {
		if (builders == null) {
			throw new IllegalArgumentException("Builders must not be null");
		}
		if (nextAndCloseNames == null) {
			throw new IllegalArgumentException("Names must not be null");
		}
		if (builders.size() != nextAndCloseNames.size()) {
			throw new IllegalArgumentException(
					"Number of builders must match number of button names");
		}
		this.builders = builders;
		this.nextAndCloseNames = nextAndCloseNames;
		this.session = session;
		this.id = id;
		// this.id = session.getId() + System.currentTimeMillis();
		this.uiRegister = frameFactory;
		this.myCloser = closer;

		this.loadNextStep();
		// current = frameFactory.createFrameFromBuilder(builders.get(step));
		// initNavbar();
	}

	private void initNavbar() {
		if (step < builders.size() - 1) {
			this.navbar = new Navbar(uiRegister.getNextId(),
					Arrays.asList(new ActionLink(new Action() {

						private final int id = Creedo.getCreedoSession(session)
								.getUiRegister().getNextId();

						@Override
						public String getReferenceName() {
							return nextAndCloseNames.get(step);
						}

						@Override
						public ResponseEntity<String> activate(String... params) {
							uiRegister.tearDownComponent(current.getId());
							loadNextStep();
							// step++;
							// if (step == builders.size()) {
							// throw new IllegalStateException(
							// "No next step available");
							// }
							// current = builders.get(step).build(
							// uiRegister.getIdGenerator(), myCloser,
							// session);
							// uiRegister.registerUiComponent(current);

							// initNavbar();
							return new ResponseEntity<String>(HttpStatus.OK);
						}

						@Override
						public ClientWindowEffect getEffect() {
							return ClientWindowEffect.REFRESH;
						}

						@Override
						public int getId() {
							return id;
						}
					})));
		} else {
			List<ActionLink> actionLinks = new ArrayList<>();
			current.getComponents().forEach(c -> {
				c.getExternalActions().forEach(a -> {
					actionLinks.add(new ActionLink(a));
				});
			});

			this.navbar = new Navbar(this.uiRegister.getNextId(),
					ImmutableList.copyOf(actionLinks));
		}
		this.uiRegister.registerUiComponent(navbar);
	}

	private void loadNextStep() {
		step++;
		if (step == builders.size()) {
			throw new IllegalStateException("No next step available");
		}

		// current = uiRegister
		// .createFrameFromBuilder(builders.get(step));
		current = builders.get(step).build(uiRegister.getIdGenerator(),
				myCloser, session);
		uiRegister.registerUiComponent(current);
		initNavbar();
	}

	@Override
	public void tearDown() {
		// current.tearDown();
		uiRegister.tearDownComponent(current.getId());
	}

	@Override
	public int getId() {
		return id;
	}

	@Override
	public ModelAndView getModelAndView() {
		return new ModelAndView(getView(), getModel().asMap());
	}

	// public void nextStep() {
	// current.tearDown();
	//
	// step++;
	// if (step == builders.size()) {
	// throw new IllegalStateException("No next step available");
	// }
	//
	// // current = builders.get(step).build(session);
	// current = uiRegister.createFrameFromBuilder(builders.get(step));
	// initNavbar();
	// }

	// public Frame getCurrentDashboard() {
	// return current;
	// }

	@Override
	public String getView() {
		return current.getView();
	}

	@Override
	public Model getModel() {
		Model model = current.getModel();
		model.addAttribute("frameId", getId());
		// here navbar attributes of contained model are apparently overwritten
		// in order to have clean navbar as defined in this class
		model.addAllAttributes(navbar.getModel().asMap());
		return model;
	}

	@Override
	public List<UiComponent> getComponents() {
		return Arrays.asList(navbar);
	}

}
