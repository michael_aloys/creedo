package de.unibonn.creedo.webapp.dashboard.mining;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Sets;

import de.unibonn.creedo.ApplicationRepositories;
import de.unibonn.creedo.ui.core.UiRegister.IdGenerator;
import de.unibonn.creedo.webapp.dashboard.mining.MiningSystem.DefaultDataView;
import de.unibonn.creedo.webapp.dashboard.mining.rankerfactories.DefaultRankerFactory;
import de.unibonn.creedo.webapp.dashboard.mining.rankerfactories.KnowledgeModelRankerFactory;
import de.unibonn.creedo.webapp.dashboard.mining.rankerfactories.RankerFactory;
import de.unibonn.creedo.webapp.viewmodels.DeveloperViewModel;
import de.unibonn.realkd.algorithms.AlgorithmFactory;
import de.unibonn.realkd.algorithms.MiningAlgorithmFactory;
import de.unibonn.realkd.common.parameter.DefaultParameterContainer;
import de.unibonn.realkd.common.parameter.DefaultRangeEnumerableParameter.RangeComputer;
import de.unibonn.realkd.common.parameter.DefaultSubCollectionParameter;
import de.unibonn.realkd.common.parameter.DefaultSubCollectionParameter.CollectionComputer;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.ParameterContainer;
import de.unibonn.realkd.common.parameter.Parameters;
import de.unibonn.realkd.common.parameter.RangeEnumerableParameter;
import de.unibonn.realkd.common.parameter.SubCollectionParameter;
import de.unibonn.realkd.common.workspace.Workspace;

/**
 * Abstract class for builders of MiningSystems that creates MiningAlgorithms
 * and passes them to concrete subclasses via the concretelyBuild-hook.
 * 
 * @author Björn Jacobs
 * 
 * @since 0.1.0
 * 
 * @version 0.4.0
 * 
 */
public abstract class MiningSystemBuilder<T> implements ParameterContainer {

	protected final SubCollectionParameter<String, List<String>> helpPages;

	protected final RangeEnumerableParameter<DefaultDataView> defaultDataView;

	protected final RangeEnumerableParameter<Boolean> usePCAasDefaultVisualization;

	private static List<RankerFactory> rankerFactoryOptions = Arrays.asList(new DefaultRankerFactory(),
			new KnowledgeModelRankerFactory());

	protected final Parameter<Set<MiningAlgorithmFactory>> algorithms;

	private final Parameter<RankerFactory> postProcessor;

	protected final DefaultParameterContainer parameterContainer;

	public MiningSystemBuilder() {
		this.algorithms = DefaultSubCollectionParameter.subSetParameter("Algorithms",
				"The algorithms that will be available in the mining system",
				new CollectionComputer<Set<MiningAlgorithmFactory>>() {
					@Override
					public Set<MiningAlgorithmFactory> computeCollection() {
						return Sets.newHashSet(AlgorithmFactory.values());
					}

				});
		this.parameterContainer = new DefaultParameterContainer();
		this.postProcessor = Parameters.rangeEnumerableParameter("Post processor",
				"A post-processor which is applied to the output of any mining algorithm started with the system.",
				RankerFactory.class, new RangeComputer<RankerFactory>() {
					@Override
					public List<RankerFactory> computeRange() {
						return rankerFactoryOptions;
					}
				});

		this.helpPages = DefaultSubCollectionParameter.getDefaultSubListParameter("Help pages",
				"List of user help pages associated with this mining system.",
				() -> ApplicationRepositories.PAGE_REPOSITORY.getAllIds());

		this.defaultDataView = Parameters.rangeEnumerableParameter("Default data view",
				"The view of the data that is first shown to user on loading of analytics dashboard",
				DefaultDataView.class, () -> ImmutableList.copyOf(DefaultDataView.values()));

		this.usePCAasDefaultVisualization = Parameters.booleanParameter("PCA as scatter default",
				"Whether the scatter plot tries to use the first and second principal component as default axes (if false tries to find first two metric dimensions instead)");

		this.parameterContainer.addAllParameters(Arrays.asList(algorithms, postProcessor, helpPages, defaultDataView, usePCAasDefaultVisualization));
	}

	public final MiningSystem build(IdGenerator idGenerator, DeveloperViewModel developerViewModel,
			Workspace dataWorkspace) {
		validate();
		fixRankerFactory();
		return concreteBuild(idGenerator, developerViewModel, dataWorkspace);
	}

	protected abstract MiningSystem concreteBuild(IdGenerator idGenerator, DeveloperViewModel developerViewModel,
			Workspace dataWorkspace);

	public T setRankerFactory(RankerFactory rankerFactory) {
		this.postProcessor.set(rankerFactory);
		return (T) this;
	}

	public RankerFactory getRankerFactory() {
		return this.postProcessor.current();
	}

	private void fixRankerFactory() {
		if (this.postProcessor.current() == null) {
			this.setRankerFactory(new DefaultRankerFactory());
		}
	}

	public T setAlgorithmFactories(Collection<MiningAlgorithmFactory> algorithmFactories) {
		this.algorithms.set(Sets.newHashSet(algorithmFactories));
		return (T) this;
	}

	public Collection<MiningAlgorithmFactory> getAlgorithmFactories() {
		return algorithms.current();
	}

	@Override
	public List<Parameter<?>> getTopLevelParameters() {
		return this.parameterContainer.getTopLevelParameters();
	}

}
