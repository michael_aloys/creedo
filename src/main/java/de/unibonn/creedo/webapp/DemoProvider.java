package de.unibonn.creedo.webapp;

import static de.unibonn.creedo.ui.indexpage.DashboardLinkEntry.dashboardLinkEntry;

import java.util.ArrayList;
import java.util.List;

import de.unibonn.creedo.ApplicationRepositories;
import de.unibonn.creedo.ui.indexpage.DashboardLinkEntry;
import de.unibonn.creedo.ui.indexpage.DashboardLinkProvider;

/**
 * Compiles the list of demo links available to a user.
 * 
 * @author Björn Jacobs
 * 
 * @since 0.1.0
 * 
 * @version 0.1.2
 * 
 */
public class DemoProvider implements DashboardLinkProvider {

	public static final DemoProvider INSTANCE = new DemoProvider();

	private DemoProvider() {
	}

	public List<DashboardLinkEntry> getDashboardLinks(CreedoSession session) {
		List<DashboardLinkEntry> results = new ArrayList<>();
		ApplicationRepositories.DEMO_REPOSITORY.getAllEntries().stream().filter(x -> x.getContent().isVisibleTo(session.getUser()))
				.forEach(x -> {
					results.add(
							dashboardLinkEntry(x.getContent().build(session.getUser()), session));
				});
		// for (RepositoryEntry<String, DashboardLinkBuilder> demoEntry : ) {
		// results.add(demoEntry.getContent().build(user));
		// }
		return results;
	}

	@Override
	public String getTitle() {
		return "Interactive Demos";
	}

}
