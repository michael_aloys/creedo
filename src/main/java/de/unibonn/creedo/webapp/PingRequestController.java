package de.unibonn.creedo.webapp;

import javax.servlet.http.HttpSession;

import de.unibonn.creedo.Creedo;
import de.unibonn.creedo.WebConstants;
import de.unibonn.creedo.ui.core.UiComponent;
import de.unibonn.creedo.webapp.handler.TimeoutHandler;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

@Controller
public class PingRequestController {

	@RequestMapping(value = WebConstants.PING_PATH, method = RequestMethod.GET)
	@ResponseStatus(value = HttpStatus.OK)
	public ResponseEntity<String> ping(HttpSession session,
//			@RequestParam(value = "analysisSessionId") int asId,
			@RequestParam(value = "pingReceiverId") int pingReceiverId) {

		// Check if ping comes from outdated client window
		// this occurs for instance when user logged out with dashboard still
		// open
		CreedoSession creedoSession = Creedo.getCreedoSession(session);
		// if (!creedoSession.getUiRegister().hasComponent(asId)) {
		// HttpHeaders headers = new HttpHeaders();
		// headers.add("CLOSE_REQUESTED", "1");
		// return new ResponseEntity<>(headers, HttpStatus.OK);
		// }

		// Object timeoutHandlerObj = session
		// .getAttribute(WebConstants.TIMEOUT_HANDLER_KEY);

		// if (timeoutHandlerObj != null) {
		// ((TimeoutHandler) timeoutHandlerObj).ping();
		// return new ResponseEntity<>(HttpStatus.OK);
		// } else {
		// return new ResponseEntity<>(WebConstants.HOME_PATH,
		// HttpStatus.UNAUTHORIZED);
		// }

		UiComponent pingReceiver = creedoSession.getUiComponent(pingReceiverId);

		if (pingReceiver instanceof TimeoutHandler) {
			((TimeoutHandler) pingReceiver).ping();
			return new ResponseEntity<>(HttpStatus.OK);
		} else {
			return new ResponseEntity<>(WebConstants.HOME_PATH,
					HttpStatus.UNAUTHORIZED);
		}

	}

}
