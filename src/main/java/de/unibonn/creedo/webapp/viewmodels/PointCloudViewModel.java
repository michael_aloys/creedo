package de.unibonn.creedo.webapp.viewmodels;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.google.common.collect.ImmutableList;

import weka.core.matrix.EigenvalueDecomposition;
import de.unibonn.realkd.common.parameter.DefaultRangeEnumerableParameter.RangeComputer;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.ParameterContainer;
import de.unibonn.realkd.common.parameter.Parameters;
import de.unibonn.realkd.data.table.DataTable;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.data.table.attribute.CategoricAttribute;
import de.unibonn.realkd.data.table.attribute.DefaultCategoricAttribute;
import de.unibonn.realkd.data.table.attribute.MetricAttribute;
import de.unibonn.realkd.data.table.attribute.OrdinalAttribute;

public class PointCloudViewModel implements ParameterContainer {

	private static final Logger LOGGER = Logger.getLogger(PointCloudViewModel.class.getName());

	private final static class AttributeSupplier implements Supplier<List<Double>> {

		private final OrdinalAttribute<Number> attribute;

		public AttributeSupplier(OrdinalAttribute<Number> attribute) {
			this.attribute = attribute;
		}

		@Override
		public List<Double> get() {
			Stream<Optional<Number>> values = attribute.getValues().stream();
			Stream<Double> imputed = values.map(o -> o.orElse(attribute.median())).map(n -> n.doubleValue());
			return imputed.collect(Collectors.toList());
		}

		@Override
		public String toString() {
			return attribute.name();
		}

		@Override
		public boolean equals(Object other) {
			if (!(other instanceof AttributeSupplier)) {
				return false;
			}
			return ((AttributeSupplier) other).attribute.equals(this.attribute);
		}

	}

	private final class SecondComponentSupplier implements Supplier<List<Double>> {

		private static final String NAME = "PCA.2";

		@Override
		public List<Double> get() {
			return PointCloudViewModel.this.yCoordinates;
		}

		@Override
		public String toString() {
			return NAME;
		}

		@Override
		public boolean equals(Object other) {
			return (other instanceof SecondComponentSupplier);
		}

	}

	private final class FirstComponentSupplier implements Supplier<List<Double>> {

		private static final String NAME = "PCA.1";

		@Override
		public List<Double> get() {
			return PointCloudViewModel.this.xCoordinates;
		}

		@Override
		public String toString() {
			return NAME;
		}

		@Override
		public boolean equals(Object other) {
			return (other instanceof FirstComponentSupplier);
		}

	}

	public static class JsonPointCloudDataEntry {
		private final Double x;
		private final Double y;
		private final String objName;
		private final int id;
		private final int category;
		private final String creedoCategoryName;

		public JsonPointCloudDataEntry(int id, Double x, Double y, String objName, int categoryNumber, String categoryName) {
			this.x = x;
			this.y = y;
			this.objName = objName;
			this.id = id;
			this.category = categoryNumber;
			this.creedoCategoryName = categoryName; 
		}

		public Double getX() {
			return x;
		}

		public Double getY() {
			return y;
		}

		public String getObjName() {
			return objName;
		}

		public int getId() {
			return id;
		}

		public int getCategory() {
			return category;
		}
		
		public String getCreedoCategoryName() {
			return creedoCategoryName;
		}
	}

	private List<List<Double>> transactionFeatureList;
	private List<Double> xCoordinates;
	private List<Double> yCoordinates;
	private List<Double> normXCoordinates;
	private List<Double> normYCoordinates;
	private final DataTable dataTable;

	private final Parameter<Supplier<List<Double>>> xAxisData;
	private final Parameter<Supplier<List<Double>>> yAxisData;
	private final Parameter<Optional<CategoricAttribute<?>>> categoryData;
	
	private final boolean pcaAsScatterDefault;

	public PointCloudViewModel(DataTable dataTable, boolean pcaAsScatterDefault) {
		LOGGER.fine("Creating new point cloud view model");
		this.pcaAsScatterDefault=pcaAsScatterDefault;
		this.transactionFeatureList = getTransactionFeatureList(dataTable);
		doPrincipalComponentAnalysis();
		this.dataTable = dataTable;
		xAxisData = Parameters.rangeEnumerableParameter("X-data", "values to be visualized on x-axis of diagram",
				Supplier.class, createAxisRangeComputer());
		yAxisData = Parameters.rangeEnumerableParameter("Y-data", "values to be visualized on y-axis of diagram",
				Supplier.class, createAxisRangeComputer());
		List<Optional<CategoricAttribute<?>>> categoryOptions = categoryOptions();
		categoryData = Parameters.rangeEnumerableParameter("Category symbols",
				"categories to be mapped to plot symbols", Optional.class, () -> categoryOptions);
		xAxisData.setByString(initialXAxisOption());
		yAxisData.setByString(initialYAxisOption());
		categoryData.setByString(initialCategoryOption().toString());
	}
	
	private List<Attribute<?>> metricAttributes() {
		return dataTable.getAttributes().stream().filter(a->a instanceof MetricAttribute).collect(Collectors.toList());
	}
	
	private Optional<Attribute<?>> firstCategoricalAttribute() {
		return dataTable.getAttributes().stream().filter(a->a instanceof CategoricAttribute).findFirst();
	}
	
	private Optional<String> initialCategoryOption() {
		return firstCategoricalAttribute().map(a->a.name());
	}
	
	private String initialXAxisOption() {
		if (pcaAsScatterDefault || metricAttributes().size()<2) {
			return FirstComponentSupplier.NAME;
		}
		else {
			return metricAttributes().get(0).name();
		}
	}
	
	private String initialYAxisOption() {
		if (pcaAsScatterDefault || metricAttributes().size()<2) {
			return SecondComponentSupplier.NAME;
		}
		else {
			return metricAttributes().get(1).name();
		}
	}

	private List<Optional<CategoricAttribute<?>>> categoryOptions() {
		List<Optional<CategoricAttribute<?>>> options = new ArrayList<>();
		options.add(Optional.empty());
		dataTable.getAttributes().forEach(a -> {
			if (a instanceof CategoricAttribute<?>)
				options.add(Optional.of((CategoricAttribute<?>) a));
		});
		return options;
	}

	private RangeComputer<Supplier<List<Double>>> createAxisRangeComputer() {
		return new RangeComputer<Supplier<List<Double>>>() {

			@Override
			public List<Supplier<List<Double>>> computeRange() {
				List<Supplier<List<Double>>> result = new ArrayList<>();

				Supplier<List<Double>> firstPCSupplier = new FirstComponentSupplier();

				Supplier<List<Double>> secondPCSupplier = new SecondComponentSupplier();

				result.add(firstPCSupplier);
				result.add(secondPCSupplier);

				@SuppressWarnings("unchecked") //safe?
				Stream<OrdinalAttribute<Number>> metricAttributes = PointCloudViewModel.this.dataTable.getAttributes()
						.stream().filter(a -> a instanceof OrdinalAttribute && Number.class.isAssignableFrom(a.type()))
						.map(a -> (OrdinalAttribute<Number>) a);
				metricAttributes.forEach(a -> {
					result.add(metricAttributeToValueSupplier(a));
				});
				return result;
			}

			private Supplier<List<Double>> metricAttributeToValueSupplier(OrdinalAttribute<Number> a) {
				return new AttributeSupplier(a);
			}
		};
	}

	private void doPrincipalComponentAnalysis() {
		int maxRowSize = 0;
		for (List<Double> aTransactionFeatureList : transactionFeatureList) {
			if (maxRowSize < aTransactionFeatureList.size())
				maxRowSize = aTransactionFeatureList.size();
		}
		weka.core.matrix.Matrix dataMatrix = new weka.core.matrix.Matrix(transactionFeatureList.size(), maxRowSize);
		weka.core.matrix.Matrix centralizedDataMatrix = new weka.core.matrix.Matrix(transactionFeatureList.size(),
				maxRowSize);
		weka.core.matrix.Matrix covarianceMatrix = new weka.core.matrix.Matrix(maxRowSize, maxRowSize);
		double[] columnMean = new double[dataMatrix.getColumnDimension()];
		for (int i = 0; i < transactionFeatureList.size(); i++) {
			for (int j = 0; j < maxRowSize; j++) {
				dataMatrix.set(i, j, transactionFeatureList.get(i).get(j));
			}
		}
		for (int i = 0; i < dataMatrix.getColumnDimension(); i++) {
			double columnTotal = 0.0;
			for (int j = 0; j < dataMatrix.getRowDimension(); j++) {
				columnTotal += dataMatrix.get(j, i);
			}
			columnMean[i] = columnTotal / dataMatrix.getRowDimension();
		}

		for (int i = 0; i < dataMatrix.getColumnDimension(); i++) {
			for (int j = 0; j < dataMatrix.getRowDimension(); j++) {
				centralizedDataMatrix.set(j, i, dataMatrix.get(j, i) - columnMean[i]);
			}
		}
		covarianceMatrix = ((centralizedDataMatrix.transpose()).times(centralizedDataMatrix))
				.times(1.0 / (maxRowSize - 1.0));
		EigenvalueDecomposition eigValDecomp = new EigenvalueDecomposition(covarianceMatrix);
		weka.core.matrix.Matrix eigenVectors = eigValDecomp.getV();
		weka.core.matrix.Matrix highestEigenVectors = eigenVectors.getMatrix(0, (eigenVectors.getRowDimension() - 1),
				(eigenVectors.getColumnDimension() - 2), (eigenVectors.getColumnDimension() - 1));
		weka.core.matrix.Matrix finalData = centralizedDataMatrix.times(highestEigenVectors);
		xCoordinates = new ArrayList<>(finalData.getRowDimension());
		yCoordinates = new ArrayList<>(finalData.getRowDimension());

		for (int i = 0; i < finalData.getRowDimension(); i++) {
			xCoordinates.add(finalData.get(i, 1));
		}
		for (int i = 0; i < finalData.getRowDimension(); i++) {
			yCoordinates.add(finalData.get(i, 0));
		}

		normXCoordinates = normalizeZeroToHundred(xCoordinates);
		normYCoordinates = normalizeZeroToHundred(yCoordinates);

	}

	public List<Double> getNormalizedXCoordinates() {
		return normXCoordinates;
	}

	public List<Double> getNormalizedYCoordinates() {
		return normYCoordinates;
	}

	public List<Double> getAbsoluteXCoordinates() {
		return xCoordinates;
	}

	public List<Double> getAbsoluteYCoordinates() {
		return yCoordinates;
	}

	private List<Double> normalizeZeroToHundred(List<Double> data) {
		List<Double> result = new ArrayList<>(data.size());

		double range = Collections.max(data) - Collections.min(data);
		for (int i = 0; i < data.size(); i++) {
			// double normalized = 100.0 * (0.05 * range + data.get(i) -
			// Collections.min(data)) / (1.1 * range);
			double normalized = 100.0 * (data.get(i) - Collections.min(data)) / (range);

			result.add(normalized);
		}

		return result;
	}

	public List<List<Double>> getTransactionFeatureList(DataTable dataTable) {
		List<List<Double>> transFeatureList = new ArrayList<>();
		for (Integer row : dataTable.population().objectIds()) {
			transFeatureList.add(createFeatureVectorFromTrans(row, dataTable));
		}
		return transFeatureList;
	}

	/**
	 * Returns numeric vector for object. Missing numerical values are replaced
	 * by attribute median. Categorical values are replaced by a number of
	 * numeric elements corresponding to the different categories (with a 1
	 * entry for the matching category and 0 elsewhere).
	 */
	private List<Double> createFeatureVectorFromTrans(Integer row, DataTable dataTable) {
		List<Double> transFeature = new ArrayList<>();
		for (Attribute<?> attribute : dataTable.getAttributes()) {
			if (attribute instanceof DefaultCategoricAttribute) {

				List<String> categories = ((DefaultCategoricAttribute) attribute).categories();

				for (String category : categories) {
					if (!attribute.valueMissing(row) && attribute.value(row).equals(category)) {
						transFeature.add(1.);
					} else {
						transFeature.add(0.);
					}
				}

			} else if (attribute instanceof MetricAttribute) {
				if (attribute.valueMissing(row)) {
					transFeature.add(((MetricAttribute) attribute).median());
				} else {
					transFeature.add(((MetricAttribute) attribute).value(row));
				}
			}
		}
		return transFeature;
	}

	public List<String> getAxisTitles() {
		return ImmutableList.of(xAxisData.current().toString(), yAxisData.current().toString());
	}
	
	private String category(int index) {
		if (!categoryData.current().isPresent() || categoryData.current().get().valueMissing(index)) {
			return null;
		}
		else {
			CategoricAttribute<?> attribute = categoryData.current().get();
			return attribute.value(index).toString();
		}
	}
	
	private int categoryNumber(int index) {
		if (!categoryData.current().isPresent() || categoryData.current().get().valueMissing(index)) {
			return 0;
		}
		else {
			CategoricAttribute<?> attribute = categoryData.current().get();
			Object value = attribute.value(index);
			return attribute.categories().indexOf(value);
		}
	}

	public List<PointCloudViewModel.JsonPointCloudDataEntry> getPoints() {
		List<String> objNames = dataTable.population().objectNames();

		List<PointCloudViewModel.JsonPointCloudDataEntry> result = new ArrayList<>();

		// Zip
		for (int i = 0; i < this.getNormalizedXCoordinates().size(); i++) {
			double x = this.xAxisData.current().get().get(i);
			double y = this.yAxisData.current().get().get(i);
			String objName = objNames.get(i);
			result.add(new PointCloudViewModel.JsonPointCloudDataEntry(i, x, y, objName,categoryNumber(i), category(i)));
		}

		return result;
	}

	@Override
	public List<Parameter<?>> getTopLevelParameters() {
		return ImmutableList.of(xAxisData, yAxisData, categoryData);
	}

}
