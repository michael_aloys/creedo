package de.unibonn.creedo.webapp.viewmodels;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import de.unibonn.realkd.data.propositions.AttributeBasedProposition;
import de.unibonn.realkd.data.propositions.TableBasedPropositionalLogic;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.data.table.attribute.CategoricAttribute;
import de.unibonn.realkd.data.table.attribute.MetricAttribute;
import de.unibonn.realkd.data.table.attribute.OrdinalAttribute;

/**
 * 
 * 
 * @author Suparno Datta
 * 
 * @since 0.1.0
 * 
 * @version 0.1.0.1
 *
 */
public class MetaDataTableModel {

	private static final Logger LOGGER = Logger.getLogger(MetaDataTableModel.class.getName());

	private TableBasedPropositionalLogic propLogic;

	public MetaDataTableModel(TableBasedPropositionalLogic propLogic) {
		LOGGER.fine("Creating new metadata view model");
		this.propLogic = propLogic;
	}

	public List<Attribute<?>> getNonIDAttributes() {
		return propLogic.getDatatable().getAttributes();
	}

	// public Attribute getNonIDAttribute(int attributeIndex) {
	// return getNonIDAttributes().get(attributeIndex);
	// }

	/**
	 * @return highest scale level of attribute
	 */
	private String getAttributeType(Attribute<?> attribute) {
		String result;
		if (attribute instanceof MetricAttribute) {
			result = "Metric";
		} else if (attribute instanceof OrdinalAttribute) {
			result = "Ordinal";
		} else if (attribute instanceof CategoricAttribute) {
			result = "Categorical";
		} else {
			throw new IllegalArgumentException("Must be called with non id attribute");
		}

		return result;
	}

	public String getConstraintDescriptionAbout(Attribute<?> attribute) {
		StringBuffer result = new StringBuffer();
		for (AttributeBasedProposition<?> prop : propLogic.getAttributeBasedPropositionsAbout(attribute)) {
			result.append(prop.getConstraint().description() + ", ");
		}

		if (result.length() > 0) {
			result.delete(result.length() - 2, result.length());
		}

		return result.toString();
	}

	public List<AttributeMetaDataContainer> getEntries() {
		List<AttributeMetaDataContainer> result = new ArrayList<>();
		for (Attribute<?> attr : getNonIDAttributes()) {
			result.add(new AttributeMetaDataContainer(attr));
		}
		return result;
	}

	public class AttributeMetaDataContainer {
		public String getName() {
			return name;
		}

		public String getType() {
			return type;
		}

		public String getDescription() {
			return description;
		}

		private final String name;
		private final String type;
		private final String description;

		public AttributeMetaDataContainer(Attribute<?> attr) {
			this.name = attr.name();
			this.type = getAttributeType(attr);
			this.description = getConstraintDescriptionAbout(attr);
		}
	}

}
