package de.unibonn.creedo.webapp.patternviews;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import de.unibonn.creedo.webapp.dashboard.patterncontainer.WebPattern;
import de.unibonn.realkd.patterns.association.Association;
import de.unibonn.realkd.patterns.emm.ExceptionalModelPattern;
import de.unibonn.realkd.patterns.outlier.Outlier;
import de.unibonn.realkd.patterns.pmm.PureModelSubgroup;
import de.unibonn.realkd.patterns.subgroups.Subgroup;
import de.unibonn.realkd.util.Strings;

public class ResultPatternGenerator implements PatternHTMLGenerator {

	public static ResultPatternGenerator INSTANCE = new ResultPatternGenerator();

	private static List<String> ACTIONS = new ArrayList<String>();

	static {
		ACTIONS.add(GeneralPattenGenerator.FILTER_DATA_ACTION_HTML);
		ACTIONS.add(GeneralPattenGenerator.OPEN_DETAILED_VIEW_ACTION_HTML);
		ACTIONS.add(GeneralPattenGenerator.DELETION_ACTION_HTML);
	}

	private static ResultDefaultPatternGenerator DEFAULT_GENERATOR = new ResultDefaultPatternGenerator(ACTIONS,
			ResultPatternMapper.AnnotationVisibility.EDITABLE);

	private static ResultPatternMapper EMM_GENERATOR = new ResultPatternMapper("subgroup", ACTIONS,
			ResultPatternMapper.AnnotationVisibility.EDITABLE,
			webPattern -> "<strong>Exceptional Subgroup</strong> ("
					+ Strings.chop(((Subgroup) webPattern.getPattern().descriptor()).targetAttributes().stream()
							.map(a -> a.name() + ",").collect(Collectors.joining()), 1)
					+ ")",
			pattern -> ((Subgroup) pattern.descriptor()).extensionDescriptor().getElementsAsStringList());

	private static ResultPatternMapper PMM_GENERATOR = new ResultPatternMapper("subgroup", ACTIONS,
			ResultPatternMapper.AnnotationVisibility.EDITABLE,
			webPattern -> "<strong>Pure Subgroup</strong> ("
					+ Strings.chop(((Subgroup) webPattern.getPattern().descriptor()).targetAttributes().stream()
							.map(a -> a.name() + ",").collect(Collectors.joining()), 1)
					+ ")",
			pattern -> ((Subgroup) pattern.descriptor()).extensionDescriptor().getElementsAsStringList());

	private static ResultAssociationPatternMapper ASSOCIATION_GENERATOR = new ResultAssociationPatternMapper(ACTIONS,
			ResultPatternMapper.AnnotationVisibility.EDITABLE);

	private static ResultOutlierPatternGenerator OUTLIER_GENERATOR = new ResultOutlierPatternGenerator(ACTIONS,
			ResultPatternMapper.AnnotationVisibility.EDITABLE);

	private ResultPatternGenerator() {
		;
	}

	@Override
	public String getHTML(HttpSession session, WebPattern webPattern) {
		if (webPattern.getPattern() instanceof Association) {
			return ResultPatternGenerator.ASSOCIATION_GENERATOR.getHTML(session, webPattern);
		} else if (webPattern.getPattern() instanceof ExceptionalModelPattern) {
			return ResultPatternGenerator.EMM_GENERATOR.getHTML(session, webPattern);
		} else if (webPattern.getPattern() instanceof PureModelSubgroup) {
			return ResultPatternGenerator.PMM_GENERATOR.getHTML(session, webPattern);
		} else if (webPattern.getPattern() instanceof Outlier) {
			return ResultPatternGenerator.OUTLIER_GENERATOR.getHTML(session, webPattern);
		} else {
			return ResultPatternGenerator.DEFAULT_GENERATOR.getHTML(session, webPattern);
		}
	}

}
