package de.unibonn.creedo.webapp.patternviews;

import java.util.List;

import de.unibonn.realkd.patterns.QualityMeasureId;

public class ResultAssociationPatternMapper extends ResultPatternMapper implements PatternHTMLGenerator {

	public ResultAssociationPatternMapper(List<String> optionalActions, AnnotationVisibility annotationVisibility) {
		super("association", optionalActions, annotationVisibility,
				webPattern -> webPattern.getPattern().hasMeasure(QualityMeasureId.LIFT)
						? "<strong>Positively Associated Attribute Values</strong>"
						: "<strong>Negatively Associated Attribute Values</strong>",
				LogicalDescriptorPatternToAnnotatedDescriptorMap.INSTANCE);
	}

}
