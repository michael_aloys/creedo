package de.unibonn.creedo.webapp.patternviews;

import static com.google.common.base.Preconditions.checkArgument;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.function.Function;

import javax.servlet.http.HttpSession;

import com.google.common.collect.ImmutableList;

import de.unibonn.creedo.webapp.dashboard.patterncontainer.WebPattern;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.patterns.Measure;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.QualityMeasureId;
import de.unibonn.realkd.patterns.SubPopulationDescriptor;
import de.unibonn.realkd.patterns.association.Association;
import de.unibonn.realkd.patterns.emm.ExceptionalModelPattern;
import de.unibonn.realkd.patterns.logical.LogicalDescriptor;
import de.unibonn.realkd.patterns.outlier.Outlier;
import de.unibonn.realkd.patterns.pmm.PureModelSubgroup;
import de.unibonn.realkd.patterns.subgroups.Subgroup;

public class CandidatePatternGenerator implements PatternHTMLGenerator {

	public static CandidatePatternGenerator INSTANCE = new CandidatePatternGenerator();

	private static List<String> ACTIONS = new ArrayList<String>();

	static {
		ACTIONS.add(GeneralPattenGenerator.FILTER_DATA_ACTION_HTML);
		ACTIONS.add(GeneralPattenGenerator.OPEN_DETAILED_VIEW_ACTION_HTML);
		ACTIONS.add(GeneralPattenGenerator.DELETION_ACTION_HTML);
	}

	private static CandidateAssociationPatternMapper ASSOCIATION_GENERATOR = new CandidateAssociationPatternMapper(
			ACTIONS);

	private static DefaultCandidatePatternGenerator EMM_GENERATOR = new DefaultCandidatePatternGenerator(ACTIONS,
			webPattern -> "Subgroup of Rows with",
			pattern -> ((ExceptionalModelPattern) pattern).descriptor().extensionDescriptor()
					.getElementsAsStringList(),
			"subgroup", new SubgroupPatternToolTipMapper(), new CandidateExceptionalModelExplanationMapper());

	private static DefaultCandidatePatternGenerator PMM_GENERATOR = new DefaultCandidatePatternGenerator(ACTIONS,
			webPattern -> "Subgroup of Rows with",
			pattern -> ((PureModelSubgroup) pattern).descriptor().extensionDescriptor().getElementsAsStringList(),
			"subgroup", new SubgroupPatternToolTipMapper(), new CandidatePureModelExplanationMapper());

	private static OutlierPatternGenerator OUTLIER_GENERATOR = new OutlierPatternGenerator(ACTIONS);

	private static DefaultCandidatePatternGenerator DEFAULT_GENERATOR = new DefaultCandidatePatternGenerator(ACTIONS,
			webPattern -> "Pattern", new DefaultPatternDescriptionMapper(), "generic-pattern",
			new DefaultPatternToolTipMapper(), pattern -> ImmutableList.of());

	private static class SubgroupPatternToolTipMapper implements Function<Pattern, String> {

		@Override
		public String apply(Pattern pattern) {
			StringBuilder sb = new StringBuilder();
			LogicalDescriptor extensionDescriptor = ((Subgroup) pattern.descriptor()).extensionDescriptor();
			for (int j = 0; j < extensionDescriptor.getElementsAsStringList().size(); j++) {
				sb.append(extensionDescriptor.getElementsAsStringList().get(j));
				sb.append("<br/>");
			}
			sb.append("-------------------------<br/>");

			for (Measure measure : pattern.measures()) {
				sb.append(measure.getName()).append(": ")
						.append(String.format(Locale.ENGLISH, "%.4f", pattern.value(measure))).append("<br/>");
			}

			return sb.toString();
		}

	}

	private static class CandidatePureModelExplanationMapper implements Function<Pattern, List<String>> {

		@Override
		public List<String> apply(Pattern pattern) {
			checkArgument(pattern instanceof PureModelSubgroup, "Pattern must be pmm pattern");
			checkArgument(pattern.descriptor() instanceof SubPopulationDescriptor,
					"Pattern descriptor must describe sub population of data.");
			List<String> result = new ArrayList<>();
			Measure deviationIntMeasure = ((PureModelSubgroup) pattern).purityGainMeasure();
			result.add("has <b>pure distribution of " + getTargetsString(pattern) + "</b> " + "("
					+ deviationIntMeasure.getName() + " "
					+ String.format(Locale.ENGLISH, "%.3f", pattern.value(deviationIntMeasure)) + ")");

			result.add("and <b>contains</b> " + ((SubPopulationDescriptor) pattern.descriptor()).indices().size()
					+ " data points" + (pattern.hasMeasure(QualityMeasureId.FREQUENCY) ? String.format(Locale.ENGLISH,
							" (freq. %.3f)", pattern.value(QualityMeasureId.FREQUENCY)) : ""));
			return result;
		}

		private String getTargetsString(Pattern pattern) {
			StringBuilder sb = new StringBuilder();
			for (Attribute<?> attribute : ((PureModelSubgroup) pattern).descriptor().targetAttributes()) {
				sb.append(attribute.name() + ", ");
			}
			return sb.substring(0, sb.length() - 2);
		}

	}

	private static class CandidateExceptionalModelExplanationMapper implements Function<Pattern, List<String>> {

		@Override
		public List<String> apply(Pattern pattern) {
			checkArgument(pattern instanceof ExceptionalModelPattern, "Pattern must be EMM pattern"); // TODO
																										// generalize
			checkArgument(pattern.descriptor() instanceof SubPopulationDescriptor,
					"Pattern descriptor must describe sub population of data.");
			List<String> result = new ArrayList<>();
			Measure deviationIntMeasure = ((ExceptionalModelPattern) pattern).getDeviationMeasure();
			result.add("has <b>unusual distribution of " + getTargetsString(pattern) + "</b> "
			// + "(dev. "
					+ "(" + deviationIntMeasure.getName() + " "
					+ String.format(Locale.ENGLISH, "%.3f", pattern.value(deviationIntMeasure))
					// ((ExceptionalModelPattern) pattern).getModelDeviation())
					+ ")");

			result.add("and <b>contains</b> " + ((SubPopulationDescriptor) pattern.descriptor()).indices().size()
					+ " data points" + (pattern.hasMeasure(QualityMeasureId.FREQUENCY) ? String.format(Locale.ENGLISH,
							" (freq. %.3f)", pattern.value(QualityMeasureId.FREQUENCY)) : ""));
			return result;
		}

		private String getTargetsString(Pattern pattern) {
			StringBuilder sb = new StringBuilder();
			for (Attribute<?> attribute : ((ExceptionalModelPattern) pattern).descriptor().targetAttributes()) {
				sb.append(attribute.name() + ", ");
			}
			return sb.substring(0, sb.length() - 2);
		}

	}

	// public CandidateExceptionalModelPatternMapper(List<String>
	// optionalActions) {
	// super(optionalActions, webPattern -> "Subgroup of Rows with",
	// pattern -> ((ExceptionalModelPattern)
	// pattern).getDescriptor().extensionDescriptor()
	// .getElementsAsStringList(),
	// "subgroup", new ExceptionalModelPatternToolTipMapper(),
	// new CandidateExceptionalModelExplanationMapper());
	// }

	private static class DefaultPatternToolTipMapper implements Function<Pattern, String> {
		@Override
		public String apply(Pattern pattern) {
			StringBuffer res = new StringBuffer();
			if (pattern.descriptor() instanceof SubPopulationDescriptor) {
				SubPopulationDescriptor subPopulationDescriptor = (SubPopulationDescriptor) pattern.descriptor();
				for (Integer index : subPopulationDescriptor.indices()) {
					res.append("<br/>");
					res.append(subPopulationDescriptor.population().objectName(index));
				}
			}
			return res.toString();
		}
	}

	private static class DefaultPatternDescriptionMapper implements Function<Pattern, List<String>> {

		@Override
		public List<String> apply(Pattern pattern) {
			if (!(pattern.descriptor() instanceof SubPopulationDescriptor)) {
				return Arrays.asList("No description available.");
			}

			SubPopulationDescriptor subPopulationDescriptor = (SubPopulationDescriptor) pattern.descriptor();
			List<String> res = new ArrayList<>(subPopulationDescriptor.indices().size());
			for (Integer index : subPopulationDescriptor.indices()) {
				res.add(subPopulationDescriptor.population().objectName(index));
			}

			return res;
		}

	}

	private CandidatePatternGenerator() {
		;
	}

	@Override
	public String getHTML(HttpSession session, WebPattern webPattern) {
		if (webPattern.getPattern() instanceof Association) {
			return CandidatePatternGenerator.ASSOCIATION_GENERATOR.getHTML(session, webPattern);
		} else if (webPattern.getPattern() instanceof ExceptionalModelPattern) {
			return CandidatePatternGenerator.EMM_GENERATOR.getHTML(session, webPattern);
		} else if (webPattern.getPattern() instanceof PureModelSubgroup) {
			return CandidatePatternGenerator.PMM_GENERATOR.getHTML(session, webPattern);
		} else if (webPattern.getPattern() instanceof Outlier) {
			return CandidatePatternGenerator.OUTLIER_GENERATOR.getHTML(session, webPattern);
		} else {
			return CandidatePatternGenerator.DEFAULT_GENERATOR.getHTML(session, webPattern);
		}
	}
}
