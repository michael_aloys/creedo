package de.unibonn.creedo.webapp.patternviews;

import static com.google.common.base.Preconditions.checkArgument;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.function.Function;

import de.unibonn.realkd.data.propositions.PropositionalLogic;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.logical.LogicalDescriptor;

/**
 * Maps a pattern with logical descriptor to a list with one entry per
 * descriptor element (conjunction term), which is annotated by the terms
 * occurrence frequency in brackets.
 * 
 * @author Mario Boley
 * 
 * @author Bo Kang
 * 
 * @author Ruafang Xu
 * 
 * @since 0.1.0
 * 
 * @version 0.1.0
 *
 */
public enum LogicalDescriptorPatternToAnnotatedDescriptorMap implements Function<Pattern, List<String>> {

	INSTANCE;

	@Override
	public List<String> apply(Pattern pattern) {
		/*
		 * currently this is duplication from ResultAssociationMapper
		 */
		checkArgument(pattern.descriptor() instanceof LogicalDescriptor,
				"Assumes that pattern has logical descriptor.");
		LogicalDescriptor logicalDescriptor = (LogicalDescriptor) pattern.descriptor();
		PropositionalLogic propositionalLogic = logicalDescriptor.getPropositionalLogic();
		List<String> result = new ArrayList<>(logicalDescriptor.size());

		for (int i = 0; i < logicalDescriptor.size(); i++) {
			result.add(logicalDescriptor.getElement(i) + " ("
					+ String.format(Locale.ENGLISH, "%.4f",
							propositionalLogic.getSupportSet(logicalDescriptor.getElement(i).getId()).size()
									/ (double) propositionalLogic.population().size())
					+ ")");
		}

		return result;
	}
}