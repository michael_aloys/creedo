package de.unibonn.creedo.webapp.patternviews;

import javax.servlet.http.HttpSession;

import de.unibonn.creedo.webapp.dashboard.patterncontainer.WebPattern;

public interface PatternHTMLGenerator {

	public String getHTML(HttpSession session, WebPattern webPattern);

	static int VISIBLE_DESCRIPTOR_NUMBER = 3;

}
