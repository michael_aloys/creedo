package de.unibonn.creedo.webapp.patternviews;

import static com.google.common.base.Preconditions.checkArgument;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.Function;

import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.SubPopulationDescriptor;
import de.unibonn.realkd.patterns.TableSubspaceDescriptor;

public class ResultOutlierPatternGenerator extends ResultPatternMapper {

	public ResultOutlierPatternGenerator(List<String> optionalActions, AnnotationVisibility annotationVisibility) {
		super("generic-pattern", optionalActions, annotationVisibility, webPattern -> "Outlier Pattern",
				new ResultOutlierDescriptionMapping());
	}

	private static class ResultOutlierDescriptionMapping implements Function<Pattern, List<String>> {

		@Override
		public List<String> apply(Pattern pattern) {
			checkArgument(pattern.descriptor() instanceof SubPopulationDescriptor,
					"Descriptor must describe sub population.");
			checkArgument(pattern.descriptor() instanceof TableSubspaceDescriptor,
					"Descriptor must describe table subspace.");
			SubPopulationDescriptor subPopulationDescriptor = (SubPopulationDescriptor) pattern.descriptor();
			List<String> res = new ArrayList<>(subPopulationDescriptor.indices().size());
			res.add("The rows");
			for (Integer index : subPopulationDescriptor.indices()) {
				res.add(subPopulationDescriptor.population().objectName(index));
			}
			if (!((TableSubspaceDescriptor) pattern.descriptor()).getReferencedAttributes().isEmpty()) {
				res.add("behave annormaly in terms of " + getAttributesDescriptionSubstring(pattern) + ".");
			}
			return res;
		}

		private String getAttributesDescriptionSubstring(Pattern pattern) {
			StringBuffer resultBuffer = new StringBuffer();
			Iterator<Attribute<?>> attributes = ((TableSubspaceDescriptor) pattern.descriptor())
					.getReferencedAttributes().iterator();
			while (attributes.hasNext()) {
				resultBuffer.append("<strong>" + attributes.next().name() + "</strong>");
				if (attributes.hasNext()) {
					resultBuffer.append(", ");
				}
			}
			return resultBuffer.toString();
		}

	}

}
