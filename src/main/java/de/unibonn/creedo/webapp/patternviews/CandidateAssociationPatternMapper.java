package de.unibonn.creedo.webapp.patternviews;

import static com.google.common.base.Preconditions.checkArgument;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.function.Function;

import de.unibonn.realkd.patterns.Measure;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.QualityMeasureId;
import de.unibonn.realkd.patterns.SubPopulationDescriptor;
import de.unibonn.realkd.patterns.association.Association;
import de.unibonn.realkd.patterns.logical.LogicalDescriptor;

public class CandidateAssociationPatternMapper extends DefaultCandidatePatternGenerator
		implements PatternHTMLGenerator {

	private static final class AssociationExplanationMapper implements Function<Pattern, List<String>> {

		@Override
		public List<String> apply(Pattern pattern) {
			checkArgument(pattern.descriptor() instanceof SubPopulationDescriptor,
					"Pattern descriptor must describe sub population of data.");
			List<String> result = new ArrayList<>();

			result.add("<b>occur together in</b> " + ((SubPopulationDescriptor) pattern.descriptor()).indices().size()
					+ " rows"
					+ (pattern.hasMeasure(QualityMeasureId.FREQUENCY)
							? String.format(Locale.ENGLISH, " (freq. %.3f)", pattern.value(QualityMeasureId.FREQUENCY))
							: ""));

			result.add(
					String.format(Locale.ENGLISH, "<b>compared to</b> %.1f rows, which are ",
							((Association) pattern).getExpectedFrequency() * ((LogicalDescriptor) pattern.descriptor())
									.getPropositionalLogic().population().size())
					+ String.format(Locale.ENGLISH, "expected assuming independence (lift %.3f)",
							((Association) pattern).getLift()));

			return result;
		}

	}

	private static class AssociationToolTipMapper implements Function<Pattern, String> {

		@Override
		public String apply(Pattern pattern) {
			checkArgument(pattern instanceof Association, "Pattern must be Association");
			checkArgument(pattern.descriptor() instanceof LogicalDescriptor, "Pattern must be described logically.");
			StringBuilder sb = new StringBuilder();

			Association association = (Association) pattern;

			for (String element : ((LogicalDescriptor) association.descriptor()).getElementsAsStringList()) {
				sb.append(element).append("<br/>");
			}

			sb.append("-------------------------<br/>");

			for (Measure measure : association.measures()) {
				sb.append(measure.getName()).append(": ")
						.append(String.format(Locale.ENGLISH, "%.4f", association.value(measure))).append("<br/>");
			}

			return sb.toString();
		}

	}

	public CandidateAssociationPatternMapper(List<String> optionalActions) {
		super(optionalActions,
				webPattern -> webPattern.getPattern().hasMeasure(QualityMeasureId.LIFT)
						? "Positively Associated Attribute Values" : "Negatively Associated Attribute Values",
				LogicalDescriptorPatternToAnnotatedDescriptorMap.INSTANCE, "association",
				new AssociationToolTipMapper(), new AssociationExplanationMapper());
	}


}
