package de.unibonn.creedo.webapp.patternviews;

import static com.google.common.base.Preconditions.checkArgument;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import com.google.common.collect.ImmutableList;

import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.SubPopulationDescriptor;

public class OutlierPatternGenerator extends DefaultCandidatePatternGenerator {

	private static class OutlierTooltipMapper implements Function<Pattern, String> {
		@Override
		public String apply(Pattern pattern) {
			checkArgument(pattern.descriptor() instanceof SubPopulationDescriptor,
					"Pattern descriptor must describe sub population of data.");
			StringBuffer res = new StringBuffer();
			SubPopulationDescriptor subPopulation = (SubPopulationDescriptor) pattern.descriptor();
			for (Integer index : subPopulation.indices()) {
				res.append("<br/>");
				res.append(subPopulation.population().objectName(index));
			}
			return res.toString();
		}
	}

	private static class OutlierDescriptionMapper implements Function<Pattern, List<String>> {
		@Override
		public List<String> apply(Pattern pattern) {
			checkArgument(pattern.descriptor() instanceof SubPopulationDescriptor,
					"Pattern descriptor must describe subpopulation of data.");
			SubPopulationDescriptor subPopulation = (SubPopulationDescriptor) pattern.descriptor();
			List<String> res = new ArrayList<>(subPopulation.indices().size());
			for (Integer index : subPopulation.indices()) {
				res.add(subPopulation.population().objectName(index));
			}

			return res;
		}
	}

	public OutlierPatternGenerator(List<String> optionalActions) {
		// TODO: Is there need for an own html class here?
		super(optionalActions, webPattern -> "Outlier Pattern", new OutlierDescriptionMapper(), "generic-pattern",
				new OutlierTooltipMapper(), pattern -> ImmutableList.of());
	}

	// @Override
	// protected String getHTMLClass() {
	// // TODO: Is there need for an own html class here?
	// return "generic-pattern";
	// }

	// @Override
	// protected String getTitle(WebPattern webPattern) {
	// return "Outlier Pattern";
	// }

	// @Override
	// protected String getTooltip(Pattern pattern) {
	// checkArgument(pattern.getDescriptor() instanceof SubPopulationDescriptor,
	// "Pattern descriptor must describe sub population of data.");
	// StringBuffer res = new StringBuffer();
	// SubPopulationDescriptor subPopulation = (SubPopulationDescriptor)
	// pattern.getDescriptor();
	// for (Integer index : subPopulation.indices()) {
	// res.append("<br/>");
	// res.append(subPopulation.population().objectName(index));
	// }
	// return res.toString();
	// }

//	@Override
//	protected List<String> getExplanationElements(Pattern pattern) {
//		return new ArrayList<>();
//	}

	// @Override
	// protected String getDescription(Pattern pattern) {
	// StringBuffer res=new StringBuffer();
	// int numElements=0;
	// for (Integer index: pattern.getSupportSet()) {
	// res.append("<br/>");
	// res.append(pattern.getDataTable().getObjectName(index));
	// numElements++;
	// if (numElements==VISIBLE_DESCRIPTOR_NUMBER) break;
	// }
	// if (pattern.getSupportSet().size() > VISIBLE_DESCRIPTOR_NUMBER) {
	// res.append("<span id = \"hasMore\"><br/>... </span>");
	// }
	// return res.toString();
	// }

	// @Override
	// protected List<String> getDescriptorElements(Pattern pattern) {
	// checkArgument(pattern.getDescriptor() instanceof SubPopulationDescriptor,
	// "Pattern descriptor must describe subpopulation of data.");
	// SubPopulationDescriptor subPopulation = (SubPopulationDescriptor)
	// pattern.getDescriptor();
	// List<String> res = new ArrayList<>(subPopulation.indices().size());
	// for (Integer index : subPopulation.indices()) {
	// res.add(subPopulation.population().objectName(index));
	// }
	//
	// return res;
	// }

}
