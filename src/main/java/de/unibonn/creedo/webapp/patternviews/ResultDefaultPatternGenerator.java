package de.unibonn.creedo.webapp.patternviews;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.Function;

import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.SubPopulationDescriptor;
import de.unibonn.realkd.patterns.TableSubspaceDescriptor;

public class ResultDefaultPatternGenerator extends ResultPatternMapper {

	public ResultDefaultPatternGenerator(List<String> optionalActions, AnnotationVisibility annotationVisibility) {
		super("generic-pattern", optionalActions, annotationVisibility, webPattern -> "Pattern",
				new ResultDefaultPatternDescriptionMapper());
	}

	private static class ResultDefaultPatternDescriptionMapper implements Function<Pattern, List<String>> {

		@Override
		public List<String> apply(Pattern pattern) {
			if (pattern.descriptor() instanceof SubPopulationDescriptor) {
				return getDescriptionElements((SubPopulationDescriptor) pattern.descriptor());
			}
			List<String> res = new ArrayList<>();
			res.add("No description available.");
			return res;
		}

		private List<String> getDescriptionElements(SubPopulationDescriptor descriptor) {
			List<String> res = new ArrayList<>(descriptor.indices().size());

			res.add("The rows");
			for (Integer index : descriptor.indices()) {
				res.add(descriptor.population().objectName(index));
			}
			if ((descriptor instanceof TableSubspaceDescriptor)
					&& !((TableSubspaceDescriptor) descriptor).getReferencedAttributes().isEmpty()) {
				res.add("have a notable behavior in term of " + getAttributesDescriptionSubstring(
						((TableSubspaceDescriptor) descriptor).getReferencedAttributes()) + ".");
			} else {
				res.add("stick out.");
			}

			return res;
		}

		private String getAttributesDescriptionSubstring(List<Attribute<?>> attributes) {
			StringBuffer resultBuffer = new StringBuffer();
			Iterator<Attribute<?>> attributeIterator = attributes.iterator();
			while (attributeIterator.hasNext()) {
				resultBuffer.append("<strong>" + attributeIterator.next().name() + "</strong>");
				if (attributeIterator.hasNext()) {
					resultBuffer.append(", ");
				}
			}
			return resultBuffer.toString();
		}

	}

}
