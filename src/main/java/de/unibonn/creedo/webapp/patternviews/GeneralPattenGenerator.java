package de.unibonn.creedo.webapp.patternviews;

import java.util.List;

import javax.servlet.http.HttpSession;

import de.unibonn.creedo.webapp.dashboard.patterncontainer.WebPattern;

import org.jfree.chart.JFreeChart;

import de.unibonn.creedo.webapp.utils.ServerVisualizationTools;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.visualization.pattern.PatternVisualization;
import de.unibonn.realkd.visualization.pattern.RegisteredPatternVisualization;

public abstract class GeneralPattenGenerator implements PatternHTMLGenerator {

	public static final String FILTER_DATA_ACTION_HTML = "<a href=\"#\" title='Filter data view' class=\"btn-toggle-support-set\"><span class=\"glyphicon glyphicon-screenshot\"></span></a>";

	public static final String OPEN_DETAILED_VIEW_ACTION_HTML = "<a href=\"#\" title='Detailed view' class='detailed-view'><span class=\"glyphicon glyphicon-search\"></span></a>";

	/**
	 * This string has some leading space in order to separate this action from
	 * actions from the left. Consequently, it should always be added as last
	 * optional action.
	 */
	public static final String DELETION_ACTION_HTML = "&nbsp;&nbsp;&nbsp;<a href=\"#\" title='Discard' class=\"btn-remove-pattern\"><span class=\"glyphicon glyphicon-remove\"></span></a>";

	private final List<String> optionalActions;

	public GeneralPattenGenerator(List<String> optionalActions) {
		this.optionalActions = optionalActions;
	}

	@Override
	public final String getHTML(HttpSession session, WebPattern webPattern) {
		StringBuilder result = new StringBuilder();
		fillHtmlStringBuffer(result, session, webPattern);
		return result.toString();
	}

	protected abstract void fillHtmlStringBuffer(StringBuilder buffer,
			HttpSession session, WebPattern webPattern);

	protected String getActionsDiv(WebPattern webPattern) {
		StringBuilder result = new StringBuilder();
		result.append("<div class=\"actions pull-right\">");
		for (String action : optionalActions) {
			result.append(action);
		}
		result.append("</div><div class=\"clearfix\"></div></div>");
		return result.toString();
	}

	protected String getIllustration(HttpSession session, Pattern pattern) {
		StringBuilder result = new StringBuilder();
		result.append("<div class='visualization'>");

		for (PatternVisualization visualization : RegisteredPatternVisualization
				.values()) {
			if (visualization.isApplicable(pattern)) {
				result.append(illustrate(visualization, pattern, session));
			}
		}

		result.append("</div>");
		return result.toString();
	}

	private String illustrate(PatternVisualization pv, Pattern pattern,
			HttpSession session) {
		return "<img src='" + getDrawing(pv, pattern, session) + "' /> ";
	}

	private String getDrawing(PatternVisualization pv, Pattern pattern,
			HttpSession session) {
		JFreeChart chart = pv.draw(pattern);
		return ServerVisualizationTools
				.serveChartAsPNG(session, chart, 130, 90);
	}

}