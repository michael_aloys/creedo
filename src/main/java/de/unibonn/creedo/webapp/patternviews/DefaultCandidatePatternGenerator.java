package de.unibonn.creedo.webapp.patternviews;

import java.util.List;
import java.util.function.Function;

import javax.servlet.http.HttpSession;

import de.unibonn.creedo.webapp.dashboard.patterncontainer.WebPattern;
import de.unibonn.realkd.patterns.Pattern;

public class DefaultCandidatePatternGenerator extends GeneralPattenGenerator implements PatternHTMLGenerator {

	private final Function<WebPattern, String> titleMapper;

	private final Function<Pattern, List<String>> patternToDescriptorMapper;

	private final String htmlClass;

	private final Function<Pattern, String> toolTipMapper;

	private final Function<Pattern, List<String>> explanationMapper;

	public DefaultCandidatePatternGenerator(List<String> optionalActions, Function<WebPattern, String> titleMapper,
			Function<Pattern, List<String>> patternToDescriptorMapper, String htmlClass,
			Function<Pattern, String> toolTipMapper, Function<Pattern, List<String>> explanationMapper) {
		super(optionalActions);
		this.titleMapper = titleMapper;
		this.toolTipMapper = toolTipMapper;
		this.htmlClass = htmlClass;
		this.patternToDescriptorMapper = patternToDescriptorMapper;
		this.explanationMapper = explanationMapper;
	}

	private String getTitleDiv(String title) {
		return "<div class=\"panel-heading\"><div class=\"title pull-left\">" + title + "</div>";
	}

	@Override
	protected void fillHtmlStringBuffer(StringBuilder result, HttpSession session, WebPattern webPattern) {
		result.append("<div class=\"panel panel-default pattern " + htmlClass + "\" "
				+ "data-toggle=\"tooltip\" data-html=\"true\" data-placement=\"left\" id=\"" + webPattern.getId()
				+ "\" title=\"" + toolTipMapper.apply(webPattern.getPattern()) + "\" supportSetProviderId=\""
				+ webPattern.getSupportSetProviderId() + "\" actionProviderId=\"" + webPattern.getActionProviderId()
				+ "\" openDetailedViewActionId=\"" + webPattern.getOpenDetailedViewActionId() + "\">");
		// header
		result.append(getTitleDiv(titleMapper.apply(webPattern)));
		result.append(getActionsDiv(webPattern));

		// panel body
		result.append("<div class=\"panel-body\">");

		// description
		result.append(getDescription(webPattern.getPattern()));

		// explanation
		result.append(getExplanation(webPattern.getPattern()));

		// visualization
		result.append(getIllustration(session, webPattern.getPattern()));

		// close panel-body and panel div tags
		result.append("</div></div>");

	}

	private String getDescription(Pattern pattern) {
		// List<String> descriptorList = getDescriptorElements(pattern);
		List<String> descriptorList = patternToDescriptorMapper.apply(pattern);

		StringBuilder sb = new StringBuilder();
		sb.append("<div class='description'>");

		if (!descriptorList.isEmpty()) {
			sb.append(descriptorList.get(0));
			for (int dscrpCount = 1; dscrpCount < VISIBLE_DESCRIPTOR_NUMBER
					&& dscrpCount < descriptorList.size(); dscrpCount++) {
				sb.append("<br/>");
				sb.append(descriptorList.get(dscrpCount));
			}
		}
		if (descriptorList.size() > VISIBLE_DESCRIPTOR_NUMBER) {
			sb.append("<span id = \"hasMore\"><br/>... </span>");
		}

		sb.append("</div>");
		return sb.toString();
	}

	private final String getExplanation(Pattern pattern) {
		StringBuilder sb = new StringBuilder();
		sb.append("<div class='explanation'>");
		for (String element : explanationMapper.apply(pattern)) {
			sb.append(element);
			sb.append("<br/>");
		}
		sb.append("</div>");
		return sb.toString();
	}

}
