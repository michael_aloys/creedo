package de.unibonn.creedo.webapp.studies.result;

import java.util.List;

import de.unibonn.creedo.ui.mining.AnalyticsDashboard;
import de.unibonn.realkd.patterns.PatternBuilder;

/**
 * <p>
 * Interface representing result definitions of study tasks. A result definition
 * consists of requirements of when a task is considered done (in terms of the
 * state of an analytics dashboard) and a procedure to extract a list of study
 * results from an analytics dashboard.
 * </p>
 * <p>
 * NOTE: at the moment (0.1.2.1), this contains also the (only) elementary
 * result metric definition of time. This is to be changed in future versions.
 * However, applicable result metrics will always depend on the chosen result
 * definition.
 * </p>
 *
 * @author Bo Kang
 * @author Björn Jacobs
 * @author Mario Boley
 * 
 * @since 0.1.0
 * 
 * @version 0.1.2.1
 * 
 */
public interface ResultDefinition {

	/**
	 * Applies the result definition to a mining dashboard by retrieving all
	 * results in serializable form. If the task is not done (i.e., no or not
	 * enough results can be retrieved), throws an IllegalStateException.
	 * 
	 * @param miningDashboardModel
	 *            the mining dashboard from which results are to be retrieved
	 * @return all study results produced by the user of the mining dashboard
	 * 
	 * @throws IllegalStateException
	 *             if {link {@link #taskDone()==false}
	 */
	public List<PatternBuilder> retrieveResultBuilders(
			AnalyticsDashboard miningDashboardModel);

	public List<Integer> getListOfSecondsUntilSaved(
			AnalyticsDashboard miningDashboardModel);

	/**
	 * Checks if the task is done and results can be retrieved for the state of
	 * a given analytics dashboard.
	 * 
	 * @param trialDashboard
	 *            dashboard the state of which is used for check
	 * @return whether task is done and results can be retrieved
	 */
	public boolean taskDone(AnalyticsDashboard trialDashboard);

	/**
	 * Help message for assisting the user of a dashboard in a state, which is
	 * not done, as to how to finish the task.
	 * 
	 * @param trialDashboard
	 *            dashboard the state of which is subject of the help message
	 * 
	 * @return help message for assisting the user in finishing the task
	 *         (undefined if task is done)
	 */
	public String getHelp(AnalyticsDashboard trialDashboard);

}
