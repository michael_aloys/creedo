package de.unibonn.creedo.webapp.studies.rating;

/**
 * Represents different metrics that can be available in surveys for evaluating
 * patterns.
 * 
 * @author mboley, bjacobs, bkang
 */
public enum RatingMetric {
	VALIDITY {

		private RatingOption[] options = new RatingOption[] {
				new RatingOption("valid (1)", 1.0),
				new RatingOption("undecided (0)", 0.0),
				new RatingOption("invalid (-1)", -1.0) };

		@Override
		public String getName() {
			return "Validity";
		}

		@Override
		public String getDescription() {
			return "Do you consider this statement about the data as valid?";
		}

		@Override
		public RatingOption[] getRatingOptions() {
			return options;
		}

		@Override
		public RatingOption getDefault() {
			// return the undecided option as default
			return this.options[1];
		}
		
	},
	INTERESTINGNESS {

        private RatingOption[] options = new RatingOption[] {
                new RatingOption("interesting (2)", 2.0),
                new RatingOption("rather interesting (1)", 1.0),
                new RatingOption("undecided (0)", 0.0),
                new RatingOption("rather not interesting (-1)", -1.0),
                new RatingOption("not interesting (-2)", -2.0) };

		@Override
		public String getName() {
			return "Interestingness";
		}

		@Override
		public String getDescription() {
			return "Do you consider this statement about the data as interesting?";
		}

		@Override
		public RatingOption[] getRatingOptions() {
			return options;
		}

		@Override
		public RatingOption getDefault() {
            // return the undecided option as default
            return this.options[2];
		}
	},
    DIVERSITY {
        private RatingOption[] options = new RatingOption[] {
                new RatingOption("diverse (2)", 2.0),
                new RatingOption("rather diverse (1)", 1.0),
                new RatingOption("undecided (0)", 0.0),
                new RatingOption("rather not diverse (-1)", -1.0),
                new RatingOption("not diverse (-2)", -2.0) };
        @Override
        public String getName() {
            return "Diversity";
        }

        @Override
        public String getDescription() {
            return "Do you consider this statement reveals diverse aspects in assisting to answer the question?";
        }

        @Override
        public RatingOption[] getRatingOptions() {
            return options;
        }

        @Override
        public RatingOption getDefault() {
            // return the undecided option as default
            return this.options[2];
        }
    }, COMPLETENESS{
        private RatingOption[] options = new RatingOption[] {
                new RatingOption("complete (2)", 2.0),
                new RatingOption("rather complete (1)", 1.0),
                new RatingOption("undecided (0)", 0.0),
                new RatingOption("rather not complete (-1)", -1.0),
                new RatingOption("not complete (-2)", -2.0) };
        @Override
        public String getName() {
            return "Completeness";
        }

        @Override
        public String getDescription() {
            return "Do you consider this statement completely answers the asked question?";
        }

        @Override
        public RatingOption[] getRatingOptions() {
            return options;
        }

        @Override
        public RatingOption getDefault() {
            // return the undecided option as default
            return this.options[2];
        }
    }, CUMULATIVE_KNOWLEDGE{
        private RatingOption[] options = new RatingOption[] {
                new RatingOption("a lot (4)", 4.0),
                new RatingOption("some (3)", 3.0),
                new RatingOption("a little (2)", 2.0),
                new RatingOption("almost none (1)", 1.0),
                new RatingOption("none (0)", 0.0)
        };

        @Override
        public String getName() {
            return "Cumulative Knowledge";
        }

        @Override
        public String getDescription() {
            return "How much additional knowledge do you gain from the highlighted set of patterns " +
                    "in addition to the summary statistics of the attributes?";
        }

        @Override
        public RatingOption[] getRatingOptions() {
            return options;
        }


        @Override
        public RatingOption getDefault() {
            return null;
        }
    };

	public abstract String getName();

	public abstract String getDescription();

	public abstract RatingOption[] getRatingOptions();

    /**
     * Get the default rating option for a metric.
     * A default rating option of a metric can be set to null.
     *
     * @return default ration option.
     */
	public abstract RatingOption getDefault();
}
