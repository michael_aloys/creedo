package de.unibonn.creedo.webapp.studies.result;

import static com.google.common.base.Preconditions.checkState;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import com.google.common.collect.ImmutableList;

import de.unibonn.creedo.studies.designs.TaskSpecBuilder;
import de.unibonn.creedo.ui.mining.AnalyticsDashboard;
import de.unibonn.realkd.common.parameter.DefaultParameter;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.ParameterContainer;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.PatternBuilder;
import de.unibonn.realkd.patterns.patternset.PatternSets;

/**
 * Result definition that considers the pattern set of all individual patterns
 * stored by a user as one single result. The definition has two parameters: the
 * maximum and the minimum number of individual patterns that a user has to
 * store in order to successfully finish her task.
 * 
 * @author Bo Kang
 * @author Mario Boley
 * 
 * @since 0.1.0
 * 
 * @version 0.1.2.1
 * 
 */
public class PatternSetResultDefinition implements ResultDefinition, ParameterContainer {

	/**
	 * WARNING: changing the value of this constant will break deserialization
	 * of {@link TaskSpecBuilder}.
	 */
	public static final String MAX_NUMBER_OF_PATTERNS_PARAMETER_NAME = "Max. number of patterns";

	/**
	 * WARNING: changing the value of this constant will break deserialization
	 * of {@link TaskSpecBuilder}.
	 */
	public static final String MIN_NUMBER_OF_PATTERNS_PARAMETER_NAME = "Min. number of patterns";

	/**
	 * WARNING: changing the value of this constant will break deserialization
	 * of {@link TaskSpecBuilder}.
	 */
	public static final String STRING_NAME = "Pattern set of all stored patterns is single result";

	private final Parameter<Integer> minNumberOfPatternsParameter;

	private final Parameter<Integer> maxNumberOfPatternsParameter;

	public PatternSetResultDefinition() {
		this.minNumberOfPatternsParameter = new DefaultParameter<Integer>(MIN_NUMBER_OF_PATTERNS_PARAMETER_NAME,
				"The minimum number of pattern that a user has to store as results in order to finish the task.",
				Integer.class, input -> Integer.valueOf(input), x -> x > 0, "Must be integer no less then 1.", () -> 1);
		this.maxNumberOfPatternsParameter = new DefaultParameter<Integer>(MAX_NUMBER_OF_PATTERNS_PARAMETER_NAME,
				"The maximum number of patterns that a user is allowed to store in the result area in order to comply with the study result definition.",
				Integer.class, input -> Integer.valueOf(input),
				x -> x >= minNumberOfPatternsParameter.current(),
				"Provide integer no less than minimum number of patterns.",
				() -> minNumberOfPatternsParameter.current(), minNumberOfPatternsParameter);
	}

	@Override
	public List<PatternBuilder> retrieveResultBuilders(AnalyticsDashboard trialDashboard) {

		checkState(taskDone(trialDashboard));
		List<Pattern> patterns = trialDashboard.getResultPatterns();
		List<PatternBuilder> patternBuilders = new ArrayList<>();
		patternBuilders.add(PatternSets
				.createPatternSet(trialDashboard.getPropositionalLogic().population(), new HashSet<>(patterns))
				.toBuilder());
		return patternBuilders;
	}

	@Override
	public List<Integer> getListOfSecondsUntilSaved(AnalyticsDashboard trialDashboard) {
		List<Integer> result = new ArrayList<>();
		result.add(Collections.max(trialDashboard.getSecondsUntilSaved()));
		return result;
	}

	@Override
	public boolean taskDone(AnalyticsDashboard trialDashboard) {
		validate();
		List<Pattern> resultPatterns = trialDashboard.getMiningSystem().getDiscoveryProcess().getDiscoveryProcessState()
				.getResultPatterns();
		return (resultPatterns.size() >= minNumberOfPatternsParameter.current())
				&& (resultPatterns.size() <= maxNumberOfPatternsParameter.current());
	}

	@Override
	public String getHelp(AnalyticsDashboard trialDashboard) {
		validate();
		boolean maxEqualsMin = minNumberOfPatternsParameter.current()
				.equals(maxNumberOfPatternsParameter.current());
		String requiredNumberPart = maxEqualsMin ? "exactly " + minNumberOfPatternsParameter.current()
				: "at least " + minNumberOfPatternsParameter.current() + " and at most "
						+ maxNumberOfPatternsParameter.current();
		return "Please store " + requiredNumberPart + " patterns in the result area to finish your task.";
	}

	@Override
	public List<Parameter<?>> getTopLevelParameters() {
		return ImmutableList.of(minNumberOfPatternsParameter, maxNumberOfPatternsParameter);
	}

	@Override
	public String toString() {
		return STRING_NAME;
	}

}
