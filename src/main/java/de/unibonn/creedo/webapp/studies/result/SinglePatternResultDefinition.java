package de.unibonn.creedo.webapp.studies.result;

import static com.google.common.base.Preconditions.checkState;

import java.util.List;
import java.util.stream.Collectors;

import com.google.common.collect.ImmutableList;

import de.unibonn.creedo.studies.designs.TaskSpecBuilder;
import de.unibonn.creedo.ui.mining.AnalyticsDashboard;
import de.unibonn.realkd.common.parameter.DefaultParameter;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.ParameterContainer;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.PatternBuilder;

/**
 * Result definition that considers each individual pattern stored by a
 * participant as a trial result and has two parameters: the minimum and the
 * maximum number of results to be produced by the participant in order to
 * successfully finish her task.
 * 
 * @author Bo Kang
 * @author Mario Boley
 * 
 * @since 0.1.0
 * 
 * @version 0.1.2.1
 * 
 */
public class SinglePatternResultDefinition implements ResultDefinition,
		ParameterContainer {

	/**
	 * WARNING: changing the value of this constant will break deserialization
	 * of {@link TaskSpecBuilder}.
	 */
	public static final String MAX_NUMBER_OF_RESULTS_PARAMETER_NAME = "Max. number of results";

	/**
	 * WARNING: changing the value of this constant will break deserialization
	 * of {@link TaskSpecBuilder}.
	 */
	public static final String MIN_NUMBER_OF_RESULTS_PARAMETER_NAME = "Min. number of results";

	/**
	 * WARNING: changing the value of this constant will break deserialization
	 * of {@link TaskSpecBuilder}.
	 */
	public static final String STRING_NAME = "Single patterns are results";

	private final Parameter<Integer> minNumberOfResultsParameter;

	private final Parameter<Integer> maxNumberOfResultsParameter;

	public SinglePatternResultDefinition() {
		this.minNumberOfResultsParameter = new DefaultParameter<Integer>(
				MIN_NUMBER_OF_RESULTS_PARAMETER_NAME,
				"The minimum number of pattern that a user has to store as results in order to finish the task.",
				Integer.class, input -> Integer.valueOf(input), x -> x > 0,
				"Must be integer no less then 1.", () -> 1);
		this.maxNumberOfResultsParameter = new DefaultParameter<Integer>(
				MAX_NUMBER_OF_RESULTS_PARAMETER_NAME,
				"The maximum number of patterns that a user is allowed to store in the result area in order to comply with the study result definition.",
				Integer.class, input -> Integer.valueOf(input),
				x -> x >= minNumberOfResultsParameter.current(),
				"Provide integer no less than minimum number of patterns.",
				() -> minNumberOfResultsParameter.current(),
				minNumberOfResultsParameter);
	}

	@Override
	public List<PatternBuilder> retrieveResultBuilders(
			AnalyticsDashboard trialDashboard) {
		checkState(taskDone(trialDashboard));
		return trialDashboard.getResultPatterns().stream()
				.map(p -> p.toBuilder()).collect(Collectors.toList());
	}

	@Override
	public List<Integer> getListOfSecondsUntilSaved(
			AnalyticsDashboard trialDashboard) {
		return trialDashboard.getSecondsUntilSaved();
	}

	@Override
	public boolean taskDone(AnalyticsDashboard trialDashboard) {
		validate();
		List<Pattern> resultPatterns = trialDashboard.getMiningSystem()
				.getDiscoveryProcess().getDiscoveryProcessState()
				.getResultPatterns();
		return (resultPatterns.size() >= minNumberOfResultsParameter
				.current())
				&& (resultPatterns.size() <= maxNumberOfResultsParameter
						.current());
	}

	@Override
	public String getHelp(AnalyticsDashboard trialDashboard) {
		validate();
		boolean maxEqualsMin = minNumberOfResultsParameter.current()
				.equals(maxNumberOfResultsParameter.current());
		String requiredNumberPart = maxEqualsMin ? "exactly "
				+ minNumberOfResultsParameter.current() : "at least "
				+ minNumberOfResultsParameter.current()
				+ " and at most "
				+ maxNumberOfResultsParameter.current();
		return "Please store " + requiredNumberPart
				+ " patterns in the result area to finish your task.";
	}

	@Override
	public List<Parameter<?>> getTopLevelParameters() {
		return ImmutableList.of(minNumberOfResultsParameter,
				maxNumberOfResultsParameter);
	}

	@Override
	public String toString() {
		return STRING_NAME;
	}

}
