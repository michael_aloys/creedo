package de.unibonn.creedo.webapp.studies;

/**
 * Container for mybatis query results of study trial session results.
 * 
 * @author Björn Jacobs
 * 
 * @since 0.1.0
 * 
 * @version 0.1.2.1
 *
 */
public class ResultDataContainer {
	private int resultId;
	private int sessionId;
	private String resultBuilderContent;
	private int secondsInSessionUntilSaved;

	public ResultDataContainer() {
	}

	public int getSessionId() {
		return sessionId;
	}

	public void setSessionId(int sessionId) {
		this.sessionId = sessionId;
	}

	public int getResultId() {
		return resultId;
	}

	public void setResultId(int resultId) {
		this.resultId = resultId;
	}

	public String getResultBuilderContent() {
		return resultBuilderContent;
	}

	public void setResultBuilderContent(String resultBuilderContent) {
		this.resultBuilderContent = resultBuilderContent;
	}

	public int getSecondsInSessionUntilSaved() {
		return secondsInSessionUntilSaved;
	}

	public void setSecondsInSessionUntilSaved(int secondsInSessionUntilSaved) {
		this.secondsInSessionUntilSaved = secondsInSessionUntilSaved;
	}
}
