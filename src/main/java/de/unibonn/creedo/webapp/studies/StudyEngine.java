package de.unibonn.creedo.webapp.studies;

import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import de.unibonn.creedo.ApplicationRepositories;
import de.unibonn.creedo.repositories.RepositoryEntry;
import de.unibonn.creedo.studies.Study;
import de.unibonn.creedo.studies.StudyBuilder;
import de.unibonn.creedo.ui.indexpage.DashboardLinkEntry;
import de.unibonn.creedo.ui.indexpage.DashboardLinkProvider;
import de.unibonn.creedo.webapp.CreedoSession;

/**
 * Builds all study-related dashboard links for a user.
 *
 * @author Mario Boley
 * @author Björn Jacobs
 * 
 * @since 0.1.0
 * 
 * @version 0.1.2.1
 * 
 */
public class StudyEngine implements DashboardLinkProvider {

	private static final Logger LOGGER = Logger.getLogger(StudyEngine.class.getName());

	public static final StudyEngine INSTANCE = new StudyEngine();

	private StudyEngine() {
		;
	}

	public List<DashboardLinkEntry> getDashboardLinks(CreedoSession session) {
		LOGGER.log(Level.FINE, "Start creating study assignment links");
		List<DashboardLinkEntry> results = new ArrayList<>();
		for (RepositoryEntry<String, StudyBuilder> studyEntry : ApplicationRepositories.STUDY_REPOSITORY
				.getAllEntries()) {
			LOGGER.fine(() -> "Start materializing study '" + studyEntry.getId() + "'");
			Study study = studyEntry.getContent().build();
			LOGGER.fine(() -> "Done materializing study '" + studyEntry.getId() + "'");
			results.addAll(study.getDashboardLinks(session.getUser()).stream()
					.map(l -> DashboardLinkEntry.dashboardLinkEntry(l, session)).collect(toList()));
		}
		LOGGER.fine("Done creating study assignment links");
		return results;
	}

	@Override
	public String getTitle() {
		return "Study Assignments";
	}
}
