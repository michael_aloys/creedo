package de.unibonn.creedo.webapp;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.JsonObject;

import de.unibonn.creedo.Creedo;
import de.unibonn.creedo.admin.users.DefaultUserGroup;

/**
 * This controller handles the requests made from the Study Administration
 * dashboard.
 *
 * @author bkang
 */
@Controller()
@RequestMapping(value = "/studyAdminDash")
public class StudyAdminDashboardController {

	public static final String GET_STUDY_ADMIN_GENERAL_INFO_URL = "studyGeneralInfo.htm";

	public static final String GET_STUDY_ADMIN_PARTICIPANTS_INFO_URL = "studyParticipantsInfo.htm";

	public static final String GET_STUDY_ADMIN_DASHBOARD_URL = "studyAdminDash.htm";

	@Autowired
	private HttpSession session;

	// private StudyDAO studyDAO = new StudyDAO();
	//
	// private UserDAO userDAO = new UserDAO();
	//
	// private SessionDAO sessionDAO = new SessionDAO();
	//
	// private AssignmentGroupDAO assignmentGroupDAO = new AssignmentGroupDAO();

	@RequestMapping(value = "/" + GET_STUDY_ADMIN_GENERAL_INFO_URL, method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<String> getSelectedStudyInfo(
			@RequestParam(value = "selectedStudyId", required = true) Integer selectedStudyId)
			throws Exception {
		CreedoSession creedoSession = Creedo.getCreedoSession(session);
		if (!creedoSession.getUser().groups()
				.contains(DefaultUserGroup.ADMINISTRATOR)) {
			return new ResponseEntity<>(
					"Request forbidden: not administrator.",
					HttpStatus.BAD_REQUEST);
		}

		// Study selectedStudy = studyDAO.getStudyById(selectedStudyId);
		//
		JsonObject js = new JsonObject();
		//
		// js.addProperty("id", selectedStudy.getStudyId());
		// js.addProperty("name", selectedStudy.getName());
		// js.addProperty("imageURL", selectedStudy.getImageURL());

		try {
			return new ResponseEntity<>(js.toString(), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}

	// @RequestMapping(value = "/"+ GET_STUDY_ADMIN_PARTICIPANTS_INFO_URL,
	// method = RequestMethod.GET)
	// @ResponseBody
	// public ResponseEntity<String> getParticipantsInfo(
	// @RequestParam(value = "selectedStudyId", required = true) Integer
	// selectedStudyId, Model model)
	// throws Exception {
	// CreedoSession creedoSession = CreedoSession.getCreedoSession(session);
	// if (!creedoSession.getUser().isAdministrator()) {
	// return new ResponseEntity<>("Request forbidden: not administrator.",
	// HttpStatus.BAD_REQUEST);
	// }
	//
	// Study selectedStudy = studyDAO.getStudyById(selectedStudyId);
	// List<StudyToUser> participantsInfo = new ArrayList<>();
	//
	// // Get all distinct users in study
	// List<Integer> userIdsOfStudy =
	// studyDAO.getDistinctUserIdsOfStudy(selectedStudy.getStudyId());
	//
	// for (Integer id : userIdsOfStudy) {
	// User user = userDAO.getUserById(id);
	//
	// // get AssignmentGroups in study for this user
	// List<AssignmentGroup> groupsForUserInStudy =
	// assignmentGroupDAO.getGroupsForUserInStudy(user, selectedStudy);
	//
	// for (AssignmentGroup assignmentGroup : groupsForUserInStudy) {
	// // Check for participation flag
	// int numberOfSessions =
	// sessionDAO.getNumberOfSessions(selectedStudy.getStudyId(), user.getId());
	//
	// // System.out.println(numberOfSessions);
	// StudyToUser studyToUser = new StudyToUser(user, assignmentGroup,
	// numberOfSessions > 0);
	// participantsInfo.add(studyToUser);
	// }
	// }
	// Gson gson = new Gson();
	// try {
	// return new ResponseEntity<>(gson.toJson(participantsInfo),
	// HttpStatus.OK);
	// } catch(Exception e) {
	// e.printStackTrace();
	// return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
	// }
	// }

}
