package de.unibonn.creedo.webapp.utils;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import de.unibonn.creedo.webapp.dashboard.patterncontainer.WebPattern;

import org.jfree.chart.JFreeChart;

import de.unibonn.realkd.visualization.pattern.PatternVisualization;
import de.unibonn.realkd.visualization.pattern.RegisteredPatternVisualization;

/**
 * User: bjacobs Date: 28.05.14 Time: 12:27
 */

public class VisualizationProvider {

	public static final int DETAILED_DRAWING_WIDTH = 1000;
	public static final int DETAILED_DRAWING_HEIGHT = 620; // Golden ratio

	public static List<String> createDetailedPatternImages(HttpSession session,
			WebPattern webPattern) {
		ArrayList<String> vImages = new ArrayList<>();

		for (PatternVisualization visualization : RegisteredPatternVisualization
				.values()) {
			if (visualization.isApplicable(webPattern.getPattern())) {
				JFreeChart chart = visualization.drawDetailed(webPattern.getPattern());
				vImages.add(ServerVisualizationTools.serveChartAsPNG(session,
						chart, DETAILED_DRAWING_WIDTH, DETAILED_DRAWING_HEIGHT));
			}
		}

		return vImages;
	}
}
