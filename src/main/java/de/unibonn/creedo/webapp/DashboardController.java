package de.unibonn.creedo.webapp;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import de.unibonn.creedo.Creedo;
import de.unibonn.creedo.ui.core.UiComponent;
import de.unibonn.creedo.ui.mining.AnalyticsDashboard;
import de.unibonn.creedo.webapp.dashboard.patterncontainer.WebPattern;

@Controller
public class DashboardController {

	// public static final String GET_ADMIN_DASHBOARD_URL =
	// "adminDashboard.htm";

	public static final String GET_CUSTOM_DASHBOARD_CREATION_URL = "customDashboardCreation.htm";

	public static final String GET_LOADINGSCREEN_URL = "loading.htm";

	public static final String GET_DASHBOARD_URL = "dashboard.htm";

	public static final String GET_EXPORT_PATTERNS_URL = "exportPatterns.htm";

	@Autowired
	private HttpSession session;

	public DashboardController() {
	}

	/**
	 * get the mining session id from session retrieve it from database and
	 * build the dashboard for that mining session
	 */
	@RequestMapping(value = "/" + GET_DASHBOARD_URL, method = RequestMethod.GET)
	public ModelAndView mining() {
		System.out.println("OLD DASHBOARD REQUEST!!!!!!!!!");
		CreedoSession uiSession = Creedo.getCreedoSession(session);
		if (uiSession.getUiRegister().analyticsDashboardOpen())
			return new ModelAndView("redirect:index.htm");

		return uiSession.getUiRegister().getDashboard().getModelAndView();
	}

	@RequestMapping(value = GET_EXPORT_PATTERNS_URL, method = RequestMethod.GET)
	public void getResultPatternFile(
			@RequestParam("analyticsDashboardId") int analyticsDashboardId,
			HttpServletResponse response) {
		CreedoSession creedoSession = Creedo.getCreedoSession(session);

		UiComponent analyticsDashboard = creedoSession
				.getUiComponent(analyticsDashboardId);

		if (analyticsDashboard instanceof AnalyticsDashboard) {
			try {
				PrintWriter writer = response.getWriter();
				for (WebPattern pattern : ((AnalyticsDashboard) analyticsDashboard)
						.getResults()) {
					writer.print(pattern.toString());
					writer.print("\r\n");
				}
			} catch (IOException e) {
				e.printStackTrace();
			}

			response.setContentType("application/x-download");
			response.setHeader("Content-Disposition",
					"filename=exportedPatterns.txt");
		} else {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		}
	}

	@RequestMapping(value = "/" + GET_LOADINGSCREEN_URL, method = RequestMethod.GET)
	public String loadingScreen() {
		return "util/loading";
	}
}
