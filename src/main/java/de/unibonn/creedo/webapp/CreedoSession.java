package de.unibonn.creedo.webapp;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpSession;

import de.unibonn.creedo.admin.users.CreedoUser;
import de.unibonn.creedo.boot.DefaultMainFrameBuilder;
import de.unibonn.creedo.ui.CustomDashboardCreationPage;
import de.unibonn.creedo.ui.core.PageFrame;
import de.unibonn.creedo.ui.core.UiComponent;
import de.unibonn.creedo.ui.core.UiRegister;

/**
 * Holds the state of the application session. Requesting the current
 * application session is the entry point in to the application logic for all
 * application requests (different from root requests for application setup).
 * 
 * There is at most one CreedoSession bound to the HttpSession trough which a
 * request is issued.
 * 
 * @author Mario Boley
 * 
 * @since 0.1.0
 * 
 * @version 0.4.0
 * 
 */
public class CreedoSession {

	private static final Logger LOGGER = Logger.getLogger(CreedoSession.class.getName());

	public static final String CREEDO_SESSION_HTTPSESSION_PROPERTY_NAME = "uisession";

	private final int loadLoginPageActionId;

	private final CreedoUser user;

	private final HttpSession httpSession;

	private final UiRegister uiRegister;

	public static Object retrieveObjectWithCreedoSessionKey(HttpSession httpSession) {
		return httpSession.getAttribute(CreedoSession.CREEDO_SESSION_HTTPSESSION_PROPERTY_NAME);
	}

	/**
	 * Creates a new creedo session for user and binds it to httpSession
	 * (replacing old session).
	 * 
	 * NOTE: old session should also be invalidated and resources freed up
	 * 
	 */
	public static void createNewCreedoSession(CreedoUser user, HttpSession httpSession) {
		CreedoSession uiSession = new CreedoSession(user, httpSession);
		httpSession.setAttribute(CREEDO_SESSION_HTTPSESSION_PROPERTY_NAME, uiSession);
		LOGGER.log(Level.INFO, "Created new Creedo session for user '" + user.id() + "'");
	}

	private CreedoSession(CreedoUser user, HttpSession httpSession) {
		this.httpSession = httpSession;
		this.user = user;
		this.uiRegister = new UiRegister(httpSession);
		// reserve id for showing log in page that is used by static url
		this.loadLoginPageActionId = uiRegister.getNextId();

		customDashboardCreationPage = new CustomDashboardCreationPage(uiRegister.getNextId());

		this.mainFrame = new DefaultMainFrameBuilder().build(this);
		
	}

	public int getLoadLoginPageActionId() {
		return loadLoginPageActionId;
	}

	public CreedoUser getUser() {
		return user;
	}

	private PageFrame mainFrame = null;

	private CustomDashboardCreationPage customDashboardCreationPage = null;

	public PageFrame getMainFrame() {
		return mainFrame;
	}

	public CustomDashboardCreationPage getCustomDashboardCreationPage() {
		return customDashboardCreationPage;
	}

	public UiRegister getUiRegister() {
		return this.uiRegister;
	}

	public UiComponent getUiComponent(int componentId) {
		return uiRegister.retrieveUiComponent(componentId);
	}

	public HttpSession getHttpSession() {
		return httpSession;
	}
}
