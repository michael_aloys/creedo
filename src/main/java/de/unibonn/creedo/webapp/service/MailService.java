package de.unibonn.creedo.webapp.service;

import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;

import de.unibonn.creedo.admin.users.CreedoUser;

/**
 * User: paveltokmakov Date: 8/12/13
 */
public class MailService {

	private MailSender mailSender;

	private String senderEmail;

	// @Autowired
	// private UserDAO userDao;

	public void sendActivationEmail(CreedoUser user, String baseURL) {
		// List<User> administrators = userDao.getAdministrators();
		//
		// for(User administrator: administrators) {
		// sendActivationToAdmin(administrator, user, baseURL);
		// }
	}

	private void sendActivationToAdmin(CreedoUser administrator, CreedoUser user, String baseURL) {
		SimpleMailMessage message = new SimpleMailMessage();

		message.setFrom(senderEmail);
		message.setTo(administrator.id());
		message.setSubject("New registration! User name: " + user.id());
		String activationURL = baseURL + "/activate.htm?userId=" + user.id();
		message.setText("New user has been registered: " + user.id() + ". Click the following link to activate: "
				+ activationURL);
		mailSender.send(message);
	}

	public void sendConfirmationEmail(CreedoUser user) {
		SimpleMailMessage message = new SimpleMailMessage();

		message.setFrom(senderEmail);
		message.setTo(user.id());
		message.setSubject("Activation Confirmation");
		message.setText("Your account has been activated.");
		mailSender.send(message);
	}

	public void setMailSender(MailSender mailSender) {
		this.mailSender = mailSender;
	}

	public void setSenderEmail(String senderEmail) {
		this.senderEmail = senderEmail;
	}

}
