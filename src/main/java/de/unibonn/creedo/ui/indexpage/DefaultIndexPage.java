package de.unibonn.creedo.ui.indexpage;

import static de.unibonn.creedo.admin.users.Users.DEFAULT_USER;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.validation.support.BindingAwareModelMap;

import com.google.common.collect.ImmutableList;

import de.unibonn.creedo.admin.users.CreedoUser;
import de.unibonn.creedo.ui.core.Action;
import de.unibonn.creedo.ui.core.ActionProvider;
import de.unibonn.creedo.ui.core.Page;
import de.unibonn.creedo.webapp.CreedoSession;
import de.unibonn.creedo.webapp.DemoProvider;
import de.unibonn.creedo.webapp.studies.StudyEngine;

/**
 * Page that displays some static content as well as links to all demos in db to
 * a logged in user (or a log-in invitation when user is not logged in).
 * 
 * @author Mario Boley
 * 
 * @since 0.1.0
 * 
 * @version 0.4.0
 *
 */
public class DefaultIndexPage implements Page, ActionProvider {

	public class DashboardLinkSection {

		private final String title;
		private final List<DashboardLinkEntry> entries;

		public DashboardLinkSection(String title, List<DashboardLinkEntry> actions) {
			this.title = title;
			this.entries = actions;
		}

		public String getTitle() {
			return title;
		}

		public List<DashboardLinkEntry> getEntries() {
			return entries;
		}

	}

	private static final List<DashboardLinkProvider> DASHBOARD_PROVIDER = ImmutableList.of(StudyEngine.INSTANCE,
			StoredDashboardProvider.INSTANCE, DemoProvider.INSTANCE);

	private final String title;

	private final Model model;

	private final String referenceName;

	private final Map<Integer, Action> idToActions;

	private final CreedoSession session;

	private final String loginInviteHtml;

	public DefaultIndexPage(CreedoSession session, String title, String referenceName, String contentPageContent,
			String loginInvite) {
		this.session = session;
		this.idToActions = new HashMap<>();
		this.model = new BindingAwareModelMap();
		this.title = title;
		this.referenceName = referenceName;
		this.model.addAttribute("mainContent", contentPageContent);
		this.loginInviteHtml = loginInvite;
	}

	@Override
	public String getTitle() {
		return title;
	}

	@Override
	public String getViewImport() {
		return "indexPage.jsp";
	}

	@Override
	public Model getModel() {
		CreedoUser user = session.getUser();
		if (user.equals(DEFAULT_USER)) {
			model.addAttribute("loginInvitationHtml", this.loginInviteHtml);
		}
		List<DashboardLinkSection> entrySections = new ArrayList<>();

		DASHBOARD_PROVIDER.forEach(provider -> {
			DashboardLinkSection section = compileSection(user, provider);
			if (section.entries.size() > 0) {
				entrySections.add(section);
			}
		});

		this.model.addAttribute("actionSections", entrySections);

		return model;
	}

	private DashboardLinkSection compileSection(CreedoUser user, DashboardLinkProvider linkProvider) {
		List<DashboardLinkEntry> dashboardLinks = new ArrayList<>();
		dashboardLinks.addAll(linkProvider.getDashboardLinks(session));

		for (DashboardLinkEntry entry : dashboardLinks) {
			Action openLinkAction = entry.getOpenAction();
			idToActions.put(openLinkAction.getId(), openLinkAction);
			// is ugly because JSP interface object
			if (entry.getRenameAction() != null) {
				idToActions.put(entry.getRenameAction().getId(), entry.getRenameAction());
			}
			for (Action action : entry.getOtherActions()) {
				idToActions.put(action.getId(), action);
			}
		}
		DashboardLinkSection linkSection = new DashboardLinkSection(linkProvider.getTitle(), dashboardLinks);
		return linkSection;
	}

	@Override
	public String getReferenceName() {
		return referenceName;
	}

	@Override
	public Collection<Integer> getActionIds() {
		return idToActions.keySet();
	}

	@Override
	public ResponseEntity<String> performAction(int id, String... params) {
		Action action = idToActions.get(id);
		if (action == null) {
			throw new IllegalArgumentException("No action with id " + id);
		}
		return action.activate(params);
	}

	@Override
	public boolean isActionAvailable(int id) {
		return idToActions.containsKey(id);
	}

}
