package de.unibonn.creedo.ui.indexpage;

import static de.unibonn.realkd.common.parameter.Parameters.stringParameter;

import java.nio.file.Path;
import java.util.List;
import java.util.function.Supplier;

import com.google.common.collect.ImmutableList;

import de.unibonn.creedo.ApplicationRepositories;
import de.unibonn.creedo.admin.users.CreedoUser;
import de.unibonn.creedo.admin.users.Users;
import de.unibonn.creedo.repositories.IdentifierInRepositoryParameter;
import de.unibonn.creedo.repositories.Repositories;
import de.unibonn.creedo.ui.core.PreparedFrame;
import de.unibonn.creedo.webapp.dashboard.mining.DefaultAnalyticsDashboardBuilder;
import de.unibonn.creedo.webapp.dashboard.mining.MiningSystemBuilder;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.ParameterContainer;
import de.unibonn.realkd.common.parameter.Parameters;
import de.unibonn.realkd.common.parameter.ValueValidator;
import de.unibonn.realkd.common.workspace.Workspace;
import de.unibonn.realkd.common.workspace.Workspaces;

/**
 * Serial form of a link to a stored dashboard (based on a workspace stored in
 * the file system).
 * 
 * @author Mario Boley
 * 
 * @since 0.4.0
 * 
 * @version 0.4.0
 *
 */
public class StoredDashboardLinkBuilder implements Supplier<DashboardLink>, ParameterContainer {

	private final Parameter<String> titleParameter;

	private final Parameter<String> descriptionParameter;

	private final Parameter<String> imagePathParameter;

	private final Parameter<String> imageCreditsParameter;

	private final Parameter<String> analyticsDashboardParameter;

	private final Parameter<String> workspaceFolderParameter;

	private final Parameter<String> userParameter;

	public StoredDashboardLinkBuilder() {
		titleParameter = Parameters.stringParameter("Name", "Name of this dashboard.", "", v -> !v.isEmpty(),
				"Value must be non-empty.");
		descriptionParameter = Parameters.stringParameter("Description", "Description of dashboard contents.", "",
				v -> true, "");

		imagePathParameter = new IdentifierInRepositoryParameter<Path>("Image", "Image file for the demo link.",
				ApplicationRepositories.CONTENT_FOLDER_REPOSITORY,
				Repositories.getIdIsFilenameWithExtensionPredicate(Repositories.IMAGE_FILE_EXTENSIONS));

		imageCreditsParameter = stringParameter("Image credits",
				"Can be used to display credit information for the image.", "", ValueValidator.VALUE_NOT_NULL_VALIDATOR,
				"");

		analyticsDashboardParameter = new IdentifierInRepositoryParameter<MiningSystemBuilder<?>>("Analytics dashboard",
				"Mining system to be used for dashboard.", ApplicationRepositories.MINING_SYSTEM_REPOSITORY);

		workspaceFolderParameter = new IdentifierInRepositoryParameter<Path>("Workspace folder",
				"The data workspace configuration to be used for demo.",
				ApplicationRepositories.WORKSPACE_FOLDER_REPOSITORY);

		userParameter = Parameters.rangeEnumerableParameter("User", "The user for which this dashboard is visible",
				String.class, () -> Users.ids());
	}

	public StoredDashboardLinkBuilder title(String title) {
		titleParameter.set(title);
		return this;
	}

	public StoredDashboardLinkBuilder description(String description) {
		descriptionParameter.set(description);
		return this;
	}

	public StoredDashboardLinkBuilder imagePath(String imagePath) {
		imagePathParameter.set(imagePath);
		return this;
	}

	public StoredDashboardLinkBuilder imageCredits(String imageCredits) {
		imageCreditsParameter.set(imageCredits);
		return this;
	}

	public StoredDashboardLinkBuilder miningSystem(String miningSystemId) {
		analyticsDashboardParameter.set(miningSystemId);
		return this;
	}

	public StoredDashboardLinkBuilder workspaceDirectory(String directory) {
		workspaceFolderParameter.set(directory);
		return this;
	}

	public StoredDashboardLinkBuilder user(CreedoUser user) {
		userParameter.set(user.id());
		return this;
	}

	public String userId() {
		return userParameter.current();
	}

	@Override
	public DashboardLink get() {
		validate();
		MiningSystemBuilder<?> miningSystemBuilder = ApplicationRepositories.MINING_SYSTEM_REPOSITORY
				.getEntry(analyticsDashboardParameter.current()).getContent();

		Supplier<Workspace> dataSupplier = () -> Workspaces.fileSystemBasedWorkspace(
				ApplicationRepositories.WORKSPACE_FOLDER_REPOSITORY.get(workspaceFolderParameter.current()));

		PreparedFrame frameBuilder = new DefaultAnalyticsDashboardBuilder(dataSupplier, miningSystemBuilder);

		return new DashboardLink(titleParameter.current(), descriptionParameter.current(), imagePathParameter.current(),
				imageCreditsParameter.current(), frameBuilder);
	}

	@Override
	public List<Parameter<?>> getTopLevelParameters() {
		return ImmutableList.of(titleParameter, descriptionParameter, imagePathParameter, imageCreditsParameter,
				analyticsDashboardParameter, workspaceFolderParameter, userParameter);
	}

}
