package de.unibonn.creedo.ui.indexpage;

import de.unibonn.creedo.ui.core.Action;
import de.unibonn.creedo.ui.core.PreparedFrame;

/**
 * Link for predefined analytics dashboards that can be opened from the index
 * page.
 * 
 * @author Björn Jacobs
 * @author Mario Boley
 * 
 * @see {@link DefaultIndexPage}
 * 
 * @since 0.1.0
 * 
 * @version 0.1.0.1
 * 
 */
public class DashboardLink {
	private final String title;
	private final String description;
	private final String imgPath;
	private final String imgCredits;
	private final PreparedFrame analyticsDashboardBuilder;
	private final Action[] optionalActions;

	public DashboardLink(String title, String description, String imgPath, String imgCredits,
			PreparedFrame analyticsDashboardBuilder, Action... optionalActions) {
		this.title = title;
		this.description = description;
		this.imgPath = imgPath;
		this.imgCredits = imgCredits;
		this.analyticsDashboardBuilder = analyticsDashboardBuilder;
		this.optionalActions = optionalActions;
	}

	public String getTitle() {
		return title;
	}

	public String getDescription() {
		return description;
	}

	public String getImgPath() {
		return imgPath;
	}

	public String getImgCredits() {
		return imgCredits;
	}

	public PreparedFrame getAnalyticsDashboardBuilder() {
		return analyticsDashboardBuilder;
	}

	public Action[] optionalActions() {
		return optionalActions;
	}
}
