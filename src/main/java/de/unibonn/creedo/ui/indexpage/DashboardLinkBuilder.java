package de.unibonn.creedo.ui.indexpage;

import static de.unibonn.realkd.common.parameter.Parameters.stringParameter;

import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Supplier;
import java.util.logging.Logger;
import java.util.stream.Stream;

import com.google.common.collect.ImmutableSet;

import de.unibonn.creedo.ApplicationRepositories;
import de.unibonn.creedo.admin.options.Configurable;
import de.unibonn.creedo.admin.users.CreedoUser;
import de.unibonn.creedo.admin.users.DefaultUserGroup;
import de.unibonn.creedo.admin.users.UserGroup;
import de.unibonn.creedo.admin.users.UserGroups;
import de.unibonn.creedo.boot.FixedComponentWorkspaceBuilder;
import de.unibonn.creedo.repositories.IdentifierInRepositoryParameter;
import de.unibonn.creedo.repositories.Repositories;
import de.unibonn.creedo.setup.ServerPaths;
import de.unibonn.creedo.ui.core.PreparedFrame;
import de.unibonn.creedo.webapp.dashboard.mining.DefaultAnalyticsDashboardBuilder;
import de.unibonn.creedo.webapp.dashboard.mining.MiningSystemBuilder;
import de.unibonn.realkd.common.RuntimeBuilder;
import de.unibonn.realkd.common.parameter.DefaultParameterContainer;
import de.unibonn.realkd.common.parameter.DefaultSubCollectionParameter;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.ParameterContainer;
import de.unibonn.realkd.common.parameter.ValueValidator;
import de.unibonn.realkd.common.workspace.Workspace;

/**
 * <p>
 * Represents the configuration for a link to an analytics dashboard that can be
 * displayed on the index page.
 * </p>
 * <p>
 * WARNING: this class is referenced directly (by name) in the database and
 * Creedo will attempt to create instances of it on access of the demo
 * repository. Moves and renames of this class demand database/database init
 * script updates.
 * </p>
 * 
 * @author Mario Boley
 * 
 * @since 0.1.0
 * 
 * @version 0.1.1
 *
 */
public class DashboardLinkBuilder implements RuntimeBuilder<DashboardLink, CreedoUser>, ParameterContainer,
		Configurable<DemoDashboardLinkOptions> {

	private static final Logger LOGGER = Logger.getLogger(DashboardLinkBuilder.class.getName());

	private final Parameter<String> titleParameter;

	private final Parameter<String> descriptionParameter;

	private final Parameter<String> imagePathParameter;

	private final Parameter<String> imageCreditsParameter;

	private final Parameter<String> analyticsDashboardParameter;

	private final Parameter<String> dataParameter;

	private final Parameter<Set<UserGroup>> userGroupParameter;

	private final DefaultParameterContainer defaultParameterContainer;

	public DashboardLinkBuilder() {
		titleParameter = stringParameter("Title", "The title of the demo link.", "",
				ValueValidator.VALUE_NOT_NULL_VALIDATOR, "");
		descriptionParameter = stringParameter("Description",
				"A short description that explains to users what the demo is about.", "",
				ValueValidator.VALUE_NOT_NULL_VALIDATOR, "");

		imagePathParameter = new IdentifierInRepositoryParameter<Path>("Image", "Image file for the demo link.",
				ApplicationRepositories.CONTENT_FOLDER_REPOSITORY,
				Repositories.getIdIsFilenameWithExtensionPredicate(Repositories.IMAGE_FILE_EXTENSIONS));

		imageCreditsParameter = stringParameter("Image credits",
				"Can be used to display credit information for the image.", "", ValueValidator.VALUE_NOT_NULL_VALIDATOR,
				"");

		analyticsDashboardParameter = new IdentifierInRepositoryParameter<MiningSystemBuilder<?>>("Analytics dashboard",
				"Id of analytics dashboard to be used for demo.", ApplicationRepositories.MINING_SYSTEM_REPOSITORY);

		dataParameter = new IdentifierInRepositoryParameter<Supplier<Workspace>>("Data",
				"The data workspace configuration to be used for demo.", ApplicationRepositories.DATA_REPOSITORY);

		userGroupParameter = DefaultSubCollectionParameter.subSetParameter("User groups",
				"The user groups for which the link will be visible.", () -> UserGroups.get().all(),
				() -> ImmutableSet.of(DefaultUserGroup.REGISTERED));

		defaultParameterContainer = new DefaultParameterContainer();
		defaultParameterContainer
				.addAllParameters(Arrays.asList((Parameter<?>) titleParameter, descriptionParameter, imagePathParameter,
						imageCreditsParameter, analyticsDashboardParameter, dataParameter, userGroupParameter));
	}

	@Override
	public DashboardLink build(CreedoUser user) {
		validate();
		MiningSystemBuilder<?> miningSystemBuilder = ApplicationRepositories.MINING_SYSTEM_REPOSITORY
				.getEntry(analyticsDashboardParameter.current()).getContent();

		Supplier<Workspace> dataSupplier = ApplicationRepositories.DATA_REPOSITORY.getEntry(dataParameter.current())
				.getContent();

		Optional<Runnable> onOpenAction = (getOptions().buildCopyOnOpen()
				&& dataSupplier instanceof FixedComponentWorkspaceBuilder) ? Optional.of(() -> {
					Optional<Path> path = ServerPaths.newWorkspaceDirectory(user);
					if (!path.isPresent()) {
						LOGGER.warning("Could not acquire path for storing dashboard data.");
						return;
					}
					((FixedComponentWorkspaceBuilder) dataSupplier).setBackupPath(path.get());
					LOGGER.info("Set backup path for workspace to '" + path.get() + "'");
					StoredDashboardLinkBuilder storedDbLinkBuilder = new StoredDashboardLinkBuilder()
							.title(path.get().getFileName().toString())
							.description("Derived from: " + titleParameter.current())
							.imageCredits(imageCreditsParameter.current()).imagePath(imagePathParameter.current())
							.miningSystem(analyticsDashboardParameter.current())
							.workspaceDirectory(path.get().getFileName().toString()).user(user);
					ApplicationRepositories.STORED_DASHBOARD_REPOSITORY.add(path.get().getFileName().toString(),
							storedDbLinkBuilder);
				}) : Optional.empty();

		PreparedFrame frameBuilder = new DefaultAnalyticsDashboardBuilder(dataSupplier, miningSystemBuilder,
				onOpenAction);

		return new DashboardLink(titleParameter.current(), descriptionParameter.current(), imagePathParameter.current(),
				imageCreditsParameter.current(), frameBuilder);
	}

	public boolean isVisibleTo(CreedoUser user) {
		Stream<UserGroup> permittedGroups = userGroupParameter.current().stream();
		return permittedGroups.filter(x -> user.groups().contains(x)).findAny().isPresent();
	}

	@Override
	public List<Parameter<?>> getTopLevelParameters() {
		return defaultParameterContainer.getTopLevelParameters();
	}

	@Override
	public DemoDashboardLinkOptions getDefaultOptions() {
		return new DemoDashboardLinkOptions();
	}

}
