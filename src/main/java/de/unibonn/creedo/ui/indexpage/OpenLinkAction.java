package de.unibonn.creedo.ui.indexpage;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import de.unibonn.creedo.DynamicUrlController;
import de.unibonn.creedo.ui.core.Action;
import de.unibonn.creedo.webapp.CreedoSession;

public class OpenLinkAction implements Action {

	public static OpenLinkAction openLinkAction(CreedoSession session, DashboardLink link) {
		return new OpenLinkAction(session, link);
	}

	private final DashboardLink dashboardLink;

	private final int id;

	private final CreedoSession creedoSession;

	public DashboardLink getDashboardLink() {
		return dashboardLink;
	}

	private OpenLinkAction(CreedoSession session, DashboardLink link) {
		this.dashboardLink = link;
		this.id = session.getUiRegister().getIdGenerator().getNextId();
		this.creedoSession = session;
	}

	@Override
	public String getReferenceName() {
		return dashboardLink.getTitle();
	}

	@Override
	public ResponseEntity<String> activate(String... params) {
		if (creedoSession.getUiRegister().analyticsDashboardOpen()) {
			return new ResponseEntity<String>("Another analytics dashboard already open. Please close first.",
					HttpStatus.FORBIDDEN);
		}
		creedoSession.getUiRegister().createMiningDashboard(dashboardLink.getAnalyticsDashboardBuilder());
		return new ResponseEntity<String>(
				DynamicUrlController.frameUrl(creedoSession.getUiRegister().getDashboard().getId()), HttpStatus.OK);
	}

	@Override
	public ClientWindowEffect getEffect() {
		return ClientWindowEffect.POPUP;
	}

	@Override
	public int getId() {
		return id;
	}

}