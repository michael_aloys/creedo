package de.unibonn.creedo.ui.indexpage;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.apache.commons.io.FileUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import de.unibonn.creedo.ApplicationRepositories;
import de.unibonn.creedo.repositories.RepositoryEntry;
import de.unibonn.creedo.setup.ServerPaths;
import de.unibonn.creedo.ui.core.Action;
import de.unibonn.creedo.ui.core.Action.ClientWindowEffect;
import de.unibonn.creedo.ui.core.DefaultAction;
import de.unibonn.creedo.webapp.CreedoSession;

/**
 * <p>
 * Serves dashboard links from the database repository of stored dashboard
 * links. Provided links have rename and delete action so that user can manage
 * his stored dashboard links from the index page.
 * </p>
 * 
 * @author Mario Boley
 * 
 * @since 0.4.0
 * 
 * @version 0.4.0
 *
 */
public class StoredDashboardProvider implements DashboardLinkProvider {

	private static final Logger LOGGER = Logger.getLogger(StoredDashboardProvider.class.getName());

	public static final StoredDashboardProvider INSTANCE = new StoredDashboardProvider();

	private StoredDashboardProvider() {
		;
	}

	@Override
	public String getTitle() {
		return "My Dashboards";
	}

	private Action deleteAction(int actionId, String entryId) {
		return DefaultAction.action(actionId, "Delete", params -> {
			LOGGER.info("Deleting stored dashboard: " + entryId);
			ApplicationRepositories.STORED_DASHBOARD_REPOSITORY.delete(entryId);
			Path entryPath = ServerPaths.ABS_PATH_TO_CONFIGURED_WORKSPACE_DIR.resolve(entryId);
			try {
				FileUtils.deleteDirectory(entryPath.toFile());
			} catch (IOException e) {
				LOGGER.info("Could not delete workspace in filesystem at: " + entryPath);
			}
			return new ResponseEntity<>(HttpStatus.OK);
		}, ClientWindowEffect.REFRESH, true);
	}

	private Action renameAction(int actionId, String entryId) {
		return DefaultAction.action(actionId, "Rename", params -> {
			LOGGER.info("Renaming stored dashboard: " + entryId);
			RepositoryEntry<String, StoredDashboardLinkBuilder> entry = ApplicationRepositories.STORED_DASHBOARD_REPOSITORY
					.getEntry(entryId);
			entry.getContent().title(params[0]);
			ApplicationRepositories.STORED_DASHBOARD_REPOSITORY.update(entry);
			return new ResponseEntity<>(HttpStatus.OK);
		}, ClientWindowEffect.REFRESH, false, "New name");
	}

	@Override
	public List<DashboardLinkEntry> getDashboardLinks(CreedoSession session) {
		return ApplicationRepositories.STORED_DASHBOARD_REPOSITORY
				.getAllEntries(e -> session.getUser().id().equals(e.getContent().userId())).stream().map(e -> {
					DashboardLink link = e.getContent().get();
					return DashboardLinkEntry.dashboardLinkEntry(link, OpenLinkAction.openLinkAction(session, link),
							Optional.of(renameAction(session.getUiRegister().getIdGenerator().getNextId(), e.getId())),
							deleteAction(session.getUiRegister().getIdGenerator().getNextId(), e.getId()));
				}).collect(Collectors.toList());
	}

}
