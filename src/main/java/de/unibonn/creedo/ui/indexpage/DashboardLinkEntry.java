package de.unibonn.creedo.ui.indexpage;

import java.util.Optional;

import de.unibonn.creedo.ui.core.Action;
import de.unibonn.creedo.webapp.CreedoSession;

/**
 * Entry in a section of {@link DefaultIndexPage}. Interfaces to JSP. Therefore
 * uses out-dated Java conventions (null returns and getter with get-prefix).
 * 
 * @author Mario Boley
 * 
 * @since 0.4.0
 * 
 * @version 0.4.0
 *
 */
public class DashboardLinkEntry {

	public static DashboardLinkEntry dashboardLinkEntry(DashboardLink link, Action openAction,
			Optional<Action> renameAction, Action... otherActions) {
		return new DashboardLinkEntry(link, openAction, renameAction, otherActions);
	}

	public static DashboardLinkEntry dashboardLinkEntry(DashboardLink link, CreedoSession session) {
		return new DashboardLinkEntry(link, OpenLinkAction.openLinkAction(session, link), Optional.empty());
	}

	private final DashboardLink link;

	private final Action openAction;

	private final Action[] otherActions;

	private final Action renameAction;

	private DashboardLinkEntry(DashboardLink link, Action openAction, Optional<Action> renameAction,
			Action... otherActions) {
		this.link = link;
		this.openAction = openAction;
		this.otherActions = otherActions;
		this.renameAction = renameAction.orElse(null);
	}

	public DashboardLink getLink() {
		return link;
	}

	public Action getOpenAction() {
		return openAction;
	}

	public Action[] getOtherActions() {
		return otherActions;
	}

	public Action getRenameAction() {
		return renameAction;
	}

}
