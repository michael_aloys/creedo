package de.unibonn.creedo.ui.indexpage;

import de.unibonn.creedo.admin.options.Configurable;
import de.unibonn.creedo.boot.IndexPageOptions;
import de.unibonn.creedo.setup.ServerPaths;
import de.unibonn.creedo.ui.PageBuilder;
import de.unibonn.creedo.ui.core.Page;
import de.unibonn.creedo.webapp.CreedoSession;
import de.unibonn.realkd.common.RuntimeBuilder;

/**
 * Represents an index page configuration (title, reference name, content, and
 * login invite message).
 *
 * @author Björn Jacobs
 * 
 * @since 0.1.0
 * 
 * @version 0.4.0
 * 
 */
public class DefaultIndexPageBuilder
		implements PageBuilder, Configurable<IndexPageOptions>, RuntimeBuilder<Page, CreedoSession> {

	public DefaultIndexPageBuilder() {
		;
	}

	@Override
	public Page build(CreedoSession session) {
		return new DefaultIndexPage(session, getOptions().title(), getOptions().referenceName(),
				getOptions().contentPageFilename().isPresent()
						? ServerPaths.getContentFileContentAsString(getOptions().contentPageFilename().get()) : "",
				getOptions().loginInvite());
	}

	@Override
	public IndexPageOptions getDefaultOptions() {
		return new IndexPageOptions();
	}

	@Override
	public String getOptionsSerializationId() {
		return "IndexPage";
	}

}
