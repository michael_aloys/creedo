package de.unibonn.creedo.ui.indexpage;

import java.util.List;

import com.google.common.collect.ImmutableList;

import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.ParameterContainer;
import de.unibonn.realkd.common.parameter.Parameters;

public class DemoDashboardLinkOptions implements ParameterContainer {

	private final Parameter<Boolean> buildCopyOnOpen;

	public DemoDashboardLinkOptions() {
		buildCopyOnOpen = Parameters.booleanParameter("Build copy on open",
				"Whether opening the demo link will create a dashboard copy for user with persistant results.");
	}

	@Override
	public List<Parameter<?>> getTopLevelParameters() {
		return ImmutableList.of(buildCopyOnOpen);
	}

	public boolean buildCopyOnOpen() {
		return buildCopyOnOpen.current();
	}

}
