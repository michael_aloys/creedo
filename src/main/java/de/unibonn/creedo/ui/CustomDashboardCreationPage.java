package de.unibonn.creedo.ui;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Scanner;
import java.util.function.Supplier;
import java.util.logging.Logger;

import org.springframework.ui.Model;
import org.springframework.validation.support.BindingAwareModelMap;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.ImmutableSet;

import de.unibonn.creedo.ApplicationRepositories;
import de.unibonn.creedo.ConfigurationProperties;
import de.unibonn.creedo.ui.core.PreparedFrame;
import de.unibonn.creedo.ui.core.Page;
import de.unibonn.creedo.ui.core.UiComponent;
import de.unibonn.creedo.webapp.dashboard.mining.DataTableProvider;
import de.unibonn.creedo.webapp.dashboard.mining.DefaultAnalyticsDashboardBuilder;
import de.unibonn.creedo.webapp.dashboard.mining.MiningSystemBuilder;
import de.unibonn.realkd.algorithms.AlgorithmFactory;
import de.unibonn.realkd.algorithms.MiningAlgorithmFactory;
import de.unibonn.realkd.common.Pair;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.workspace.Workspace;
import de.unibonn.realkd.common.workspace.Workspaces;
import de.unibonn.realkd.data.propositions.PropositionalLogicFromTableBuilder;
import de.unibonn.realkd.data.table.DataFormatException;
import de.unibonn.realkd.data.table.DataTable;

/**
 * Model class for all custom-dashboard-creation related logic.
 *
 * @author Bo Kang
 * @auhtor Björn Jacobs
 * 
 * @since 0.1.0
 * 
 * @version 0.1.0.1
 * 
 */
public class CustomDashboardCreationPage implements Page, UiComponent {

	private static final Logger LOGGER = Logger.getLogger(CustomDashboardCreationPage.class.getName());

	private MiningSystemBuilder<?> miningSystemBuilder;

	private final int id;

	private static List<MiningSystemBuilder<?>> miningSystemBuilders = new ArrayList<>();

	static {
		// List<String> builderClassNames =
		// ConfigurationProperties.get().MINING_SYSTEM_BUILDER_CLASS_NAMES;

		// Then instantiate the mentioned MiningSystemBuilders
		for (Class<?> clazz : ConfigurationProperties.get().MINING_SYSTEM_BUILDER_CLASSES) {
			try {
				Object instance = clazz.newInstance();
				miningSystemBuilders.add((MiningSystemBuilder<?>) instance);
			} catch (InstantiationException | IllegalAccessException e) {
				LOGGER.warning("Could not instantiate '" + clazz + "' because " + e.getMessage());
			}
		}
	}

	public CustomDashboardCreationPage(int id) {
		this.id = id;
		initModel();
	}

	private void initModel() {
		this.miningSystemBuilder = miningSystemBuilders.get(0);
	}

	private List<Pair<String, Integer>> loadSystemVariantsInformation() {
		List<Pair<String, Integer>> result = new ArrayList<>();

		for (int i = 0; i < miningSystemBuilders.size(); i++) {
			result.add(Pair.pair(((Object) miningSystemBuilders.get(i)).toString(), i));
		}

		return result;
	}

	// TODO to be removed when algorithms become parameter of builder
	private List<Pair<String, Boolean>> loadAlgorithmInformation() {
		List<Pair<String, Boolean>> result = new ArrayList<>();

		for (MiningAlgorithmFactory algorithm : AlgorithmFactory.values()) {// getConfiguredAlgorithms())
																			// {
			result.add(Pair.pair(algorithm.toString(), false));
		}
		return result;
	}

	public PreparedFrame createCustomDashboardBuilder(MultipartFile attributesFile, MultipartFile dataFile,
			MultipartFile attributeGroupsFile, String dataTableId, String dataDelimiter, String missingSymbol,
			// String rankerFactoryName,
			Integer[] userSelectedAlgorithmIds, Integer selectedTabIndex) throws IOException {

		Collection<MiningAlgorithmFactory> algorithms = compileAlgorithmCollection(userSelectedAlgorithmIds);

		PreparedFrame builder;

		// Set values in builder TODO to be removed by using parameters
		miningSystemBuilder.setAlgorithmFactories(algorithms);

		// Identify system
		DataTableProvider dataTableProvider;
		if (selectedTabIndex == 0) {
			// use dataTableId
			// dataTableProvider = new
			// DataTableProvider.DbBasedDataTableProvider(
			// dataTableId);

			return new DefaultAnalyticsDashboardBuilder(
					ApplicationRepositories.DATA_REPOSITORY.getEntry(dataTableId).getContent(), miningSystemBuilder);

		} else {
			if ((attributesFile.isEmpty() || dataFile.isEmpty())) {
				String msg = "No data files were provided.";
				LOGGER.finer(msg);
				throw new IllegalArgumentException(msg);
			}

			// using files
			String dataFileContent = convertStreamToString(dataFile.getInputStream());
			String attributeFileContent = convertStreamToString(attributesFile.getInputStream());
			String attributeGroupsFileContent = convertStreamToString(attributeGroupsFile.getInputStream());
			dataTableProvider = new DataTableProvider.FileBasedDataTableProvider(dataFileContent, attributeFileContent,
					attributeGroupsFileContent, dataDelimiter, missingSymbol);
		}

		Supplier<Workspace> workspaceSupplier = new Supplier<Workspace>() {

			@Override
			public Workspace get() {
				DataTable table;
				try {
					table = dataTableProvider.get();
				} catch (DataFormatException e) {
					throw new IllegalArgumentException("Error when loading DataTable: " + e.getMessage());
				}

				Workspace workspace = Workspaces.workspace();
				workspace.add(table);
				workspace.add(new PropositionalLogicFromTableBuilder().build(table));
				return workspace;
			}

		};

		builder = new DefaultAnalyticsDashboardBuilder(workspaceSupplier, miningSystemBuilder);

		return builder;
	}

	private Collection<MiningAlgorithmFactory> compileAlgorithmCollection(Integer[] algorithmIds) {
		Collection<MiningAlgorithmFactory> algorithms = new ArrayList<>();

		for (Integer index : algorithmIds) {
			algorithms.add(AlgorithmFactory.values()[index]);
		}
		return algorithms;
	}

	private static String convertStreamToString(InputStream is) {
		Scanner s = new Scanner(is).useDelimiter("\\A");
		return s.hasNext() ? s.next() : "";
	}

	public void setSystemVariant(Integer systemVariantId) {
		if (systemVariantId == null || systemVariantId < 0 || systemVariantId > miningSystemBuilders.size()) {
			throw new IllegalArgumentException("Invalid value for system variant id: " + systemVariantId);
		}

		this.miningSystemBuilder = miningSystemBuilders.get(systemVariantId);
	}

	public List<Parameter<?>> getParameters() {
		return miningSystemBuilder.getTopLevelParameters();
	}

	public void setParameter(String parameterName, String parameterValue) {
		try {
			miningSystemBuilder.findParameterByName(parameterName).setByString(parameterValue);
		} catch (Exception e) {
			throw new IllegalArgumentException(
					"Could not set parameter \"" + parameterName + "\" to value \"" + parameterValue + "\"!", e);
		}

	}

	@Override
	public String getTitle() {
		return "Create custom Analytics Dashboard";
	}

	@Override
	public String getReferenceName() {
		return "Custom Dashboard";
	}

	@Override
	public String getViewImport() {
		return "dashboardCreationPage.jsp";
	}

	@Override
	public Model getModel() {
		Model model = new BindingAwareModelMap();
		// List<Pair<String, Integer>> dataTables = loadDataTableInformation();
		// model.addAttribute("dataTables", dataTables);
		model.addAttribute("dataTables", ApplicationRepositories.DATA_REPOSITORY.getAllIds());

		// Load available system variants information
		List<Pair<String, Integer>> systemVariants = loadSystemVariantsInformation();
		model.addAttribute("systemVariants", systemVariants);

		// Load available Ranker Factories for one click mining dash board
		// builder
		/*
		 * List<Pair<String, String>> rankerFactories =
		 * loadRankerFactoryInformation(); mav.addObject("rankerFactories",
		 * rankerFactories);
		 */

		// load available algorithms information
		List<Pair<String, Boolean>> miningAlgorithms = loadAlgorithmInformation();
		model.addAttribute("miningAlgorithms", miningAlgorithms);
		return model;
	}

	@Override
	public int getId() {
		return id;
	}

	@Override
	public String getView() {
		return getViewImport();
	}

	@Override
	public ImmutableSet<String> getOwnScriptFilenames() {
		return ImmutableSet.of("creedo-parameters.js", "customDashboard.js");
	}

}
