package de.unibonn.creedo.ui.standard;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.validation.support.BindingAwareModelMap;

import de.unibonn.creedo.ui.core.ActionLink;
import de.unibonn.creedo.ui.core.ActionProvider;
import de.unibonn.creedo.ui.core.UiComponent;

public class Navbar implements ActionProvider, UiComponent {

	private final List<ActionLink> actionLinks;

	private final Map<Integer, ActionLink> idToActionLink;

	private final int id;

	public Navbar(int id, List<ActionLink> actionLinks) {
		this.id = id;
		this.actionLinks = actionLinks;
		this.idToActionLink = new HashMap<Integer, ActionLink>();
		for (ActionLink link : actionLinks) {
			if (idToActionLink.containsKey(link.getId())) {
				throw new IllegalArgumentException("Action with id "
						+ link.getId() + " already present in navbar.");
			}
			idToActionLink.put(link.getId(), link);
		}
	}

	@Override
	public Collection<Integer> getActionIds() {
		return idToActionLink.keySet();
	}

	@Override
	public ResponseEntity<String> performAction(int id, String... params) {
		return idToActionLink.get(id).activate();
	}

	@Override
	public boolean isActionAvailable(int id) {
		return idToActionLink.containsKey(id);
	}

	@Override
	public Model getModel() {
		Model model = new BindingAwareModelMap();
		model.addAttribute("navbarId", getId());
		model.addAttribute("navbarLinks", actionLinks);
		return model;
	}

	@Override
	public String getView() {
		return "navbar.jsp";
	}

	@Override
	public int getId() {
		return id;
	}

	@Override
	public List<UiComponent> getComponents() {
		return Arrays.asList();
	}

}
