package de.unibonn.creedo.ui.mining;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.ui.Model;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import de.unibonn.creedo.ui.core.DataProvider;
import de.unibonn.creedo.ui.core.UiComponent;
import de.unibonn.creedo.webapp.dashboard.patterncontainer.WebPattern;
import de.unibonn.realkd.patterns.PatternDescriptor;
import de.unibonn.realkd.patterns.SubPopulationDescriptor;

public class PatternSupportSetProvider implements DataProvider {

	private final int id;

	private final Map<Integer, WebPattern> idToWebPattern;

	public PatternSupportSetProvider(int id, Map<Integer, WebPattern> patternMap) {
		this.id = id;
		this.idToWebPattern = patternMap;
	}

	@Override
	public int getId() {
		return id;
	}

	@Override
	public String getView() {
		throw new UnsupportedOperationException();
	}

	@Override
	public Model getModel() {
		throw new UnsupportedOperationException();
	}

	@Override
	public List<UiComponent> getComponents() {
		return ImmutableList.of();
	}

	@Override
	public ImmutableSet<String> getDataItemIds() {
		return ImmutableSet.copyOf(idToWebPattern.keySet().stream()
				.map(x -> String.valueOf(x)).collect(Collectors.toList()));
	}

	@Override
	public List<Object> getDataItem(String dataItemId) {
		PatternDescriptor descriptor = idToWebPattern
				.get(Integer.parseInt(dataItemId)).getPattern().descriptor();
		return ImmutableList.copyOf(((SubPopulationDescriptor) descriptor)
				.indices());
	}

}
