package de.unibonn.creedo.ui.mining;

import static com.google.common.base.Preconditions.checkArgument;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.support.BindingAwareModelMap;

import de.unibonn.creedo.Creedo;
import de.unibonn.creedo.ui.core.Action;
import de.unibonn.creedo.ui.core.Frame;
import de.unibonn.creedo.webapp.dashboard.patterncontainer.WebPattern;
import de.unibonn.creedo.webapp.utils.VisualizationProvider;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.patterns.Measure;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.SubPopulationDescriptor;
import de.unibonn.realkd.patterns.TableSubspaceDescriptor;
import de.unibonn.realkd.patterns.association.Association;
import de.unibonn.realkd.patterns.emm.ExceptionalModelPattern;
import de.unibonn.realkd.patterns.logical.LogicalDescriptor;
import de.unibonn.realkd.patterns.outlier.Outlier;

/**
 * Action creates and registers a new frame object for showing a detailed view
 * of a pattern within a pattern map. Action returns the url for showing this
 * frame.
 * 
 * @author Mario Boley
 * 
 * @since 0.1.2
 * 
 * @version 0.1.2.1
 *
 */
public class OpenDetailedPatternViewAction implements Action {

	private final int id;

	private final Map<Integer, WebPattern> idToPatternMap;

	private final HttpSession httpSession;

	public OpenDetailedPatternViewAction(int id, Map<Integer, WebPattern> idToPatternMap, HttpSession httpSession) {
		this.id = id;
		this.idToPatternMap = idToPatternMap;
		this.httpSession = httpSession;
	}

	@Override
	public String getReferenceName() {
		throw new UnsupportedOperationException();
	}

	@Override
	public ResponseEntity<String> activate(String... params) {
		if (params.length != 1) {
			return new ResponseEntity<String>("Expect exactly one pattern id parameter", HttpStatus.BAD_REQUEST);
		}
		int patternId = Integer.parseInt(params[0]);
		WebPattern webPattern = idToPatternMap.get(patternId);

		List<String> vImages = VisualizationProvider.createDetailedPatternImages(httpSession, webPattern);

		BindingAwareModelMap mav = new BindingAwareModelMap();
		mav.addAttribute("vImages", vImages);
		mav.addAttribute("showNavBar", true);

		if (webPattern.getPattern() instanceof ExceptionalModelPattern) {
			ExceptionalModelPattern emmPattern = (ExceptionalModelPattern) webPattern.getPattern();
			String annotation = webPattern.getAnnotationText();
			return getEmmPatternVisualization(mav, emmPattern, annotation);
		} else if (webPattern.getPattern() instanceof Association) {
			Association assocPattern = (Association) webPattern.getPattern();
			String annotation = webPattern.getAnnotationText();
			return getAssociationPatternVisualization(mav, assocPattern, annotation);
		} else if (webPattern.getPattern() instanceof Outlier) {
			Outlier outlier = (Outlier) webPattern.getPattern();
//			String annotation = webPattern.getAnnotationText();
			return getOutlierPatternVisualization(mav, outlier);
		} else {
			return getGenericPatternVisualization(mav, webPattern.getPattern());
		}
	}

	/**
	 * 
	 * Needed to wrap measurements for jsp parser which expects classic Java
	 * bean getter naming conventions
	 *
	 */
	public static class MeasurementBean {

		private Measure measure;

		private double value;

		private MeasurementBean(Measure measure, double value) {
			this.measure = measure;
			this.value = value;
		}

		public String getMeasure() {
			return measure.getName();
		}

		public double getValue() {
			return this.value;
		}

	}

	private static MeasurementBean measurementBean(Measure measure, double value) {

		return new MeasurementBean(measure, value);

	}

	private ResponseEntity<String> getEmmPatternVisualization(BindingAwareModelMap modelMap,
			ExceptionalModelPattern pattern, String annotation) {
		// BindingAwareModelMap modelMap = new BindingAwareModelMap();
		modelMap.addAttribute("targets", pattern.descriptor().targetAttributes());
		modelMap.addAttribute("propositions", pattern.descriptor().extensionDescriptor().getElements());
		modelMap.addAttribute("measurements",
				pattern.measures().stream().map(m -> measurementBean(m, pattern.value(m))).toArray());
		modelMap.addAttribute("annotation", annotation);

		Frame frame = Creedo.getCreedoSession(httpSession).getUiRegister().createStaticFrame(modelMap,
				"patterns/extendedEmmPatternView");

		return new ResponseEntity<String>("showFrame.htm?frameId=" + frame.getId(), HttpStatus.OK);
	}

	private ResponseEntity<String> getAssociationPatternVisualization(BindingAwareModelMap mav, Association pattern,
			String annotation) {
		// BindingAwareModelMap mav = new BindingAwareModelMap();
		mav.addAttribute("propositions", ((LogicalDescriptor) pattern.descriptor()).getElements());
//		mav.addAttribute("measures", CandidateAssociationPatternMapper.getMeasures(pattern));
		mav.addAttribute("measurements",
				pattern.measures().stream().map(m -> measurementBean(m, pattern.value(m))).toArray());
		mav.addAttribute("annotation", annotation);

		Frame frame = Creedo.getCreedoSession(httpSession).getUiRegister().createStaticFrame(mav,
				"patterns/extendedAssociationView");

		return new ResponseEntity<String>("showFrame.htm?frameId=" + frame.getId(), HttpStatus.OK);
	}

	private ResponseEntity<String> getOutlierPatternVisualization(BindingAwareModelMap mav, Outlier pattern) {
		checkArgument(pattern.descriptor() instanceof SubPopulationDescriptor,
				"Descriptor must describe sub population.");
		checkArgument(pattern.descriptor() instanceof TableSubspaceDescriptor,
				"Descriptor must describe table subspace.");
		// BindingAwareModelMap mav = new BindingAwareModelMap();
		List<String> elementNames = new ArrayList<String>();
		SubPopulationDescriptor subPopulationDescriptor = (SubPopulationDescriptor) pattern.descriptor();
		for (Integer rowIndex : subPopulationDescriptor.indices()) {
			elementNames.add(subPopulationDescriptor.population().objectName(rowIndex));
		}
		mav.addAttribute("elementNames", elementNames);
		mav.addAttribute("attributes", ((TableSubspaceDescriptor) pattern.descriptor()).getReferencedAttributes());

		// mav.addObject("measures",
		// pattern.);

		Frame frame = Creedo.getCreedoSession(httpSession).getUiRegister().createStaticFrame(mav,
				"patterns/outlierPatternView");

		return new ResponseEntity<String>("showFrame.htm?frameId=" + frame.getId(), HttpStatus.OK);
	}

	private ResponseEntity<String> getGenericPatternVisualization(BindingAwareModelMap mav, Pattern pattern) {
		// BindingAwareModelMap mav = new BindingAwareModelMap();
		List<String> elementNames = new ArrayList<String>();
		List<Attribute<?>> attributes = new ArrayList<Attribute<?>>();

		if (pattern.descriptor() instanceof SubPopulationDescriptor) {
			SubPopulationDescriptor subPopulationDescriptor = (SubPopulationDescriptor) pattern.descriptor();
			for (Integer rowIndex : subPopulationDescriptor.indices()) {
				elementNames.add(subPopulationDescriptor.population().objectName(rowIndex));
			}
		}

		if (pattern.descriptor() instanceof TableSubspaceDescriptor) {
			attributes.addAll(((TableSubspaceDescriptor) pattern.descriptor()).getReferencedAttributes());
		}

		mav.addAttribute("elementNames", elementNames);
		mav.addAttribute("attributes", attributes);

		// mav.addObject("measures",
		// pattern.);

		Frame frame = Creedo.getCreedoSession(httpSession).getUiRegister().createStaticFrame(mav,
				"patterns/extendedGenericPatternView");

		return new ResponseEntity<String>("showFrame.htm?frameId=" + frame.getId(), HttpStatus.OK);
	}

	@Override
	public ClientWindowEffect getEffect() {
		return ClientWindowEffect.POPUP;
	}

	@Override
	public int getId() {
		return id;
	}

}
