package de.unibonn.creedo.ui.mining;

import static com.google.common.base.Preconditions.checkArgument;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.validation.support.BindingAwareModelMap;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import de.unibonn.creedo.ui.core.Action;
import de.unibonn.creedo.ui.core.ActionProvider;
import de.unibonn.creedo.ui.core.DataProvider;
import de.unibonn.creedo.ui.core.UiComponent;
import de.unibonn.creedo.ui.core.UiRegister.IdGenerator;
import de.unibonn.creedo.webapp.utils.ParameterToJsonConverter;
import de.unibonn.creedo.webapp.viewmodels.DataTableViewModel;
import de.unibonn.creedo.webapp.viewmodels.MetaDataTableModel;
import de.unibonn.creedo.webapp.viewmodels.PointCloudViewModel;
import de.unibonn.realkd.common.workspace.Workspace;
import de.unibonn.realkd.data.propositions.PropositionalLogic;
import de.unibonn.realkd.data.propositions.TableBasedPropositionalLogic;
import de.unibonn.realkd.data.table.DataTable;

/**
 * UI component that aggregates several views on a data workspace.
 * 
 * @author Mario Boley
 * @author Elvin Efendijev
 * 
 * @since 0.1.0
 * 
 * @version 0.1.2.1
 *
 */
public class DataViewContainer implements UiComponent, DataProvider, ActionProvider {

	private static final Logger LOGGER = Logger.getLogger(DataViewContainer.class.getName());

	private final int id;

	private DataTableViewModel dataTableViewModel;

	private MetaDataTableModel metaDataModel;

	private PointCloudViewModel pointCloudViewModel;

	private final Workspace dataWorkspace;

	private final HttpSession httpSession;

	private final DataWorkSpaceTooltip tooltip;

	private final Action pointCloudParameterUpdateAction;
	
	private final boolean pcaAsScatterDefault;

	public DataViewContainer(IdGenerator idGenerator, HttpSession httpSession, Workspace dataWorkspace, boolean pcaAsScatterDefault) {
		this.id = idGenerator.getNextId();
		this.httpSession = httpSession;
		this.dataWorkspace = dataWorkspace;
		this.tooltip = new DataWorkSpaceTooltip(dataWorkspace);
		this.pcaAsScatterDefault=pcaAsScatterDefault;
		this.pointCloudParameterUpdateAction = new Action() {

			private final int id = idGenerator.getNextId();

			@Override
			public String getReferenceName() {
				return "Set parameter";
			}

			@Override
			public ResponseEntity<String> activate(String... params) {
				checkArgument(params.length == 2);
				checkArgument(params[0] != null);

				String parameterName = params[0];
				String value = params[1];

				pointCloudViewModel.findParameterByName(parameterName).setByString(value);

				return new ResponseEntity<>(HttpStatus.OK);
			}

			@Override
			public ClientWindowEffect getEffect() {
				return ClientWindowEffect.NONE;
			}

			@Override
			public int getId() {
				return id;
			}
		};
	}

	public DataTable getDataTable() {
		return dataWorkspace.getAllDatatables().get(0);
	}

	public PropositionalLogic getPropositionalLogic() {
		return dataWorkspace.getAllPropositionalLogics().get(0);
	}

	@Override
	public int getId() {
		return id;
	}

	@Override
	public String getView() {
		return "../static/dataViewContainer.jsp";
	}

	@Override
	public Model getModel() {
		BindingAwareModelMap model = new BindingAwareModelMap();
		model.addAttribute("datasetName", getDataTable().name());
		model.addAttribute("dataViewContainerId", this.getId());
		model.addAttribute("dataTableViewModel", getDataTableViewModel());
		model.addAttribute("metaDataModel", getMetaDataModel());
		model.addAttribute("updatePointCloudActionId", pointCloudParameterUpdateAction.getId());
		return model;
	}

	@Override
	public List<UiComponent> getComponents() {
		return Arrays.asList();
	}

	private PointCloudViewModel getPointCloudViewModel() {
		if (pointCloudViewModel == null) {
			pointCloudViewModel = new PointCloudViewModel(getDataTable(),pcaAsScatterDefault);
		}
		return pointCloudViewModel;
	}

	private DataTableViewModel getDataTableViewModel() {
		if (dataTableViewModel == null) {
			dataTableViewModel = new DataTableViewModel(getDataTable(), httpSession);
		}
		return dataTableViewModel;
	}

	private MetaDataTableModel getMetaDataModel() {
		if (metaDataModel == null) {
			if (getPropositionalLogic() instanceof TableBasedPropositionalLogic) {
				metaDataModel = new MetaDataTableModel((TableBasedPropositionalLogic) getPropositionalLogic());
			}
		}
		return metaDataModel;
	}

	@Override
	public ImmutableSet<String> getOwnScriptFilenames() {
		return ImmutableSet.of("creedo-dataviewcontainer.js?v=0.1.2.19");
	}

	@Override
	public ImmutableSet<String> getDataItemIds() {
		return ImmutableSet.of("tableData", "columnTooltips", "pointData", "pointParameters", "pointAxesTitles",
				"description");
	}

	private static class DataWorkSpaceTooltip {

		String tooltip;

		public DataWorkSpaceTooltip(Workspace dataWorkspace) {
			StringBuilder tooltipBuilder = new StringBuilder();
			tooltipBuilder.append("<p>" + dataWorkspace.getAllDatatables().get(0).description() + "</p>");
			tooltipBuilder.append("<hr class='no-margin' />");
			tooltipBuilder.append("<br />");
			tooltipBuilder.append("<p>");
			tooltipBuilder.append(dataWorkspace.getAllDatatables().get(0).population().size() + " rows<br />\n");
			tooltipBuilder
					.append(dataWorkspace.getAllDatatables().get(0).getNumberOfAttributes() + " attributes<br />\n");
			tooltipBuilder.append(dataWorkspace.getAllPropositionalLogics().get(0).getPropositions().size()
					+ " propositions<br />\n");
			tooltipBuilder.append("</p>");
			tooltip = tooltipBuilder.toString();
		}

		public String toString() {
			return tooltip;
		}

	}

	/**
	 * <p>
	 * <b>Item 'tableData':</b> returns the data in the DataTable in its
	 * original order. Do not change the order here since it will break the
	 * data-consistency with clients. <br>
	 * FUTURE: implement server side processing for DataTables:
	 * http://datatables.net/manual/server-side (elvin)
	 * </p>
	 * 
	 */
	@Override
	public List<? extends Object> getDataItem(String dataItem) {
		LOGGER.fine("Checking for data item '" + dataItem + "'");
		if (dataItem.equals("tableData")) {
			return ImmutableList.copyOf(getDataTableViewModel().getDataTableContent());
		} else if (dataItem.equals("pointParameters")) {
			return ParameterToJsonConverter.convertRecursively(getPointCloudViewModel().getTopLevelParameters());
		} else if (dataItem.equals("pointData")) {
			return ImmutableList.copyOf(getPointCloudViewModel().getPoints());
		} else if (dataItem.equals("pointAxesTitles")) {
			return getPointCloudViewModel().getAxisTitles();
		} else if (dataItem.equals("columnTooltips")) {
			return ImmutableList.copyOf(getDataTableViewModel().getColumnToolTipHtml());
		} else if (dataItem.equals("description")) {
			return ImmutableList.of(tooltip.toString());
		} else {
			throw new IllegalArgumentException("No such data item.");
		}
	}

	@Override
	public Collection<Integer> getActionIds() {
		return ImmutableSet.of(pointCloudParameterUpdateAction.getId());
	}

	@Override
	public ResponseEntity<String> performAction(int id, String... params) {
		if (pointCloudParameterUpdateAction.getId() == id) {
			return pointCloudParameterUpdateAction.activate(params);
		}
		return new ResponseEntity<>("unknown action", HttpStatus.BAD_REQUEST);
	}

}
