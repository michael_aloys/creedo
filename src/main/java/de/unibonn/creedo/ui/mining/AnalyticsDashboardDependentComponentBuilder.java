package de.unibonn.creedo.ui.mining;

import de.unibonn.creedo.ui.core.UiComponent;
import de.unibonn.creedo.ui.core.UiRegister.FrameCloser;
import de.unibonn.creedo.ui.core.UiRegister.IdGenerator;

public interface AnalyticsDashboardDependentComponentBuilder {

	public UiComponent build(IdGenerator idGenerator,
			FrameCloser closer, AnalyticsDashboard analyticsDashboard);

}
