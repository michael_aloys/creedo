package de.unibonn.creedo.ui.mining;

import static com.google.common.base.Preconditions.checkArgument;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.validation.support.BindingAwareModelMap;

import com.google.common.collect.ImmutableSet;

import de.unibonn.creedo.Creedo;
import de.unibonn.creedo.ui.core.Action;
import de.unibonn.creedo.ui.core.ActionProvider;
import de.unibonn.creedo.ui.core.UiComponent;
import de.unibonn.creedo.webapp.dashboard.mining.MiningSystem;
import de.unibonn.creedo.webapp.dashboard.patterncontainer.WebPattern;
import de.unibonn.creedo.webapp.patternviews.CandidatePatternGenerator;
import de.unibonn.creedo.webapp.patternviews.ResultPatternGenerator;
import de.unibonn.realkd.common.workspace.Workspace;
import de.unibonn.realkd.data.propositions.PropositionalLogic;
import de.unibonn.realkd.data.table.DataTable;
import de.unibonn.realkd.patterns.NamedPatternCollection;
import de.unibonn.realkd.patterns.Pattern;

/**
 * Aggregates UI components for performing data analysis.
 * 
 * @author Mario Boley
 * 
 * @since 0.2.0
 * 
 * @version 0.4.0
 *
 */
public class AnalyticsDashboard implements UiComponent, ActionProvider {

	private static final String RESULT_PATTERN_ENTITY_ID = "$results";

	private static final Logger LOGGER = Logger.getLogger(AnalyticsDashboard.class.getName());

	private class RunMiningAlgorithmAction implements Action {

		private final int id;

		public RunMiningAlgorithmAction(int id) {
			this.id = id;
		}

		@Override
		public String getReferenceName() {
			throw new UnsupportedOperationException();
		}

		@Override
		public ResponseEntity<String> activate(String... params) {
			try {
				return new ResponseEntity<String>(mineClicked(), HttpStatus.OK);
			} catch (RuntimeException e) {
				return new ResponseEntity<>(e.getMessage() + "\n" + Arrays.toString(e.getStackTrace()),
						HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}

		@Override
		public ClientWindowEffect getEffect() {
			throw new UnsupportedOperationException();
		}

		@Override
		public int getId() {
			return id;
		}

	}

	private class AnnotatePatternAction implements Action {

		private final int id;

		public AnnotatePatternAction(int id) {
			this.id = id;
		}

		@Override
		public String getReferenceName() {
			throw new UnsupportedOperationException();
		}

		@Override
		public ResponseEntity<String> activate(String... params) {
			checkArgument(params.length == 2, "Params must exactly contain one pattern id and one annotation string");

			int id = Integer.parseInt(params[0]);
			String annotation = params[1];
			// To prevent insertion of html tags
			annotation = annotation.replaceAll("<", " smaller ").replaceAll(">", " larger ");

			WebPattern pattern = idToWebPattern.get(id);
			if (pattern == null) {
				return new ResponseEntity<String>("No such pattern", HttpStatus.BAD_REQUEST);
			}
			pattern.setAnnotationText(annotation);

			LOGGER.log(Level.INFO, "Received pattern annotation '" + annotation + "' for pattern id '" + id + "'");

			return new ResponseEntity<>(HttpStatus.OK);
		}

		@Override
		public ClientWindowEffect getEffect() {
			throw new UnsupportedOperationException();
		}

		@Override
		public int getId() {
			return id;
		}

	}

	private class SavePatternAction implements Action {

		private final int id;

		public SavePatternAction(int id) {
			this.id = id;
		}

		@Override
		public String getReferenceName() {
			throw new UnsupportedOperationException();
		}

		@Override
		public ResponseEntity<String> activate(String... params) {
			checkArgument(params.length == 1, "Params must exactly contain one pattern id");
			LOGGER.log(Level.INFO, "Saving pattern " + params[0]);
			int id = Integer.parseInt(params[0]);
			saveToResults(id);
			return new ResponseEntity<String>(getPattern(id).getHtml(httpSession), HttpStatus.OK);
		}

		@Override
		public ClientWindowEffect getEffect() {
			throw new UnsupportedOperationException();
		}

		@Override
		public int getId() {
			return id;
		}

	}

	private class DeletePatternAction implements Action {

		private final int id;

		public DeletePatternAction(int id) {
			this.id = id;
		}

		@Override
		public String getReferenceName() {
			throw new UnsupportedOperationException();
		}

		@Override
		public ResponseEntity<String> activate(String... params) {
			checkArgument(params.length == 1, "params must contain exactly 1 pattern id");
			int deletedItemID = Integer.parseInt(params[0]);
			WebPattern deletedWebPattern = getPattern(deletedItemID);
			boolean inResults = getMiningSystem().getDiscoveryProcess().getDiscoveryProcessState()
					.isInResults(deletedWebPattern.getPattern());
			if (inResults) {
				discardFromResults(deletedItemID);
			} else {

				discardFromCandidates(deletedItemID);
			}

			LOGGER.log(Level.INFO,
					"Deleted pattern" + deletedItemID + "' from " + (inResults ? "results" : "candidates"));

			return new ResponseEntity<String>(HttpStatus.OK);
		}

		@Override
		public ClientWindowEffect getEffect() {
			throw new UnsupportedOperationException();
		}

		@Override
		public int getId() {
			return id;
		}

	}

	private final HttpSession httpSession;

	private final long instantiationTimeTag;

	private class PatternIdGenerator {
		private int id = 0;

		public synchronized int getNextId() {
			return id++;
		}
	}

	private final HashMap<Integer, WebPattern> idToWebPattern;
	private final HashMap<Pattern, WebPattern> patternToWebPattern;
	private final PatternIdGenerator patternIdGenerator;
	private final HashMap<Pattern, Long> patternToMillisecondsUntilSaved;

	private final Action savePatternAction;
	private final Action runAlgorithmAction;
	private final Action deletePatternAction;
	private final Action annotatePatternAction;
	private final Action detailedPatternViewAction;

	private final Map<Integer, Action> idToActionMap;

	private final MiningSystem miningSystem;
	private final DataViewContainer dataViewContainer;
	private final PatternSupportSetProvider supportSetProvider;
	private final Workspace workspace;

	private final int id;

	public AnalyticsDashboard(int id, HttpSession httpSession, Workspace dataWorkspace, MiningSystem miningSystem) {
		this.httpSession = httpSession;
		// this.dataWorkspace = dataWorkspace;
		this.id = id;
		this.idToWebPattern = new HashMap<>();
		this.patternToWebPattern = new HashMap<>();
		this.patternIdGenerator = new PatternIdGenerator();
		this.instantiationTimeTag = System.currentTimeMillis();
		this.patternToMillisecondsUntilSaved = new HashMap<>();

		this.workspace = dataWorkspace;
		this.miningSystem = miningSystem;
		this.dataViewContainer = new DataViewContainer(
				Creedo.getCreedoSession(httpSession).getUiRegister().getIdGenerator(), httpSession, dataWorkspace,
				miningSystem.pcaAsScatterDefault());
		this.supportSetProvider = new PatternSupportSetProvider(
				Creedo.getCreedoSession(httpSession).getUiRegister().getNextId(), idToWebPattern);

		this.runAlgorithmAction = new RunMiningAlgorithmAction(
				Creedo.getCreedoSession(httpSession).getUiRegister().getNextId());
		this.savePatternAction = new SavePatternAction(
				Creedo.getCreedoSession(httpSession).getUiRegister().getNextId());
		this.deletePatternAction = new DeletePatternAction(
				Creedo.getCreedoSession(httpSession).getUiRegister().getNextId());
		this.annotatePatternAction = new AnnotatePatternAction(
				Creedo.getCreedoSession(httpSession).getUiRegister().getNextId());
		this.detailedPatternViewAction = new OpenDetailedPatternViewAction(
				Creedo.getCreedoSession(httpSession).getUiRegister().getNextId(), idToWebPattern, httpSession);

		this.idToActionMap = new HashMap<>();
		this.idToActionMap.put(runAlgorithmAction.getId(), runAlgorithmAction);
		this.idToActionMap.put(savePatternAction.getId(), savePatternAction);
		this.idToActionMap.put(deletePatternAction.getId(), deletePatternAction);
		this.idToActionMap.put(annotatePatternAction.getId(), annotatePatternAction);
		this.idToActionMap.put(detailedPatternViewAction.getId(), detailedPatternViewAction);

		Optional<NamedPatternCollection> initialResults = workspace.get(RESULT_PATTERN_ENTITY_ID,
				NamedPatternCollection.class);
		if (initialResults.isPresent()) {
			miningSystem.getDiscoveryProcess().addResults(initialResults.get().patterns());
			addNewResultsToMaps(initialResults.get().patterns());
			LOGGER.info("Deserialized " + initialResults.get().patterns().size() + " result pattern(s).");
		}
	}

	@Override
	public String getView() {
		return "analyticsDashboard.jsp";
	}

	@Override
	public Model getModel() {
		Model model = new BindingAwareModelMap();
		String datasetName = getDataTable().name();

		model.addAllAttributes(dataViewContainer.getModel().asMap());
		model.addAllAttributes(miningSystem.getModel().asMap());

		model.addAttribute("title", datasetName);
		model.addAttribute("analyticsDashboardId", getId());

		model.addAttribute("runAlgorithmActionId", runAlgorithmAction.getId());
		model.addAttribute("savePatternActionId", savePatternAction.getId());
		model.addAttribute("deletePatternActionId", deletePatternAction.getId());
		model.addAttribute("annotatePatternActionId", annotatePatternAction.getId());

		model.addAttribute("resultPatternHtml", getResultPatternsAsHTML());
		// model.addAttribute("openDetailedPatternViewActionId",
		// deletePatternAction.getId());

		return model;
	}

	public MiningSystem getMiningSystem() {
		return miningSystem;
	}

	public DataTable getDataTable() {
		return dataViewContainer.getDataTable();
	}

	public PropositionalLogic getPropositionalLogic() {
		return dataViewContainer.getPropositionalLogic();
	}

	@Override
	public int getId() {
		return this.id;
	}

	@Override
	public List<UiComponent> getComponents() {
		return Arrays.asList((UiComponent) dataViewContainer, miningSystem, supportSetProvider);
	}

	public void saveToResults(int patternId) {
		long timeUntilSaved = System.currentTimeMillis() - instantiationTimeTag;
		WebPattern webPattern = idToWebPattern.get(patternId);
		webPattern.setHtmlGenerator(ResultPatternGenerator.INSTANCE);

		patternToMillisecondsUntilSaved.put(webPattern.getPattern(), timeUntilSaved);

		getMiningSystem().getDiscoveryProcess().addCandidateToResults(webPattern.getPattern());
	}

	public void discardFromResults(int patternId) {
		patternToMillisecondsUntilSaved.remove(patternId);
		WebPattern webPattern = idToWebPattern.get(patternId);
		getMiningSystem().getDiscoveryProcess().deletePatternFromResults(webPattern.getPattern());
	}

	public void discardFromCandidates(int patternId) {
		WebPattern webPattern = idToWebPattern.get(patternId);
		getMiningSystem().getDiscoveryProcess().deletePatternFromCandidates(webPattern.getPattern());
	}

	private List<WebPattern> compileWebPatternList(List<Pattern> patterns) {
		ArrayList<WebPattern> result = new ArrayList<>(patterns.size());
		for (Pattern pattern : patterns) {
			result.add(patternToWebPattern.get(pattern));
		}
		return result;
	}

	public List<WebPattern> getResults() {
		return compileWebPatternList(
				getMiningSystem().getDiscoveryProcess().getDiscoveryProcessState().getResultPatterns());
	}

	public List<WebPattern> getDiscarded() {
		return compileWebPatternList(
				getMiningSystem().getDiscoveryProcess().getDiscoveryProcessState().getDiscardedPatterns());
	}

	private String getResultPatternsAsHTML() {
		StringBuilder html = new StringBuilder();
		for (WebPattern webPattern : getResults()) {
			html.append(webPattern.getHtml(httpSession));
		}

		return html.toString();
	}

	private String getCandidatePatternsAsHTML() {
		StringBuilder html = new StringBuilder();
		for (WebPattern webPattern : compileWebPatternList(
				getMiningSystem().getDiscoveryProcess().getDiscoveryProcessState().getCandidatePatterns())) {
			html.append(webPattern.getHtml(httpSession));
		}

		return html.toString();
	}

	public String mineClicked() {
		getMiningSystem().getDiscoveryProcess().endRound();
		List<Pattern> minedPatterns = getMiningSystem().mineClicked();
		nextRound(minedPatterns);
		return this.getCandidatePatternsAsHTML();
	}

	/**
	 * Starts a new discovery round with the patterns provided as candidates.
	 * <p/>
	 * During a discovery round, calling the nextRound method will end the
	 * current round and start the next round automatically.
	 * <p/>
	 * When the current round is ended it can be started again by calling
	 * nextRound method.
	 *
	 * @param nextRoundPatterns
	 *            patterns to be used in the new round
	 */
	private void nextRound(List<Pattern> nextRoundPatterns) {
		removeOldCandidatesFromMaps();
		addNewCandidatesToMaps(nextRoundPatterns);
		getMiningSystem().getDiscoveryProcess().nextRound(nextRoundPatterns);
	}

	private void removeOldCandidatesFromMaps() {
		List<Integer> idsToRemove = new ArrayList<>();
		for (Entry<Integer, WebPattern> entry : idToWebPattern.entrySet()) {
			if (getMiningSystem().getDiscoveryProcess().getDiscoveryProcessState()
					.isInCandidates(entry.getValue().getPattern())) {
				idsToRemove.add(entry.getKey());
			}
		}
		for (Integer id : idsToRemove) {
			patternToWebPattern.remove(idToWebPattern.get(id));
			idToWebPattern.remove(id);
		}
	}

	private void addNewCandidatesToMaps(List<Pattern> nextRoundPatterns) {
		for (Pattern pattern : nextRoundPatterns) {
			WebPattern webPattern = new WebPattern(this.patternIdGenerator.getNextId(), pattern,
					CandidatePatternGenerator.INSTANCE, supportSetProvider.getId(), getId(),
					detailedPatternViewAction.getId());
			idToWebPattern.put(webPattern.getId(), webPattern);
			patternToWebPattern.put(pattern, webPattern);
		}
	}

	private void addNewResultsToMaps(Collection<Pattern> results) {
		for (Pattern pattern : results) {
			WebPattern webPattern = new WebPattern(this.patternIdGenerator.getNextId(), pattern,
					ResultPatternGenerator.INSTANCE, supportSetProvider.getId(), getId(),
					detailedPatternViewAction.getId());
			idToWebPattern.put(webPattern.getId(), webPattern);
			patternToWebPattern.put(pattern, webPattern);
		}
	}

	private WebPattern getPattern(int id) {
		WebPattern result = idToWebPattern.get(id);
		if (result == null) {
			throw new IllegalArgumentException("MiningDashboard does not contain pattern with matching id");
		}
		return result;
	}

	public long getMillisecondsUntilSaved(Pattern pattern) {
		Long result = patternToMillisecondsUntilSaved.get(pattern);
		if (result == null) {
			throw new IllegalArgumentException("MiningDashboard does not contain requested pattern");
		}

		return result;
	}

	@Override
	public Collection<Integer> getActionIds() {
		return idToActionMap.keySet();
	}

	@Override
	public ResponseEntity<String> performAction(int id, String... params) {
		checkArgument(isActionAvailable(id), "action unavailable");
		return idToActionMap.get(id).activate(params);
	}

	@Override
	public ImmutableSet<String> getOwnScriptFilenames() {
		return ImmutableSet.of("creedo-patterns.js", "creedo-analyticsdashboard.js");
	}

	public List<Pattern> getResultPatterns() {
		return getMiningSystem().getDiscoveryProcess().getDiscoveryProcessState().getResultPatterns();
	}

	public List<Integer> getSecondsUntilSaved() {
		List<Integer> secondsUntilSaved = new ArrayList<>();

		for (Pattern pattern : getResultPatterns()) {
			long millisecondsUntilSaved = getMillisecondsUntilSaved(pattern);
			secondsUntilSaved.add((int) millisecondsUntilSaved / 1000);
		}
		return secondsUntilSaved;
	}

	public void tearDown() {
		LOGGER.info("Storing result patterns in workspace");
		NamedPatternCollection resultCollection = new NamedPatternCollection(RESULT_PATTERN_ENTITY_ID, "Results",
				"Result patterns produced by user.",
				miningSystem.getDiscoveryProcess().getDiscoveryProcessState().getResultPatterns());
		workspace.overwrite(resultCollection);
	}

}
