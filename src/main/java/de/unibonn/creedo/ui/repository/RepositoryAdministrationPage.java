package de.unibonn.creedo.ui.repository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.validation.support.BindingAwareModelMap;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import de.unibonn.creedo.common.parameters.AbstractTransferableParameterState;
import de.unibonn.creedo.repositories.Repository;
import de.unibonn.creedo.repositories.RepositoryEntry;
import de.unibonn.creedo.ui.core.Action;
import de.unibonn.creedo.ui.core.ActionProvider;
import de.unibonn.creedo.ui.core.DataProvider;
import de.unibonn.creedo.ui.core.Page;
import de.unibonn.creedo.ui.core.UiComponent;
import de.unibonn.creedo.ui.core.UiRegister;
import de.unibonn.creedo.webapp.utils.ParameterToJsonConverter;
import de.unibonn.realkd.common.parameter.ParameterContainer;

/**
 * Page for adding, deleting, and modifying entries of a repository.
 * 
 * @author Björn Jacobs
 * @author Mario Boley
 * 
 * @since 0.1.0
 * 
 * @version 0.1.0
 * 
 */
public class RepositoryAdministrationPage<T> implements Page, ActionProvider, UiComponent, DataProvider {

	private final Map<Integer, Integer> actionIdToParameterIndexMap;
	private final int id;
	private final Repository<String, T> repository;
	private final List<String> typeAliases;
	private final RepositoryEntryDeletionAction deletionAction;
	private final RepositoryEntryAdditionAction<T> additionAction;
	private final UiRegister uiRegister;
	private final Map<Integer, Action> updateParameterActions;
	private final Map<String, Action> entryIdToUpdateParameterActions;

	public RepositoryAdministrationPage(int componentId, Repository<String, T> repository, List<String> typeAliases,
			RepositoryEntryDeletionAction deletionAction, RepositoryEntryAdditionAction<T> additionAction,
			UiRegister uiRegister) {
		id = componentId;
		actionIdToParameterIndexMap = new HashMap<>();
		this.repository = repository; //
		this.typeAliases = typeAliases;
		this.deletionAction = deletionAction;
		this.additionAction = additionAction;
		this.uiRegister = uiRegister;
		this.updateParameterActions = new HashMap<>();
		this.entryIdToUpdateParameterActions = new HashMap<>();
	}

	@Override
	public String getTitle() {
		return repository.getName() + " management";
	}

	@Override
	public String getReferenceName() {
		return repository.getName();
	}

	@Override
	public String getViewImport() {
		return "repositoryAdministrationPage.jsp";
	}

	@Override
	public int getId() {
		return id;
	}

	@Override
	public String getView() {
		return getViewImport();
	}

	public Model getModel() {
		// first code block prepares all update actions and the model for their
		// visual representation (transfer containers)
		updateParameterActions.clear();
		entryIdToUpdateParameterActions.clear();

		List<EntryTransferContainer> entryTransferContainers = new ArrayList<>();

		for (String entryId : repository.getAllIds()) {

			RepositoryEntry<String, T> entry = repository.getEntry(entryId);

			// TODO: The code relies here implicitly on the fact that the next
			// two lists will have the same number of
			// entries. This seems very fragile. Generally compiling the full
			// list of parameters (including nested) should have framework
			// support. Apparently there is a lot of duplication already for
			// compiling this list in various clients.
			//
			T content = entry.getContent();
			RepositoryParameterUpdateAction<T> updateAction = new RepositoryParameterUpdateAction<T>(
					uiRegister.getNextId(), repository, entryId);
			updateParameterActions.put(updateAction.getId(), updateAction);
			entryIdToUpdateParameterActions.put(entryId, updateAction);

			List<AbstractTransferableParameterState> paramTransferContainers = (content instanceof ParameterContainer)
					? ParameterToJsonConverter.convertRecursively(
							((ParameterContainer) content).getTopLevelParameters())
					: ImmutableList.of();

			String name = content.getClass().getSimpleName();

			entryTransferContainers
					.add(new EntryTransferContainer(entryId, name, paramTransferContainers, updateAction.getId()));
		}

		BindingAwareModelMap map = new BindingAwareModelMap();
		map.put("entries", entryTransferContainers);
		map.put("componentId", id);
		map.put("entryDeletionAction", deletionAction);
		map.put("entryAdditionAction", additionAction);
		map.put("additionTypes", typeAliases);
		return map;
	}

	@Override
	public List<UiComponent> getComponents() {
		return Collections.emptyList();
	}

	@Override
	public ImmutableSet<String> getOwnScriptFilenames() {
		return ImmutableSet.of("creedo-parameters.js", "creedo-repoadmin.js");
	}

	@Override
	public Collection<Integer> getActionIds() {
		/**
		 * seems wrong; deletion action and adition action seem to be missing
		 */
		return actionIdToParameterIndexMap.keySet();
	}

	@Override
	public ResponseEntity<String> performAction(int id, String... params) {
		if (id == deletionAction.getId()) {
			return deletionAction.activate(params);
		}

		if (id == additionAction.getId()) {
			return additionAction.activate(params);
		}

		return updateParameterActions.get(id).activate(params);
	}

	@Override
	public boolean isActionAvailable(int id) {
		return false;
	}

	public class EntryTransferContainer {
		private final String id;
		private final String name;
		private final List<AbstractTransferableParameterState> parameters;
		private final int parameterUpdateActionId;

		public EntryTransferContainer(String id, String name, List<AbstractTransferableParameterState> parameters,
				int parameterUpdateActionId) {
			this.id = id;
			this.name = name;
			this.parameters = parameters;
			this.parameterUpdateActionId = parameterUpdateActionId;
		}

		public String getName() {
			return name;
		}

		public String getId() {
			return id;
		}

		public List<AbstractTransferableParameterState> getParameters() {
			return parameters;
		}

		public int getParameterUpdateActionId() {
			return parameterUpdateActionId;
		}
	}

	@Override
	public ImmutableSet<String> getDataItemIds() {
		return ImmutableSet.copyOf(repository.getAllIds());
	}

	@Override
	public List<? extends Object> getDataItem(String dataItemId) {
		RepositoryEntry<String, T> entry = repository.getEntry(dataItemId);
		T content = entry.getContent();
		if (content instanceof ParameterContainer) {
			return ParameterToJsonConverter.convertRecursively(((ParameterContainer) content).getTopLevelParameters());
		}
		return ImmutableList.of();
	}
}
