package de.unibonn.creedo.ui.repository;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import de.unibonn.creedo.repositories.Repository;
import de.unibonn.creedo.ui.core.Action;

public class RepositoryEntryAdditionAction<T> implements Action {

	private final int id;
	private final Repository<String, T> repository;
	private final List<Class<?>> addClasses;

	public RepositoryEntryAdditionAction(int id,
			Repository<String, T> repository, List<Class<?>> addClasses) {
		this.id = id;
		this.repository = repository;
		this.addClasses = addClasses;
	}

	@Override
	public String getReferenceName() {
		return "Add";
	}

	@Override
	public ResponseEntity<String> activate(String... params) {
		checkNotNull(params);
		checkArgument(params.length == 2,
				"Must receive exactly two parameters: one identifier and one class index");
		checkArgument(Integer.parseInt(params[1]) < addClasses.size(),
				"Class index out of bounds");

		Class<?> addClass = addClasses.get(Integer.parseInt(params[1]));

		try {
			repository.add(params[0], (T) addClass.newInstance());
		} catch (NumberFormatException | InstantiationException
				| IllegalAccessException e) {
			return new ResponseEntity<String>("Could not instantiate class",
					HttpStatus.EXPECTATION_FAILED);
		}
		return new ResponseEntity<String>(HttpStatus.OK);

	}

	@Override
	public ClientWindowEffect getEffect() {
		return ClientWindowEffect.REFRESH;
	}

	@Override
	public int getId() {
		return id;
	}

}
