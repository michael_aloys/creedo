package de.unibonn.creedo.ui.core;

import java.util.List;
import java.util.Optional;

import com.google.common.collect.ImmutableList;

import de.unibonn.creedo.ApplicationRepositories;
import de.unibonn.creedo.repositories.IdentifierInRepositoryParameter;
import de.unibonn.creedo.repositories.Repositories;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.ParameterContainer;
import de.unibonn.realkd.common.parameter.Parameters;

public class DefaultPageContainerOptions implements ParameterContainer {

	private Parameter<Optional<String>> primaryLogoFilename;
	private Parameter<String> primaryLogoLinkUrl;
	private Parameter<Optional<String>> secondaryLogoFilename;
	private Parameter<String> secondaryLogoLinkUrl;

	public DefaultPageContainerOptions() {
		this.primaryLogoFilename = IdentifierInRepositoryParameter.optionalRepositoryIdParameter("Primary logo",
				"Optional logo image file to be displayed along with page title",
				ApplicationRepositories.CONTENT_FOLDER_REPOSITORY,
				Repositories.getIdIsFilenameWithExtensionPredicate(Repositories.IMAGE_FILE_EXTENSIONS));
		this.primaryLogoLinkUrl = Parameters.stringParameter("Primary logo link",
				"Hyperlink for primary logo (ignored if no primary logo selected)", "", s -> true, "");
		this.secondaryLogoFilename = IdentifierInRepositoryParameter.optionalRepositoryIdParameter("Secondary logo",
				"Optional additional logo image file to be displayed along with page title",
				ApplicationRepositories.CONTENT_FOLDER_REPOSITORY,
				Repositories.getIdIsFilenameWithExtensionPredicate(Repositories.IMAGE_FILE_EXTENSIONS));
		this.secondaryLogoLinkUrl = Parameters.stringParameter("Secondary logo link",
				"Hyperlink for secondary logo (ignored if no secondary logo selected)", "", s -> true, "");

	}

	@Override
	public List<Parameter<?>> getTopLevelParameters() {
		return ImmutableList.of(primaryLogoFilename, primaryLogoLinkUrl, secondaryLogoFilename, secondaryLogoLinkUrl);
	}
	
	public Optional<String> getPrimaryLogoFilename() {
		return primaryLogoFilename.current();
	}
	
	public Optional<String> getSecondaryLogoFilename() {
		return secondaryLogoFilename.current();
	}
	
	public String getPrimaryLogoLink() {
		return primaryLogoLinkUrl.current();
	}
	
	public String getSecondaryLogoLink() {
		return secondaryLogoLinkUrl.current();
	}
	

}
