package de.unibonn.creedo.ui.core;

import static de.unibonn.realkd.common.parameter.Parameters.stringParameter;

import java.util.List;
import java.util.Optional;

import com.google.common.collect.ImmutableList;

import de.unibonn.creedo.ApplicationRepositories;
import de.unibonn.creedo.Creedo;
import de.unibonn.creedo.repositories.IdentifierInRepositoryParameter;
import de.unibonn.creedo.repositories.Repositories;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.ParameterContainer;

/**
 * <p>
 * Provides access to options used by {@link DefaultFrame} which are published
 * to the global options repository.
 * </p>
 * <p>
 * NOTE: this should never be instantiated directly except by the options
 * repository. Constructor is currently only public in order for repository to
 * be able to create an instance.
 * </p>
 * 
 * @author Mario Boley
 * 
 * @since 0.3.0
 * 
 * @version 0.3.0
 *
 */
public class DefaultFrameOptions implements ParameterContainer {

	public Optional<String> getCustomCssFileName() {
		return customCssFilename.current();
	}

	public String titlePrefix() {
		return titlePrefix.current();
	}

	private final Parameter<Optional<String>> customCssFilename;

	private final Parameter<String> titlePrefix;

	public DefaultFrameOptions() {
		customCssFilename = IdentifierInRepositoryParameter.optionalRepositoryIdParameter("Custom css",
				"Name of custom css file to be injected into every default frame instance",
				ApplicationRepositories.CONTENT_FOLDER_REPOSITORY,
				Repositories.getIdIsFilenameWithExtensionPredicate("css"));
		titlePrefix = stringParameter("Title prefix", "A non-empty prefix for the title of all default frames.",
				Creedo.name(), x -> x.length() > 0, "Enter non-empty string.");
	}

	@Override
	public List<Parameter<?>> getTopLevelParameters() {
		return ImmutableList.of(titlePrefix, customCssFilename);
	}

}
