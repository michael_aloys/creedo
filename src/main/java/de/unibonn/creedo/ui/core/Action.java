package de.unibonn.creedo.ui.core;

import org.springframework.http.ResponseEntity;

/**
 * <p>
 * Interface for UI actions that can be triggered by the user. Interfaces with
 * JSP. Therefore classic Java conventions (getter name and nullable return
 * values).
 * </p>
 * <p>
 * WARNING: default members only guarantee compilation of old Action
 * implementations. For correct interaction with jsp one needs the class
 * implementing these methods. See {@link DefaultAction}.
 * </p>
 * 
 * @author Mario Boley
 * 
 * @since 0.1.1
 * 
 * @version 0.4.0
 *
 */
public interface Action {

	public enum ClientWindowEffect {
		REFRESH, REDIRECT, POPUP, CLOSE, NONE;
	}

	/**
	 * 
	 * @return name that can be used within the ui to refer to the action
	 * 
	 */
	public String getReferenceName();

	/**
	 * Performs the action changing the state of the ui.
	 * 
	 */
	public ResponseEntity<String> activate(String... params);

	public ClientWindowEffect getEffect();

	/**
	 * 
	 * @return id string that is unique within the reference space (that is
	 *         unique among actions in frame or page, respectively), in which
	 *         controller will try to resolve action
	 * 
	 */
	public int getId();

	/**
	 * @return whether the user should be asked for confirmation before client
	 *         issues action request to server
	 * 
	 * @since 0.4.0
	 * 
	 */
	public default boolean getConfirmed() {
		return false;
	}

	/**
	 * 
	 * @return whether the user should be prompted to input a parameter value
	 *         for this action that is send along with the action request
	 * 
	 * @since 0.4.0
	 * 
	 */
	public default boolean getHasInputParameter() {
		return false;
	}

	public default String getInputParameterName() {
		throw new UnsupportedOperationException();
	}

}
