package de.unibonn.creedo.ui.core;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.http.ResponseEntity;

import de.unibonn.creedo.ui.core.Action.ClientWindowEffect;

/**
 * View model for an action that provides emphasize/de-emphasize state, which
 * can be used by ui elements (navbar, footer) to, e.g., indicate a currently
 * selected option.
 * 
 * Allows specification of "modal siblings" that are automatically de-emphasized
 * when this is activated.
 * 
 * @author Mario Boley
 * 
 * @since 0.1.0
 * 
 * @version 0.1.0
 * 
 *          TODO move reference name to here from action
 *
 */
public class ActionLink {

	private final Action action;

	private boolean emphasis;

	private Collection<ActionLink> modalSiblings;

	public String getName() {
		return action.getReferenceName();
	}

	public int getId() {
		return action.getId();
	}

	public ActionLink(Action action) {
		this.action = action;
		this.modalSiblings = new ArrayList<ActionLink>();
	}

	/**
	 * Specify modal siblings that are automatically de-emphasized if this link
	 * is activated.
	 */
	public void setModalSiblings(Collection<ActionLink> modalSiblings) {
		if (modalSiblings == null) {
			throw new IllegalArgumentException("May not be null");
		}
		this.modalSiblings = modalSiblings;
	}

	public ResponseEntity<String> activate() {
		ResponseEntity<String> result = this.action.activate();

		// if action result will be displayed in same window then emphasize
		// action link
		if (action.getEffect() == ClientWindowEffect.REFRESH) {
			for (ActionLink link : modalSiblings) {
				link.emphasis = false;
			}
			this.emphasis = true;
		}

		return result;
	}

	public boolean isEmphasized() {
		return emphasis;
	}

	public ClientWindowEffect getClientWindowEffect() {
		return action.getEffect();
	}

}