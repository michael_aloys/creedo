package de.unibonn.creedo.ui.core;

import org.springframework.ui.Model;
import org.springframework.validation.support.BindingAwareModelMap;

/**
 * Empty default page with no content.
 *
 * @author mboley, bjacobs
 *
 */
public class EmptyPage implements Page {

	@Override
	public String getTitle() {
		return "Empty";
	}

	@Override
	public String getReferenceName() {
		return "Empty";
	}

	@Override
	public String getViewImport() {
		return "emptyPage.jsp";
	}

	@Override
	public Model getModel() {
		return new BindingAwareModelMap();
	}

}
