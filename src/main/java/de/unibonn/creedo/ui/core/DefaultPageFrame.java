package de.unibonn.creedo.ui.core;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.unibonn.creedo.ui.standard.Footer;
import de.unibonn.creedo.ui.standard.Navbar;

/**
 * Default frame that can display pages and links to actions in a navbar and a
 * footer.
 * 
 * @author Mario Boley
 * 
 * @since 0.1.0
 * 
 * @version 0.4.0
 *
 */
public class DefaultPageFrame extends DefaultFrame implements PageFrame {

	private final PageContainer pageContainer;

	public DefaultPageFrame(int id, PageContainer pageContainer, Navbar navbar,
			Footer footer, UiComponent... additionalComponents) {
		super(id, orderArguments(pageContainer, navbar, footer,
				additionalComponents), "contentPageFrame");
		this.pageContainer = pageContainer;
	}

	private static List<UiComponent> orderArguments(
			PageContainer pageContainer, Navbar navbar, Footer footer,
			UiComponent... additionalComponents) {
		ArrayList<UiComponent> result = new ArrayList<>();
		result.add(navbar);
		result.add(pageContainer);
		result.addAll(Arrays.asList(additionalComponents));
		result.add(footer);
		return result;
	}

	@Override
	public void loadPage(Page page) {
		this.pageContainer.loadPage(page);
	}

	@Override
	public PageContainer pageContainer() {
		return pageContainer;
	}

}
