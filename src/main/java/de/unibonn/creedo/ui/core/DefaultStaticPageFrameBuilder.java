package de.unibonn.creedo.ui.core;

import javax.servlet.http.HttpSession;

import com.google.common.collect.ImmutableList;

import de.unibonn.creedo.ui.core.UiRegister.FrameCloser;
import de.unibonn.creedo.ui.core.UiRegister.IdGenerator;
import de.unibonn.creedo.ui.standard.CloseAction;
import de.unibonn.creedo.ui.standard.Footer;
import de.unibonn.creedo.ui.standard.Navbar;
import de.unibonn.creedo.webapp.handler.TimeoutHandler;

public class DefaultStaticPageFrameBuilder implements PreparedFrame {

	private final Page page;

	private boolean addTimeOutHandler = false;

	public DefaultStaticPageFrameBuilder(Page page) {
		this.page = page;
	}

	public DefaultStaticPageFrameBuilder addTimeOutHandler(boolean add) {
		this.addTimeOutHandler = add;
		return this;
	}

	@Override
	public Frame build(IdGenerator idGenerator, FrameCloser closer,
			HttpSession session) {

		PageContainer pageContainer = new PageContainer(idGenerator.getNextId());

		pageContainer.loadPage(page);
		int frameId = idGenerator.getNextId();
		CloseAction closeAction = new CloseAction(idGenerator.getNextId(),
				closer);

		Navbar navbar = new Navbar(idGenerator.getNextId(),
				ImmutableList.of(new ActionLink(closeAction)));
		Footer footer = new Footer(idGenerator.getNextId(), ImmutableList.of());
		if (addTimeOutHandler) {
			return new DefaultPageFrame(frameId, pageContainer, navbar, footer,
					new TimeoutHandler(idGenerator.getNextId(), closer));
		} else {
			return new DefaultPageFrame(frameId, pageContainer, navbar, footer);
		}
	}
}