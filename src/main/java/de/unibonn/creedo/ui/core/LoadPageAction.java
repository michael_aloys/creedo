package de.unibonn.creedo.ui.core;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * Action that loads specific page into certain page container.
 * 
 * @author Mario Boley
 * 
 * @since 0.1.0
 * 
 * @version 0.1.0
 *
 */
public class LoadPageAction implements Action {

	private final Page page;

	private final PageContainer container;

	private final int id;

	private final String referenceName;

	/**
	 * @param id
	 *            that must be unique within reference space of action (page or
	 *            frame)
	 * 
	 * @param page
	 *            the page loaded by this action
	 * @param container
	 *            the container into which the action loads the page
	 */
	public LoadPageAction(int id, Page page, PageContainer container) {
		this(id, page, container, page.getReferenceName());
	}

	public LoadPageAction(int id, Page page, PageContainer container,
			String referenceName) {
		this.id = id;
		this.page = page;
		this.container = container;
		this.referenceName = referenceName;
	}

	@Override
	public String getReferenceName() {
		return referenceName;
	}

	@Override
	public ResponseEntity<String> activate(String... params) {
		container.loadPage(page);
		return new ResponseEntity<String>(HttpStatus.OK);
	}

	@Override
	public ClientWindowEffect getEffect() {
		return ClientWindowEffect.REFRESH;
	}

	@Override
	public int getId() {
		return id;
	}

}
