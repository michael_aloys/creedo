package de.unibonn.creedo.ui.core;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.validation.support.BindingAwareModelMap;

import com.google.common.collect.ImmutableSet;

import de.unibonn.creedo.admin.options.Configurable;

/**
 * Ui component that displays pages within a "frame" that contains also the page
 * title and customizable logos.
 * 
 * @author Mario Boley
 * 
 * @since 0.1.0
 * 
 * @version 0.1.0.1
 *
 */
public class PageContainer implements ActionProvider, UiComponent, Configurable<DefaultPageContainerOptions> {

	private Page page;

	private final int id;

	public PageContainer(int id) {
		this.id = id;
		page = new EmptyPage();
	}

	public void loadPage(Page page) {
		this.page = page;
	}

	public Model getModel() {
//		ConfigurationProperties config = ConfigurationProperties.get();
		DefaultPageContainerOptions options = getOptions();
		Model model = new BindingAwareModelMap();
		model.addAllAttributes(page.getModel().asMap());
		model.addAttribute("pageView", page.getViewImport());
		model.addAttribute("title", page.getTitle());
		// Set logo images and links
		// model.addAttribute("primaryLogoFilePath", config.PRIMARY_LOGO_PATH);
		// model.addAttribute("primaryLogoLink", config.PRIMARY_LOGO_LINK);
		// model.addAttribute("secondaryLogoFilePath",
		// config.SECONDARY_LOGO_PATH);
		// model.addAttribute("secondaryLogoLink", config.SECONDARY_LOGO_LINK);
		model.addAttribute("primaryLogoFilePath", options.getPrimaryLogoFilename().orElse(null));
		model.addAttribute("primaryLogoLink", options.getPrimaryLogoLink());
		model.addAttribute("secondaryLogoFilePath", options.getSecondaryLogoFilename().orElse(null));
		model.addAttribute("secondaryLogoLink", options.getSecondaryLogoLink());
		return model;
	}

	@Override
	public Collection<Integer> getActionIds() {
		if (page instanceof ActionProvider) {
			return ((ActionProvider) page).getActionIds();
		} else {
			return Arrays.asList();
		}
	}

	@Override
	public ResponseEntity<String> performAction(int id, String... params) {
		if (page instanceof ActionProvider) {
			return ((ActionProvider) page).performAction(id, params);
		} else {
			throw new IllegalStateException("Does not provide any action");
		}
	}

	@Override
	public boolean isActionAvailable(int id) {
		return ((page instanceof ActionProvider) && ((ActionProvider) page).isActionAvailable(id));
	}

	@Override
	public String getView() {
		return "pageContainer.jsp";
	}

	@Override
	public int getId() {
		return id;
	}

	@Override
	public List<UiComponent> getComponents() {
		return Arrays.asList();
	}

	public ImmutableSet<String> getOwnScriptFilenames() {
		if (page instanceof UiComponent) {
			return ((UiComponent) page).getAllScriptFilenames();
		}
		return ImmutableSet.of();
	}

	@Override
	public DefaultPageContainerOptions getDefaultOptions() {
		return new DefaultPageContainerOptions();
	}

}
