package de.unibonn.creedo.ui.core;

import java.util.function.Function;

import org.springframework.http.ResponseEntity;

/**
 * <p>
 * Default implementation of {@link Action}. Interfaces with JSP. Therefore uses
 * classic Java conventions.
 * </p>
 * 
 * @author Mario Boley
 * 
 * @since 0.4.0
 * 
 * @version 0.4.0
 *
 */
public class DefaultAction implements Action {

	public static DefaultAction action(int id, String referenceName, Function<String[], ResponseEntity<String>> action,
			ClientWindowEffect effect) {
		return new DefaultAction(id, referenceName, action, effect, null, false);
	}

	public static DefaultAction action(int id, String referenceName, Function<String[], ResponseEntity<String>> action,
			ClientWindowEffect effect, boolean confirmed) {
		return new DefaultAction(id, referenceName, action, effect, null, confirmed);
	}

	public static DefaultAction action(int id, String referenceName, Function<String[], ResponseEntity<String>> action,
			ClientWindowEffect effect, boolean confirmed, String inputParameterName) {
		return new DefaultAction(id, referenceName, action, effect, inputParameterName, confirmed);
	}

	private final int id;

	private final String referenceName;

	private final Function<String[], ResponseEntity<String>> action;

	private final ClientWindowEffect effect;

	private final String inputParameterName;

	private final boolean confirmed;

	private DefaultAction(int id, String referenceName, Function<String[], ResponseEntity<String>> action,
			ClientWindowEffect effect, String inputParameterName, boolean confirmed) {
		this.id = id;
		this.referenceName = referenceName;
		this.action = action;
		this.effect = effect;
		this.inputParameterName = inputParameterName;
		this.confirmed = confirmed;
	}

	@Override
	public String getReferenceName() {
		return referenceName;
	}

	@Override
	public ResponseEntity<String> activate(String... params) {
		return action.apply(params);
	}

	@Override
	public ClientWindowEffect getEffect() {
		return effect;
	}

	@Override
	public int getId() {
		return id;
	}

	public boolean getHasInputParameter() {
		return inputParameterName != null;
	}

	public String getInputParameterName() {
		return inputParameterName;
	}

	public boolean getConfirmed() {
		return confirmed;
	}

}
