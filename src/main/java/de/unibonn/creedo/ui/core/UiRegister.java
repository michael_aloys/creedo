package de.unibonn.creedo.ui.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpSession;

import org.springframework.ui.Model;

import com.google.common.collect.ImmutableList;

import de.unibonn.creedo.ui.HtmlPage;
import de.unibonn.creedo.ui.standard.NavbarFooterModalGroup;

/**
 * <p>
 * Stores UI components for later reference via id. Allows the registration of
 * call back receivers for the event when components are deregistered again.
 * </p>
 * 
 * @author Mario Boley
 * 
 * @since 0.1.0
 * 
 * @version 0.1.2.2
 *
 */
public class UiRegister {

	private static final Logger LOGGER = Logger.getLogger(UiRegister.class.getName());

	public static class IdGenerator {

		private volatile int nextId;

		public IdGenerator() {
			this.nextId = 0;
		}

		public synchronized int getNextId() {
			return nextId++;
		}

	}

	/**
	 * Object that can be used to close some specific frame. {@link UiRegister}
	 * provides link to frame closer to frame builder on construction. Builder
	 * can pass this on to components that should be allowed to close frame such
	 * as a close button.
	 *
	 */
	public interface FrameCloser {

		/**
		 * Requests closing of some specific frame. If called during the
		 * construction process of frame then construction and registration
		 * process is first completed and then frame is teared down.
		 */
		public void requestClose();

	}

	/**
	 * @see UiRegister#addTearDownCallbackReceiver(Integer,
	 *      TearDownCallbackReceiver)
	 *
	 */
	public interface TearDownCallbackReceiver {

		public void call();

	}

	private final HttpSession httpSession;

	private final Map<Integer, UiComponent> idToUiComponents;

	private final Map<Integer, List<TearDownCallbackReceiver>> idToTearDownOperations;
	
	private Frame dashboard = null;

	private final IdGenerator idGenerator;

	private static final PreparedFrame FRAME_CONSTRUCTION_ERROR_FRAME_BUILDER = new DefaultStaticPageFrameBuilder(
			new HtmlPage("Error", "Error",
					"It seems that the link you opened is no longer valid. Please close this window and refresh the index page."));

	public UiRegister(HttpSession httpSession) {
		this.idToUiComponents = new HashMap<>();
		this.idToTearDownOperations = new HashMap<>();
		this.httpSession = httpSession;
		this.idGenerator = new IdGenerator();
	}

	/**
	 * <p>
	 * Allows registration of callback receiver that will be called after
	 * component has been teared down and deregistered.
	 * </p>
	 * 
	 * @param componentId
	 *            id of component for which call back should be received after
	 *            deregistration
	 * @param callbackReceiver
	 *            receiver of call
	 */
	public void addTearDownCallbackReceiver(Integer componentId, TearDownCallbackReceiver callbackReceiver) {
		if (!hasComponent(componentId)) {
			LOGGER.warning("Could not add tear down callback for component " + componentId
					+ " (no such component registered)");
			return;
		}
		if (!(idToTearDownOperations.get(componentId) instanceof List)) {
			idToTearDownOperations.put(componentId, new ArrayList<TearDownCallbackReceiver>());
		}
		idToTearDownOperations.get(componentId).add(callbackReceiver);
		LOGGER.info("Added tear down callback receiver for component " + componentId);
	}

	/**
	 * Creates frame object and registers it to UI register.
	 */
	public DefaultPageFrame createDefaultFrame(PageContainer pageContainer, List<Action> navbarActions,
			List<Action> footerActions) {
		int id = getIdGenerator().getNextId();

		NavbarFooterModalGroup modalGroup = new NavbarFooterModalGroup(this, navbarActions, footerActions);

		DefaultPageFrame result = new DefaultPageFrame(id, pageContainer, modalGroup.getNavbar(),
				modalGroup.getFooter());

		// init frame to show first navbar page
		if (!navbarActions.isEmpty()) {
			result.performAction(navbarActions.get(0).getId());
		}

		registerUiComponent(result);
		return result;
	}

	/**
	 * Creates frame based on static jsp view and registers it.
	 */
	public Frame createStaticFrame(final Model model, final String view) {

		int id = getIdGenerator().getNextId();

		Frame result = new Frame() {

			@Override
			public String getView() {
				return view;
			}

			@Override
			public Model getModel() {
				return model;
			}

			@Override
			public List<UiComponent> getComponents() {
				return ImmutableList.of();
			}

			@Override
			public int getId() {
				return id;
			}

		};

		registerUiComponent(result);
		return result;
	}

	public synchronized int getNextId() {
		return getIdGenerator().getNextId();
	}

	public IdGenerator getIdGenerator() {
		return this.idGenerator;
	}

	private class FrameCloserImplementation implements FrameCloser {

		private Integer frameId = null;

		private boolean closeRequested = false;

		private void setFrameId(int frameId) {
			this.frameId = frameId;
		}

		private boolean wasCloseRequested() {
			return this.closeRequested;
		}

		@Override
		public void requestClose() {
			if (frameId != null) {
				tearDownComponent(frameId);
			} else {
				closeRequested = true;
			}
		}

	}

	/**
	 * Creates frame from frame builder; closer object is passed to builder on
	 * build call, which can be used by components of the frame to close it
	 * (e.g. close button).
	 * 
	 * @param builder
	 * 
	 * @return constructed frame object
	 * 
	 */
	public Frame createFrameFromBuilder(PreparedFrame builder) {
		// int id = getIdGenerator().getNextId();
		builder.notifyAboutToOpen();
		FrameCloserImplementation closer = new FrameCloserImplementation();
		Frame frame = builder.build(getIdGenerator(), closer, httpSession);
		registerUiComponent(frame);
		if (closer.wasCloseRequested()) {
			tearDownComponent(frame.getId());
			// TODO: infinite loop danger if error frame construction requests
			// close; guarantee or document requirement
			return createFrameFromBuilder(FRAME_CONSTRUCTION_ERROR_FRAME_BUILDER);
		}
		closer.setFrameId(frame.getId());
		return frame;
	}
	
	/**
	 * Creates a new dashboard inside this Creedo session that based on the
	 * provided configuration. Tears down current root-level dashboard if one
	 * exists.
	 * 
	 */
	public void createMiningDashboard(PreparedFrame builder) {
		tearDownDashboard();
		LOGGER.log(Level.INFO, "Constructing analytics dashboard");
		Frame frame = createFrameFromBuilder(builder);
		dashboard = frame;
		addTearDownCallbackReceiver(frame.getId(), () -> {
			LOGGER.info("Freeing analytics dashboard slot");
			dashboard = null;
		});
		LOGGER.log(Level.INFO, "Registered new analytics dashboard");
	}
	
	/**
	 * Frees the resources claimed by current root-level dashboard (if there is
	 * one).
	 * 
	 */
	public void tearDownDashboard() {
		if (dashboard != null) {
			LOGGER.log(Level.INFO,
					"Attempting to tear down current analytics dashboard");
			tearDownComponent(dashboard.getId());
		}
	}

	public Frame getDashboard() {
		return dashboard;
	}

	public boolean analyticsDashboardOpen() {
		return dashboard != null;
	}

	/**
	 * Registers ui component along with its sub-components (recursively) so
	 * that all those components can be subsequently retrieved via
	 * {@link #retrieveUiComponent(int)} via their id.
	 * 
	 * @param component
	 *            the ui component to be registered
	 */
	public void registerUiComponent(UiComponent component) {
		if (idToUiComponents.containsKey(component.getId())) {
			LOGGER.warning(
					"Component with same id " + component.getId() + " is already registered (aborting registration)");
			return;
		}
		LOGGER.fine("Registering component under id '" + component.getId() + "'");
		this.idToUiComponents.put(component.getId(), component);
		for (UiComponent childComponent : component.getComponents()) {
			registerUiComponent(childComponent);
		}
	}

	/**
	 * <p>
	 * Recursively calls {@link #tearDownComponent(y)} for all child components
	 * of a specified component, then {@link UiComponent#tearDown()} for the
	 * specified component itself, and finally de-registers it.
	 * </p>
	 * <p>
	 * NOTE: Issues warning when no component found with id. No exception
	 * thrown. Hence, can be called offensively in order to assure destruction.
	 * This is subject to change in future versions.
	 * </p>
	 * 
	 * @param componentId
	 *            integer id of component to be teared down
	 * 
	 */
	public void tearDownComponent(Integer componentId) {
		if (!hasComponent(componentId)) {
			LOGGER.warning("Component '" + componentId
					+ "' not found for tear down (might still be running without reference in ui register)");
			return;
		}
		UiComponent component = retrieveUiComponent(componentId);
		component.getComponents().forEach(c -> {
			this.tearDownComponent(c.getId());
		});
		LOGGER.fine("Exectuting tearDown() and deregistering component '" + componentId + "'");
		component.tearDown();
		idToUiComponents.put(componentId, null);

		List<TearDownCallbackReceiver> tearDownCallbackReceivers = idToTearDownOperations.get(componentId);
		if (tearDownCallbackReceivers instanceof List) {
			LOGGER.info("Executing callback receivers for tear down");
			tearDownCallbackReceivers.forEach(TearDownCallbackReceiver::call);
			idToTearDownOperations.put(componentId, null);
		} else {
			LOGGER.fine("No tear down callback receivers");
		}
	}

	public boolean hasComponent(int id) {
		return idToUiComponents.containsKey(id) && (idToUiComponents.get(id) instanceof UiComponent);
	}

	public UiComponent retrieveUiComponent(int id) {
		UiComponent result = idToUiComponents.get(id);
		if (result == null) {
			throw new IllegalArgumentException("No ui component with id " + id);
		}
		return result;
	}

}
