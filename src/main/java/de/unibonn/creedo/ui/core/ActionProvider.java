package de.unibonn.creedo.ui.core;

import java.util.Collection;

import org.springframework.http.ResponseEntity;

import de.unibonn.creedo.DynamicUrlController;

/**
 * UI Component that provides a collection of client actions that can be
 * triggered via the http request handled by
 * {@link DynamicUrlController#performAction(int, int, String[])} .
 * 
 * @author Mario Boley
 * 
 * @since 0.1.0
 * 
 * @version 0.1.0.1
 * 
 * @see DynamicUrlController
 *
 */
public interface ActionProvider {

	public Collection<Integer> getActionIds();

	public ResponseEntity<String> performAction(int id, String... params);

	public default boolean isActionAvailable(int id) {
		return getActionIds().contains(id);
	}

}
