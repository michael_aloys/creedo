package de.unibonn.creedo.ui.core;

import java.util.LinkedHashSet;
import java.util.List;

import org.springframework.ui.Model;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import de.unibonn.creedo.webapp.CreedoSession;

/**
 * Components of user interaction that can be registered in {@link UiRegister}
 * of a {@link CreedoSession} usually as part of a {@link Frame}.
 * 
 * @author Mario Boley
 * 
 * @since 0.1.0
 * 
 * @version 0.1.2.2
 * 
 * @see DefaultPageFrame
 *
 */
public interface UiComponent {

	public abstract int getId();

	public abstract String getView();

	public abstract Model getModel();

	/**
	 * <p>
	 * List of sub-components of this component. These components are
	 * automatically registered and teared down via the respective methods of
	 * the {@link UiRegister} of a Creedo session.
	 * </p>
	 * <p>
	 * NOTE: this assumes static components only, i.e., the returned list does
	 * not change throughout the lifetime of object.
	 * </p>
	 * 
	 * @return list of sub-components
	 * 
	 */
	public default List<UiComponent> getComponents() {
		return ImmutableList.of();
	}

	public default ImmutableSet<String> getOwnScriptFilenames() {
		return ImmutableSet.of();
	}

	/**
	 * <p>
	 * This should usually not be overwritten. Overwrite
	 * {@link UiComponent#getAllScriptFilenames()} to add script requirements
	 * for this component.
	 * </p>
	 * 
	 * @return all script files that have to be loaded in order to make this
	 *         component and all its subcomponents work properly
	 */
	public default ImmutableSet<String> getAllScriptFilenames() {
		LinkedHashSet<String> result = new LinkedHashSet<String>();
		for (UiComponent component : getComponents()) {
			result.addAll(component.getAllScriptFilenames());
		}
		result.addAll(getOwnScriptFilenames());
		return ImmutableSet.copyOf(result);
	}

	/**
	 * <p>
	 * Frees up all resources used by this component, e.g., running mining
	 * algorithms, and performs other mandatory exit operations such as
	 * serialization of persistent state.
	 * </p>
	 * 
	 * <p>
	 * Note that no explicit call of {@link #tearDown()} on child components is
	 * required. The Creedo UI framework calls method recursively on child
	 * components when tear down is initialized.
	 * </p>
	 * 
	 * @see UiRegister#tearDownComponent(int)
	 * 
	 */
	public default void tearDown() {
		;
	}

	/**
	 * <p>
	 * Provides optional set of actions that a container can publish at a place
	 * external to the view of the component such as, e.g., a navbar.
	 * </p>
	 * 
	 * @return set of external actions provided by this component
	 * 
	 */
	public default List<Action> getExternalActions() {
		return ImmutableList.of();
	}

}
