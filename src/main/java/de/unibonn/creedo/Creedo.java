package de.unibonn.creedo;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.servlet.http.HttpSession;

import de.unibonn.creedo.admin.users.Users;
import de.unibonn.creedo.webapp.CreedoSession;

/**
 * <p>
 * Creedo is a Java web application for showcase data analytics dashboards based
 * on realKD algorithms and for testing those dashboards in controlled user
 * studies.
 * </p>
 * 
 * @author Mario Boley
 * @author Björn Jacobs
 * @author Bo Kang
 * @author Pavel Tokmakov
 * @author Elvin Efendiyev
 * @author Maike Krause-Traudes
 * @author Sandy Moens
 * @author Surparno Datta
 * @author Ruofan Xu
 * @author Michael Kamp
 * 
 * @since 0.1.0
 * 
 * @version 0.4.1
 *
 */
public class Creedo {

	private static final Properties PROPERTIES = new Properties();
	static {
		InputStream stream = Creedo.class.getClassLoader().getResourceAsStream("creedo.properties");
		if (stream != null) {
			try {
				PROPERTIES.load(stream);
			} catch (IOException e) {
				System.out.println("corrupt jar---could not read creedo.properties");
				System.exit(1);
			}
		}
	}

	private static final String NAME = "Creedo";

	private static final String COPYRIGHT = "&copy 2014-16 by the Creedo Contributors";

	private static final String VERSION = PROPERTIES.getProperty("version", "unknown version");

	public static String version() {
		return VERSION;
	}

	public static String name() {
		return NAME;
	}

	public static String copyright() {
		return COPYRIGHT;
	}

	/**
	 * Returns Creedo session object already bound to httpSession if one exists
	 * and otherwise creates a new {@link CreedoSession} object with a default
	 * user logged in, binds it to the httpSession, and returns it.
	 * 
	 * @param httpSession
	 *            the HTTP session for which the unique Creedo session is
	 *            requested
	 * @return unique Creedo session within HTTP session
	 */
	public static CreedoSession getCreedoSession(HttpSession httpSession) {
		Object result = CreedoSession.retrieveObjectWithCreedoSessionKey(httpSession);
		if (!(result instanceof CreedoSession)) {
			CreedoSession.createNewCreedoSession(Users.DEFAULT_USER, httpSession);
			result = CreedoSession.retrieveObjectWithCreedoSessionKey(httpSession);
		}
		return (CreedoSession) result;
	}

}
