package de.unibonn.creedo;

import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.function.Supplier;

import de.unibonn.creedo.repositories.Repository;
import de.unibonn.creedo.repositories.filesystimpl.FileSystemBasedRepository;
import de.unibonn.creedo.repositories.mybatisimpl.MyBatisRepository;
import de.unibonn.creedo.studies.StudyBuilder;
import de.unibonn.creedo.studies.designs.EvaluationScheme;
import de.unibonn.creedo.studies.designs.StudyDesignBuilder;
import de.unibonn.creedo.studies.designs.SystemSpecBuilder;
import de.unibonn.creedo.studies.designs.TaskSpecBuilder;
import de.unibonn.creedo.ui.core.Page;
import de.unibonn.creedo.ui.indexpage.DashboardLinkBuilder;
import de.unibonn.creedo.ui.indexpage.StoredDashboardLinkBuilder;
import de.unibonn.creedo.webapp.CreedoSession;
import de.unibonn.creedo.webapp.dashboard.mining.MiningSystemBuilder;
import de.unibonn.realkd.common.RuntimeBuilder;
import de.unibonn.realkd.common.workspace.Workspace;

public class ApplicationRepositories {

	public static final Repository<String, Supplier<Workspace>> DATA_REPOSITORY = new MyBatisRepository<>("Data",
			"mining__data", "mining__data_parameters");

	public static final Repository<String, RuntimeBuilder<Page, CreedoSession>> PAGE_REPOSITORY = new MyBatisRepository<>(
			"Pages", "content__pages", "content__pages_parameters");

	public static final Repository<String, DashboardLinkBuilder> DEMO_REPOSITORY = new MyBatisRepository<DashboardLinkBuilder>(
			"Demos", "content__demos", "content__demos_parameters");

	public static final Repository<String, StoredDashboardLinkBuilder> STORED_DASHBOARD_REPOSITORY = new MyBatisRepository<StoredDashboardLinkBuilder>(
			"Stored Dashboards", "content__stored_dashboards", "content__stored_dashboards_parameters");

	public static final Repository<String, MiningSystemBuilder<?>> MINING_SYSTEM_REPOSITORY = new MyBatisRepository<MiningSystemBuilder<?>>(
			"Mining Systems", "mining__mining_systems", "mining__mining_systems_parameters");

	public static final Repository<String, StudyDesignBuilder> STUDY_DESIGN_REPOSITORY = new MyBatisRepository<StudyDesignBuilder>(
			"Study Designs", "content__study_designs", "content__study_designs_parameters");

	public static final Repository<String, StudyBuilder> STUDY_REPOSITORY = new MyBatisRepository<StudyBuilder>(
			"Studies", "content__studies", "content__studies_parameters");

	public static final Repository<String, SystemSpecBuilder> STUDY_SYSTEM_SPECIFICATION_REPOSITORY = new MyBatisRepository<SystemSpecBuilder>(
			"System Specifications", "content__study_system_specifications",
			"content__study_system_specifications_parameters");

	public static final Repository<String, TaskSpecBuilder> STUDY_TASK_SPECIFICATION_REPOSITORY = new MyBatisRepository<TaskSpecBuilder>(
			"Task Specifications", "content__study_task_specifications",
			"content__study_task_specifications_parameters");

	public static final Repository<String, EvaluationScheme> STUDY_EVALUATION_SCHEME_REPOSITORY = new MyBatisRepository<EvaluationScheme>(
			"Evaluation Schemes", "content__study_evaluation_schemes", "content__study_evaluation_schemes_parameters");

	public static final Repository<String, Path> CONTENT_FOLDER_REPOSITORY = new FileSystemBasedRepository("Ressources",
			FileSystems.getDefault().getPath(ConfigurationProperties.get().USER_FOLDER));

	public static final Repository<String, Path> WORKSPACE_FOLDER_REPOSITORY = new FileSystemBasedRepository(
			"Workspaces", FileSystems.getDefault().getPath(ConfigurationProperties.get().WORKSPACE_FOLDER));

}
