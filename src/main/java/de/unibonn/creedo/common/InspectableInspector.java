package de.unibonn.creedo.common;

import de.unibonn.realkd.common.inspection.Inspectable;

/**
 * Unified interface for the client classes which need to inspect the internal
 * state of an inspectable. This is achieved by telling the inspectors the
 * target inspectable.
 * 
 * @see de.unibonn.realkd.common.inspection.Inspectable
 * 
 * @author ???
 * 
 */
public interface InspectableInspector {

	public void tellInspectable(Inspectable inspectable);

}
