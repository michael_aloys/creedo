package de.unibonn.creedo.common.parameters;

import java.util.Optional;

public class PlainParameterState extends AbstractTransferableParameterState {

	private final String value;

	public PlainParameterState(int depth, String type,
			String name, String description, boolean isActive, String dependsOnNotice,
			Optional<String> value, boolean isValid, String solutionHint, boolean hidden) {
		super(depth, type, name, description, isActive,
				dependsOnNotice, isValid, solutionHint, hidden);
		this.value = value.orElse(null);
	}

	public String getValue() {
		return value;
	}

}
