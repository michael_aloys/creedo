package de.unibonn.creedo.common.parameters;

import de.unibonn.realkd.common.parameter.Parameter;

/**
 * Specialization of the Parameter interface for parameters that store their
 * values as secure hash values only.
 * 
 * @author Mario Boley
 *
 * @param <T>
 * 
 * @since 0.3.0
 * 
 * @version 0.3.0
 * 
 */
public interface SecureParameter<T> extends Parameter<T> {

	/**
	 * Allows to set a value as a clear text, to which a hash function will be
	 * applied before storage.
	 * 
	 * @param clearValue
	 *            a value to be hashed
	 * 
	 */
	public void setByClearValue(String clearValue);

}
