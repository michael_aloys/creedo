package de.unibonn.creedo.common.parameters;

import java.util.List;
import java.util.Optional;

/**
 * 
 * @author Björn Jacobs
 * 
 * @since 0.1.0
 * 
 * @version 0.2.1
 *
 */
public class TransferableParameterState extends
		AbstractTransferableParameterState {

	public TransferableParameterState(int depth, String type,
			String name, String description, boolean isActive, String dependsOnNotice,
			boolean isValid, String solutionHint, boolean hidden) {
		super(depth, type, name, description, isActive,
				dependsOnNotice, isValid, solutionHint, hidden);
	}

	private List<String> range;
	private List<String> values;

	public void setRange(List<String> range) {
		this.range = range;
		// this.multiplicity = multiplicity;
	}

	public void setValues(Optional<List<String>> values) {
		this.values = values.orElse(null);
		// if (values == null) {
		// this.values = null;
		// return;
		// }
		// this.values = new ArrayList<String>();
		// for (Object value : values) {
		// this.values.add(String.valueOf(value));
		// }
		// this.values = values;
	}

	// public String getMultiplicity() {
	// return multiplicity;
	// }

	public List<String> getRange() {
		return range;
	}

	public List<String> getValues() {
		return values;
	}

	public String toString() {
		return getName() + " = " + getValues() + ", active: " + isActive()
				+ ", valid: " + getValid();
	}

}
