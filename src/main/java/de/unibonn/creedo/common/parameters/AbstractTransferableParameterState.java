package de.unibonn.creedo.common.parameters;

public abstract class AbstractTransferableParameterState {

	private final int depth;

	private final String type;
	private final String name;
	private final String description;
	private final String solutionHint;
	private final String dependsOnNotice;
	private final boolean valid;
	private final boolean active;
	private final boolean hidden;

	public AbstractTransferableParameterState(int depth, String type, String name, String description, boolean isActive,
			String dependsOnNotice, boolean isValid, String solutionHint, boolean hidden) {
		this.depth = depth;
		this.type = type;
		this.name = name;
		this.description = description;
		this.active = isActive;
		this.dependsOnNotice = dependsOnNotice;
		this.valid = isValid;
		this.solutionHint = solutionHint;
		this.hidden = hidden;
	}

	public boolean isActive() {
		return active;
	}

	public boolean getValid() {
		return valid;
	}

	public boolean isHidden() {
		return hidden;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public String getSolutionHint() {
		return solutionHint;
	}

	public String getDependsOnNotice() {
		return dependsOnNotice;
	}

	public int getDepth() {
		return depth;
	}

	public String getType() {
		return type;
	}

}