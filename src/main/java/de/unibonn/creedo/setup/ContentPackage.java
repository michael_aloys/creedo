package de.unibonn.creedo.setup;

import java.nio.file.Path;
import java.util.Optional;

/**
 * <p>
 * Creedo content packages bundle content files and sql init scripts for
 * initializing the contents of a Creedo server instance.
 * </p>
 * 
 * @author Mario Boley
 * 
 * @since 0.4.0
 * 
 * @version 0.4.0
 * 
 * @see ContentPackages
 *
 */
public interface ContentPackage {
	
	public String id();
	
	public Optional<Path> pathToContentFiles();
	
	public Optional<Path> pathToDbInitScripts();
	
}
