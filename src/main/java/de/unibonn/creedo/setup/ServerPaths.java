package de.unibonn.creedo.setup;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Optional;
import java.util.Scanner;

import de.unibonn.creedo.ApplicationRepositories;
import de.unibonn.creedo.ConfigurationProperties;
import de.unibonn.creedo.admin.users.CreedoUser;

/**
 * Contains static members that allow to resolve pathes on the Creedo server.
 * 
 * WARNING: this might fail if jvm does not perform lazy inits of static fields,
 * because it relies on configuration properties already loaded by spring.
 * Should be turned into object similar to other components.
 * 
 * @author Bjoern Jacobs
 * @author Mario Boley
 * 
 * @since 0.1.0
 * 
 * @version 0.3.0
 *
 */
public class ServerPaths {


	public static final Path ABS_PATH_TO_CONFIGURED_RESSOURCE_DIR = FileSystems.getDefault()
			.getPath(ConfigurationProperties.get().USER_FOLDER);

	public static Optional<Path> newWorkspaceDirectory(CreedoUser user) {
		Path result = null;
		int suffix = 0;
		do {
			suffix++;
			result = ABS_PATH_TO_CONFIGURED_WORKSPACE_DIR.resolve(user.id() + "_" + suffix);
		} while (Files.exists(result));
		try {
			Files.createDirectory(result);
			return Optional.of(result);
		} catch (IOException e) {
			return Optional.empty();
		}
	}

	public static final Path ABS_PATH_TO_CONFIGURED_WORKSPACE_DIR = FileSystems.getDefault()
			.getPath(ConfigurationProperties.get().WORKSPACE_FOLDER);

	public static String getContentFileContentAsString(String pageFileName) {

		try {
			InputStream resourceAsStream = Files.newInputStream(
					ApplicationRepositories.CONTENT_FOLDER_REPOSITORY.getEntry(pageFileName).getContent());
			Scanner scanner = new Scanner(resourceAsStream, "UTF-8");
			String pageContent = scanner.useDelimiter("\\A").next();
			scanner.close();
			return pageContent;
		} catch (IOException e) {
			throw new IllegalStateException("could not access ressource file: " + pageFileName, e);
		}
	}

}
