/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-15 The Contributors of the realKD Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.creedo.boot;

import static de.unibonn.realkd.common.parameter.Parameters.stringParameter;

import java.nio.file.Path;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.logging.Logger;

import com.google.common.collect.ImmutableList;

import de.unibonn.creedo.ApplicationRepositories;
import de.unibonn.creedo.repositories.IdentifierInRepositoryParameter;
import de.unibonn.creedo.repositories.Repositories;
import de.unibonn.creedo.setup.ServerPaths;
import de.unibonn.realkd.common.parameter.DefaultParameter;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.ParameterContainer;
import de.unibonn.realkd.common.workspace.Workspace;
import de.unibonn.realkd.common.workspace.Workspaces;
import de.unibonn.realkd.data.propositions.PropositionalLogicFromTableBuilder;
import de.unibonn.realkd.data.table.DataFormatException;
import de.unibonn.realkd.data.table.DataTable;
import de.unibonn.realkd.data.table.DataTableFromCSVBuilder;

/**
 * 
 * @author Mario Boley
 * 
 * @since 0.1.1
 * 
 * @version 0.1.1
 *
 */
public class FixedComponentWorkspaceBuilder implements ParameterContainer, Supplier<Workspace> {

	private static final Logger LOGGER = Logger.getLogger(FixedComponentWorkspaceBuilder.class.getName());

	private final Parameter<String> dataFileIdentifier;

	private final Parameter<String> attributeFileIdentifier;

	private final Parameter<String> attributeGroupFileIdentifier;

	private final Parameter<Character> delimeter;

	private final Parameter<String> missingCode;

	private final Parameter<String> tableName;

	private final Parameter<String> tableDescription;

	private final List<Parameter<?>> parameters;

	private final PropositionalLogicFromTableBuilder propLogicFactory;

	private final DataTableFromCSVBuilder tableBuilder;

	private Optional<Path> backupPath = Optional.empty();

	public FixedComponentWorkspaceBuilder() {
		this.tableBuilder = new DataTableFromCSVBuilder();
		this.propLogicFactory = new PropositionalLogicFromTableBuilder();
		this.tableName = stringParameter("Table name", "Name of the datatable.", "?",
				value -> value != null && value.length() > 0, "Enter non-empty string.");

		this.tableDescription = stringParameter("Table description", "Description of the datatable.", "?",
				value -> value != null, "Enter description.");

		this.dataFileIdentifier = new IdentifierInRepositoryParameter<>("Data file",
				"File with main data content given in the form of one row per data entity of delimeter-separated values.",
				ApplicationRepositories.CONTENT_FOLDER_REPOSITORY,
				Repositories.getIdIsFilenameWithExtensionPredicate(Repositories.CSV_FILE_EXTENSIONS));
		this.attributeFileIdentifier = new IdentifierInRepositoryParameter<>("Attributes file",
				"File with single attribute meta data given in the form of one row per attribute in the form of 3 delimeter-separated values corresponding to name, type, and description, respectively.",
				ApplicationRepositories.CONTENT_FOLDER_REPOSITORY,
				Repositories.getIdIsFilenameWithExtensionPredicate(Repositories.CSV_FILE_EXTENSIONS));

		this.attributeGroupFileIdentifier = IdentifierInRepositoryParameter.getOptionalIdentifierInRepositoryParameter(
				"Groups file", "File with attribute group meta data content.",
				ApplicationRepositories.CONTENT_FOLDER_REPOSITORY,
				Repositories.getIdIsFilenameWithExtensionPredicate(Repositories.CSV_FILE_EXTENSIONS));

		this.delimeter = new DefaultParameter<Character>("Delimeter",
				"Delimeter used in csv-files to seperate entries (default is ';').", Character.class, ';',
				string -> string.length() == 1 ? string.charAt(0) : ';', value -> value != null, "Enter single symbol");

		this.missingCode = stringParameter("Missing code", "String to indicate missing value in raw data.", "?",
				value -> value != null, "Enter string.");

		this.parameters = ImmutableList.of(tableName, tableDescription, dataFileIdentifier, attributeFileIdentifier,
				attributeGroupFileIdentifier, delimeter, missingCode, tableBuilder.groupMapperParam(),
				propLogicFactory.attributeMapperParamater());
	}

	@Override
	public List<Parameter<?>> getTopLevelParameters() {
		return parameters;
	}

	/**
	 * If provided constructs a file system based workspace that stores
	 * serialized versions of data artifacts in provided path.
	 * 
	 * @param path
	 *            the path for storing the workspace elements
	 * 
	 */
	public void setBackupPath(Path path) {
		this.backupPath = Optional.ofNullable(path);
	}

	@Override
	public Workspace get() {
		validate();
		LOGGER.fine("Building workspace");
		tableBuilder.setId(tableName.current());
		tableBuilder.setDescription(tableDescription.current());
		tableBuilder.setDataCSV(ServerPaths.getContentFileContentAsString(dataFileIdentifier.current()));
		tableBuilder
				.setAttributeMetadataCSV(ServerPaths.getContentFileContentAsString(attributeFileIdentifier.current()));
		tableBuilder.setAttributeGroupCSV(attributeGroupFileIdentifier.current().equals("---") ? ""
				: ServerPaths.getContentFileContentAsString(attributeGroupFileIdentifier.current()));
		tableBuilder.setDelimiter(delimeter.current());
		tableBuilder.setMissingSymbol(missingCode.current());

		Workspace result = backupPath.isPresent() ? Workspaces.fileSystemBasedWorkspace(backupPath.get())
				: Workspaces.workspace();
		DataTable table;
		try {
			table = tableBuilder.build();
			result.add(table.population());
			result.add(table);
		} catch (DataFormatException e) {
			throw new IllegalArgumentException(e);
		}

		result.add(propLogicFactory.build(table));

		return result;
	}
}
