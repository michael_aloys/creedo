package de.unibonn.creedo.boot;

import static com.google.common.base.Preconditions.checkArgument;

import java.util.ArrayList;
import java.util.List;

import de.unibonn.creedo.repositories.Repository;
import de.unibonn.creedo.ui.core.Page;
import de.unibonn.creedo.ui.repository.RepositoryAdministrationPage;
import de.unibonn.creedo.ui.repository.RepositoryEntryAdditionAction;
import de.unibonn.creedo.ui.repository.RepositoryEntryDeletionAction;
import de.unibonn.creedo.webapp.CreedoSession;
import de.unibonn.realkd.common.RuntimeBuilder;

public class DefaultRepositoryAdministrationPageBuilder<T> implements RuntimeBuilder<Page, CreedoSession> {

	private Repository<String, T> repository = null;

	private List<Class<?>> additionClasses = new ArrayList<>();

	private List<String> additionTypeAliases = new ArrayList<>();

	public DefaultRepositoryAdministrationPageBuilder<T> setRepository(Repository<String, T> repository) {
		this.repository = repository;
		return this;
	}

	public DefaultRepositoryAdministrationPageBuilder<T> setAdditionClasses(List<Class<?>> additionClasses) {
		this.additionClasses = additionClasses;
		return this;
	}

	public DefaultRepositoryAdministrationPageBuilder<T> setAdditionTypeAliases(List<String> additionTypeAliases) {
		this.additionTypeAliases = additionTypeAliases;
		return this;
	}

	@Override
	public RepositoryAdministrationPage<T> build(CreedoSession creedoSession) {
		checkArgument(repository != null, "Unspecified repository");
		checkArgument(additionClasses != null, "Unspecified addition classes");
		checkArgument(additionTypeAliases != null, "Unspecified addition type aliases");
		return new RepositoryAdministrationPage<T>(creedoSession.getUiRegister().getNextId(), repository,
				additionTypeAliases,
				new RepositoryEntryDeletionAction(creedoSession.getUiRegister().getNextId(), repository),
				new RepositoryEntryAdditionAction<T>(creedoSession.getUiRegister().getNextId(), repository,
						additionClasses),
				creedoSession.getUiRegister());
	}

}
