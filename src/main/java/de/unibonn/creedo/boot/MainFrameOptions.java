package de.unibonn.creedo.boot;

import java.util.List;

import com.google.common.collect.ImmutableList;

import de.unibonn.creedo.ApplicationRepositories;
import de.unibonn.creedo.repositories.Repositories;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.ParameterContainer;
import de.unibonn.realkd.common.parameter.SubCollectionParameter;

public class MainFrameOptions implements ParameterContainer {

	private final SubCollectionParameter<String, List<String>> optionalNavbarContents;

	private final SubCollectionParameter<String, List<String>> optionalFooterContents;

//	private final Parameter<Boolean> showLoginLink;

	private final List<Parameter<?>> parameters;

	public MainFrameOptions() {
		this.optionalNavbarContents = Repositories.getIdentifierListOfRepositoryParameter("Navbar entries",
				"List of optional content to be linked from the navbar of frame.",
				ApplicationRepositories.PAGE_REPOSITORY);
		this.optionalFooterContents = Repositories.getIdentifierListOfRepositoryParameter("Footer entries",
				"List of optional content to be linked from the footer of frame.",
				ApplicationRepositories.PAGE_REPOSITORY);
//		this.showLoginLink = Parameters.booleanParameter("Show login link",
//				"Whether a link to the login page should be added to the navbar for non logged in users (anonymous default user).");
		this.parameters = ImmutableList.of(optionalNavbarContents, optionalFooterContents);
	}

	@Override
	public List<Parameter<?>> getTopLevelParameters() {
		return parameters;
	}

	public List<String> optionalNavbarContents() {
		return optionalNavbarContents.current();
	}

	public List<String> optionalFooterContents() {
		return optionalFooterContents.current();
	}

//	public boolean showLoginLink() {
//		return showLoginLink.current();
//	}

}
