package de.unibonn.creedo.boot;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import de.unibonn.creedo.ApplicationRepositories;
import de.unibonn.creedo.ConfigurationProperties;
import de.unibonn.creedo.admin.options.Options;
import de.unibonn.creedo.admin.users.DefaultUserDetails;
import de.unibonn.creedo.admin.users.UserDetails;
import de.unibonn.creedo.admin.users.Users;
import de.unibonn.creedo.studies.StudyBuilder;
import de.unibonn.creedo.studies.designs.EvaluationScheme;
import de.unibonn.creedo.studies.designs.StudyDesignBuilder;
import de.unibonn.creedo.studies.designs.SystemSpecBuilder;
import de.unibonn.creedo.studies.designs.TaskSpecBuilder;
import de.unibonn.creedo.studies.designs.TaskSpecification;
import de.unibonn.creedo.ui.ContentPageBuilder;
import de.unibonn.creedo.ui.ResourceUploadPage;
import de.unibonn.creedo.ui.core.Action;
import de.unibonn.creedo.ui.core.Frame;
import de.unibonn.creedo.ui.core.LoadPageAction;
import de.unibonn.creedo.ui.core.Page;
import de.unibonn.creedo.ui.core.PageContainer;
import de.unibonn.creedo.ui.indexpage.DashboardLinkBuilder;
import de.unibonn.creedo.ui.repository.RepositoryAdministrationPage;
import de.unibonn.creedo.webapp.CreedoSession;
import de.unibonn.creedo.webapp.dashboard.mining.MiningSystemBuilder;
import de.unibonn.realkd.common.RuntimeBuilder;
import de.unibonn.realkd.common.parameter.ParameterContainer;
import de.unibonn.realkd.common.workspace.Workspace;

public class AdminFrameBuilder implements RuntimeBuilder<Frame, CreedoSession> {

	@Override
	public Frame build(final CreedoSession creedoSession) {
		PageContainer adminFramePageContainer = new PageContainer(creedoSession.getUiRegister().getNextId());
		List<Action> adminFrameActions = new ArrayList<>();

		ResourceUploadPage resourceUploadPage = new ResourceUploadPage(creedoSession.getUiRegister().getNextId());
		creedoSession.getUiRegister().registerUiComponent(resourceUploadPage);
		adminFrameActions.add(new LoadPageAction(creedoSession.getUiRegister().getNextId(), resourceUploadPage,
				adminFramePageContainer));

		RepositoryAdministrationPage<ParameterContainer> optionManagementPage = new DefaultRepositoryAdministrationPageBuilder<ParameterContainer>()
				.setRepository(Options.getRepository()).build(creedoSession);
		creedoSession.getUiRegister().registerUiComponent(optionManagementPage);
		adminFrameActions.add(new LoadPageAction(creedoSession.getUiRegister().getNextId(), optionManagementPage,
				adminFramePageContainer));

		RepositoryAdministrationPage<UserDetails> userManagementPage = new DefaultRepositoryAdministrationPageBuilder<UserDetails>()
				.setRepository(Users.detailsRepository())
				.setAdditionClasses(
						Arrays.asList((Class<DefaultUserDetails>) DefaultUserDetails.class))
				.setAdditionTypeAliases(Arrays.asList("User")).build(creedoSession);
		creedoSession.getUiRegister().registerUiComponent(userManagementPage);
		adminFrameActions.add(new LoadPageAction(creedoSession.getUiRegister().getNextId(), userManagementPage,
				adminFramePageContainer));
		
		RepositoryAdministrationPage<Supplier<Workspace>> dataManagementPage = new DefaultRepositoryAdministrationPageBuilder<Supplier<Workspace>>()
				.setRepository(ApplicationRepositories.DATA_REPOSITORY)
				.setAdditionClasses(
						Arrays.asList((Class<FixedComponentWorkspaceBuilder>) FixedComponentWorkspaceBuilder.class))
				.setAdditionTypeAliases(Arrays.asList("Full data workspace")).build(creedoSession);
		creedoSession.getUiRegister().registerUiComponent(dataManagementPage);
		adminFrameActions.add(new LoadPageAction(creedoSession.getUiRegister().getNextId(), dataManagementPage,
				adminFramePageContainer));

		RepositoryAdministrationPage<MiningSystemBuilder<?>> miningSystemManagementPage = new DefaultRepositoryAdministrationPageBuilder<MiningSystemBuilder<?>>()
				.setRepository(ApplicationRepositories.MINING_SYSTEM_REPOSITORY)
				.setAdditionClasses(ConfigurationProperties.get().MINING_SYSTEM_BUILDER_CLASSES)
				.setAdditionTypeAliases(ConfigurationProperties.get().MINING_SYSTEM_BUILDER_CLASSES.stream()
						.map(clazz -> clazz.getSimpleName()).collect(Collectors.toList()))
				.build(creedoSession);
		creedoSession.getUiRegister().registerUiComponent(miningSystemManagementPage);
		adminFrameActions.add(new LoadPageAction(creedoSession.getUiRegister().getNextId(), miningSystemManagementPage,
				adminFramePageContainer));

		RepositoryAdministrationPage<DashboardLinkBuilder> demoManagementPage = new DefaultRepositoryAdministrationPageBuilder<DashboardLinkBuilder>()
				.setRepository(ApplicationRepositories.DEMO_REPOSITORY)
				.setAdditionClasses(Arrays.asList((Class<?>) DashboardLinkBuilder.class))
				.setAdditionTypeAliases(Arrays.asList("Demo link")).build(creedoSession);
		creedoSession.getUiRegister().registerUiComponent(demoManagementPage);
		adminFrameActions.add(new LoadPageAction(creedoSession.getUiRegister().getNextId(), demoManagementPage,
				adminFramePageContainer));

		RepositoryAdministrationPage<StudyDesignBuilder> studyDesignManagementPage = new DefaultRepositoryAdministrationPageBuilder<StudyDesignBuilder>()
				.setRepository(ApplicationRepositories.STUDY_DESIGN_REPOSITORY)
				.setAdditionClasses(Arrays.asList((Class<?>) StudyDesignBuilder.class))
				.setAdditionTypeAliases(Arrays.asList("Study Design")).build(creedoSession);

		creedoSession.getUiRegister().registerUiComponent(studyDesignManagementPage);
		adminFrameActions.add(new LoadPageAction(creedoSession.getUiRegister().getNextId(), studyDesignManagementPage,
				adminFramePageContainer));

		RepositoryAdministrationPage<TaskSpecBuilder> taskSpecManagementPage = new DefaultRepositoryAdministrationPageBuilder<TaskSpecBuilder>()
				.setRepository(ApplicationRepositories.STUDY_TASK_SPECIFICATION_REPOSITORY)
				.setAdditionClasses(Arrays.asList((Class<?>) TaskSpecification.class))
				.setAdditionTypeAliases(Arrays.asList("Task Specification")).build(creedoSession);

		creedoSession.getUiRegister().registerUiComponent(taskSpecManagementPage);
		adminFrameActions.add(new LoadPageAction(creedoSession.getUiRegister().getNextId(), taskSpecManagementPage,
				adminFramePageContainer));

		RepositoryAdministrationPage<EvaluationScheme> evaluationSchemeManagementPage = new DefaultRepositoryAdministrationPageBuilder<EvaluationScheme>()
				.setRepository(ApplicationRepositories.STUDY_EVALUATION_SCHEME_REPOSITORY)
				.setAdditionClasses(Arrays.asList((Class<?>) EvaluationScheme.class))
				.setAdditionTypeAliases(Arrays.asList("Evaluation Scheme")).build(creedoSession);
		creedoSession.getUiRegister().registerUiComponent(evaluationSchemeManagementPage);
		adminFrameActions.add(new LoadPageAction(creedoSession.getUiRegister().getNextId(),
				evaluationSchemeManagementPage, adminFramePageContainer));

		RepositoryAdministrationPage<SystemSpecBuilder> systemSpecManagementPage = new DefaultRepositoryAdministrationPageBuilder<SystemSpecBuilder>()
				.setRepository(ApplicationRepositories.STUDY_SYSTEM_SPECIFICATION_REPOSITORY)
				.setAdditionClasses(Arrays.asList((Class<?>) SystemSpecBuilder.class))
				.setAdditionTypeAliases(Arrays.asList("System Specification")).build(creedoSession);
		creedoSession.getUiRegister().registerUiComponent(systemSpecManagementPage);
		adminFrameActions.add(new LoadPageAction(creedoSession.getUiRegister().getNextId(), systemSpecManagementPage,
				adminFramePageContainer));

		RepositoryAdministrationPage<StudyBuilder> studyManagementPage = new DefaultRepositoryAdministrationPageBuilder<StudyBuilder>()
				.setRepository(ApplicationRepositories.STUDY_REPOSITORY)
				.setAdditionClasses(Arrays.asList((Class<?>) StudyBuilder.class))
				.setAdditionTypeAliases(Arrays.asList("Study")).build(creedoSession);
		creedoSession.getUiRegister().registerUiComponent(studyManagementPage);
		adminFrameActions.add(new LoadPageAction(creedoSession.getUiRegister().getNextId(), studyManagementPage,
				adminFramePageContainer));

		RepositoryAdministrationPage<RuntimeBuilder<Page, CreedoSession>> pageManagementPage = new DefaultRepositoryAdministrationPageBuilder<RuntimeBuilder<Page, CreedoSession>>()
				.setRepository(ApplicationRepositories.PAGE_REPOSITORY)
				.setAdditionClasses(Arrays.asList((Class<?>) ContentPageBuilder.class))
				.setAdditionTypeAliases(Arrays.asList("Content page")).build(creedoSession);
		creedoSession.getUiRegister().registerUiComponent(pageManagementPage);
		adminFrameActions.add(new LoadPageAction(creedoSession.getUiRegister().getNextId(), pageManagementPage,
				adminFramePageContainer));

		adminFrameActions.add(new Action() {

			private int id = creedoSession.getUiRegister().getNextId();

			@Override
			public String getReferenceName() {
				return "Close";
			}

			@Override
			public ResponseEntity<String> activate(String... params) {
				return new ResponseEntity<String>(HttpStatus.OK);
			}

			@Override
			public ClientWindowEffect getEffect() {
				return Action.ClientWindowEffect.CLOSE;
			}

			@Override
			public int getId() {
				return id;
			}
		});

		return creedoSession.getUiRegister().createDefaultFrame(adminFramePageContainer, adminFrameActions,
				new ArrayList<Action>());

	}
}
