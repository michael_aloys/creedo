package de.unibonn.creedo.boot;

import de.unibonn.creedo.admin.options.Configurable;
import de.unibonn.creedo.ui.signup.RequestAccountAction;
import de.unibonn.creedo.ui.signup.RequestAccountPage;
import de.unibonn.creedo.webapp.CreedoSession;
import de.unibonn.realkd.common.RuntimeBuilder;

public class DefaultAccountRequestPageBuilder
		implements Configurable<AccountRequestPageOptions>, RuntimeBuilder<RequestAccountPage, CreedoSession> {

	public DefaultAccountRequestPageBuilder() {
		;
	}

	@Override
	public RequestAccountPage build(CreedoSession context) {
		RequestAccountAction requestAccountAction = new RequestAccountAction(context.getUiRegister().getNextId());
		return new RequestAccountPage(context.getUiRegister().getNextId(), getOptions().note(), requestAccountAction);
	}

	@Override
	public AccountRequestPageOptions getDefaultOptions() {
		return new AccountRequestPageOptions();
	}

	public String getOptionsSerializationId() {
		return "AccountRequestPage";
	}

}
