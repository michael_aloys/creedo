package de.unibonn.creedo.boot;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

import de.unibonn.creedo.ApplicationRepositories;
import de.unibonn.creedo.repositories.RepositoryEntry;
import de.unibonn.creedo.ui.PageBuilder;
import de.unibonn.creedo.ui.core.Action;
import de.unibonn.creedo.ui.core.DefaultPageFrame;
import de.unibonn.creedo.ui.core.LoadPageAction;
import de.unibonn.creedo.ui.core.Page;
import de.unibonn.creedo.ui.core.PageContainer;
import de.unibonn.creedo.ui.core.PageFrame;
import de.unibonn.creedo.webapp.CreedoSession;
import de.unibonn.realkd.common.RuntimeBuilder;
import de.unibonn.realkd.common.parameter.DefaultSubCollectionParameter;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.ParameterContainer;
import de.unibonn.realkd.common.parameter.SubCollectionParameter;

/**
 * <p>
 * Creates and configures a frame that can display pages.
 * </p>
 * 
 * <p>
 * WARNING: As of now this class is referenced directly in a db-based
 * repository. So make sure to update init scripts when moving or renaming this
 * class as well when you change its declared parameters.
 * </p>
 * 
 * @author Mario Boley
 * 
 * @since 0.1.0
 * 
 * @version 0.4.0
 * 
 */
public class DefaultPageFrameBuilder implements RuntimeBuilder<PageFrame, CreedoSession>, ParameterContainer {

	private final SubCollectionParameter<String, List<String>> optionalNavbarContents;

	private final SubCollectionParameter<String, List<String>> optionalFooterContents;

	private final List<Parameter<?>> parameters;

	public DefaultPageFrameBuilder() {
		DefaultSubCollectionParameter.CollectionComputer<List<String>> allPages = () -> ApplicationRepositories.PAGE_REPOSITORY
				.getAllIds();
		this.optionalNavbarContents = DefaultSubCollectionParameter.getDefaultSubListParameter("Navbar entries",
				"List of optional content to be linked from the navbar of frame.", allPages);
		this.optionalFooterContents = DefaultSubCollectionParameter.getDefaultSubListParameter("Footer entries",
				"List of optional content to be linked from the footer of frame.", allPages);
		this.parameters = ImmutableList.of(optionalNavbarContents, optionalFooterContents);
	}

	@Override
	public PageFrame build(final CreedoSession creedoSession) {
		PageContainer pageContainer = new PageContainer(creedoSession.getUiRegister().getNextId());

		List<Action> navbarActions = new ArrayList<>();
		List<Action> footerActions = new ArrayList<>();

		for (String contentId : optionalNavbarContents.current()) {
			RepositoryEntry<String, RuntimeBuilder<Page, CreedoSession>> entry = ApplicationRepositories.PAGE_REPOSITORY
					.getEntry(contentId);
			PageBuilder pageBuilder = (PageBuilder) entry.getContent();
			Page page = pageBuilder.build(creedoSession);

			navbarActions.add(new LoadPageAction(creedoSession.getUiRegister().getNextId(), page, pageContainer));

		}
		for (String contentId : optionalFooterContents.current()) {
			RepositoryEntry<String, RuntimeBuilder<Page, CreedoSession>> entry = ApplicationRepositories.PAGE_REPOSITORY
					.getEntry(contentId);
			PageBuilder pageBuilder = (PageBuilder) entry.getContent();
			Page page = pageBuilder.build(creedoSession);
			footerActions.add(new LoadPageAction(creedoSession.getUiRegister().getNextId(), page, pageContainer));
		}

		navbarActions.add(new Action() {

			private final int id = creedoSession.getUiRegister().getNextId();

			@Override
			public String getReferenceName() {
				return "Close";
			}

			@Override
			public ResponseEntity<String> activate(String... params) {
				return new ResponseEntity<String>(HttpStatus.OK);
			}

			@Override
			public ClientWindowEffect getEffect() {
				return Action.ClientWindowEffect.CLOSE;
			}

			@Override
			public int getId() {
				return id;
			}
		});

		DefaultPageFrame result = creedoSession.getUiRegister().createDefaultFrame(pageContainer,
				Lists.newArrayList(navbarActions), Lists.newArrayList(footerActions));

		// if ()
		// result.performAction(loadIndexPageActionId);

		return result;
	}

	@Override
	public List<Parameter<?>> getTopLevelParameters() {
		return parameters;
	}
	
	public DefaultPageFrameBuilder optionalNavbarContent(List<String> pageIds) {
		optionalNavbarContents.set(pageIds);
		return this;
	}
	
	public DefaultPageFrameBuilder optionalFooterContent(List<String> pageIds) {
		optionalFooterContents.set(pageIds);
		return this;
	}

}
