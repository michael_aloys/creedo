package de.unibonn.creedo.boot;

import static de.unibonn.creedo.DynamicUrlController.frameUrl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import de.unibonn.creedo.ApplicationRepositories;
import de.unibonn.creedo.admin.options.Configurable;
import de.unibonn.creedo.admin.users.DefaultUserGroup;
import de.unibonn.creedo.admin.users.Users;
import de.unibonn.creedo.repositories.RepositoryEntry;
import de.unibonn.creedo.ui.PageBuilder;
import de.unibonn.creedo.ui.core.Action;
import de.unibonn.creedo.ui.core.DefaultPageFrame;
import de.unibonn.creedo.ui.core.Frame;
import de.unibonn.creedo.ui.core.LoadPageAction;
import de.unibonn.creedo.ui.core.Page;
import de.unibonn.creedo.ui.core.PageContainer;
import de.unibonn.creedo.ui.core.PageFrame;
import de.unibonn.creedo.ui.indexpage.DefaultIndexPageBuilder;
import de.unibonn.creedo.ui.signup.DefaultLoginPage;
import de.unibonn.creedo.ui.signup.LoginAction;
import de.unibonn.creedo.ui.signup.RegistrationRequestConfirmationPage;
import de.unibonn.creedo.ui.signup.RequestAccountPage;
import de.unibonn.creedo.webapp.CreedoSession;
import de.unibonn.realkd.common.RuntimeBuilder;

/**
 * <p>
 * Creates and configures a frame object with all features necessary for the
 * Creedo mainframe.
 * </p>
 * 
 * <p>
 * WARNING: As of now this class is referenced directly in a db-based
 * repository. So make sure to update init scripts when moving or renaming this
 * class as well when you change its declared parameters.
 * </p>
 * 
 * @author Mario Boley
 * 
 * @since 0.1.0
 * 
 * @version 0.4.0
 *
 */
public class DefaultMainFrameBuilder
		implements Configurable<MainFrameOptions>, RuntimeBuilder<PageFrame, CreedoSession> {

	public DefaultMainFrameBuilder() {
		;
	}

	public PageFrame build(final CreedoSession creedoSession) {
		PageContainer pageContainer = new PageContainer(creedoSession.getUiRegister().getNextId());

		RequestAccountPage requestAccountPage = new DefaultAccountRequestPageBuilder().build(creedoSession);

		creedoSession.getUiRegister().registerUiComponent(requestAccountPage);
		Action loadAccountRequestPage = new LoadPageAction(creedoSession.getUiRegister().getNextId(),
				requestAccountPage, pageContainer);

		Page accountRequestConfirmationPage = new RegistrationRequestConfirmationPage();
		requestAccountPage.addRegistrationSuccessAction(new LoadPageAction(creedoSession.getUiRegister().getNextId(),
				accountRequestConfirmationPage, pageContainer));

		Action loginAction = new LoginAction(creedoSession.getUiRegister().getNextId(), creedoSession);

		DefaultLoginPage loginPage = new DefaultLoginPage(creedoSession.getUiRegister().getNextId(), loginAction,
				loadAccountRequestPage);
		creedoSession.getUiRegister().registerUiComponent(loginPage);

		Page indexPage = new DefaultIndexPageBuilder().build(creedoSession);

		List<Action> navbarActions = new ArrayList<>();
		List<Action> footerActions = new ArrayList<>();
		// String loadIndexPageActionId = "loadIndexPage";
		navbarActions.add(new LoadPageAction(creedoSession.getUiRegister().getNextId(), indexPage, pageContainer));

		if (creedoSession.getUser().groups().contains(DefaultUserGroup.ADMINISTRATOR)) {

			navbarActions.add(new Action() {

				private final int id = creedoSession.getUiRegister().getNextId();

				@Override
				public String getReferenceName() {
					return "Administration";
				}

				@Override
				public ResponseEntity<String> activate(String... params) {
					Frame adminFrame = new AdminFrameBuilder().build(creedoSession);
					return new ResponseEntity<String>(frameUrl(adminFrame.getId()), HttpStatus.OK);
				}

				@Override
				public ClientWindowEffect getEffect() {
					return ClientWindowEffect.POPUP;
				}

				@Override
				public int getId() {
					return id;
				}
			});
		}

		if (creedoSession.getUser().groups().contains(DefaultUserGroup.DEVELOPER)) {
			navbarActions.add(new LoadPageAction(creedoSession.getUiRegister().getNextId(),
					creedoSession.getCustomDashboardCreationPage(), pageContainer));
		}

		for (String contentId : getOptions().optionalNavbarContents()) {
			RepositoryEntry<String, RuntimeBuilder<Page, CreedoSession>> entry = ApplicationRepositories.PAGE_REPOSITORY
					.getEntry(contentId);
			PageBuilder pageBuilder = (PageBuilder) entry.getContent();
			Page page = pageBuilder.build(creedoSession);

			navbarActions.add(new LoadPageAction(creedoSession.getUiRegister().getNextId(), page, pageContainer));

		}
		for (String contentId : getOptions().optionalFooterContents()) {
			RepositoryEntry<String, RuntimeBuilder<Page, CreedoSession>> entry = ApplicationRepositories.PAGE_REPOSITORY
					.getEntry(contentId);
			PageBuilder pageBuilder = (PageBuilder) entry.getContent();
			Page page = pageBuilder.build(creedoSession);
			footerActions.add(new LoadPageAction(creedoSession.getUiRegister().getNextId(), page, pageContainer));
		}

		if (creedoSession.getUser() == Users.DEFAULT_USER) {
			navbarActions.add(new LoadPageAction(creedoSession.getLoadLoginPageActionId(), loginPage, pageContainer));
		} else if (creedoSession.getUser() != Users.DEFAULT_USER) {
			Action logOutAction = new Action() {

				private final int id = creedoSession.getUiRegister().getNextId();

				@Override
				public String getReferenceName() {
					return "Logout";
				}

				@Override
				public ResponseEntity<String> activate(String... params) {
					creedoSession.getUiRegister().tearDownDashboard();
					creedoSession.getHttpSession().invalidate();
					return new ResponseEntity<String>(HttpStatus.OK);
				}

				@Override
				public ClientWindowEffect getEffect() {
					return ClientWindowEffect.REFRESH;
				}

				@Override
				public int getId() {
					return id;
				}

			};
			navbarActions.add(logOutAction);
		}
		DefaultPageFrame result = creedoSession.getUiRegister().createDefaultFrame(pageContainer, navbarActions,
				footerActions);

		return result;
	}

	@Override
	public MainFrameOptions getDefaultOptions() {
		return new MainFrameOptions();
	}

	@Override
	public String getOptionsSerializationId() {
		return "MainFrame";
	}

}
