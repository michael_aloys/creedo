package de.unibonn.creedo.boot;

import static de.unibonn.realkd.common.parameter.Parameters.stringParameter;

import java.util.List;
import java.util.Optional;

import com.google.common.collect.ImmutableList;

import de.unibonn.creedo.ApplicationRepositories;
import de.unibonn.creedo.repositories.IdentifierInRepositoryParameter;
import de.unibonn.creedo.repositories.Repositories;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.ParameterContainer;
import de.unibonn.realkd.common.parameter.ValueValidator;

public class IndexPageOptions implements ParameterContainer {

	private final Parameter<String> title;
	private final Parameter<String> referenceName;
	private final Parameter<Optional<String>> contentPageFileName;
	private final Parameter<String> loginInviteParameter;

	private final List<Parameter<?>> parameters;

	public IndexPageOptions() {
		title = stringParameter("Title", "The title of the index page, e.g., \"Home\"", "Home",
				ValueValidator.ALWAYS_VALID_VALIDATOR, "");

		referenceName = stringParameter("Reference name",
				"A short name for referencing the index page, e.g., from the navbar.", "Home",
				ValueValidator.ALWAYS_VALID_VALIDATOR, "");

		contentPageFileName = IdentifierInRepositoryParameter.optionalRepositoryIdParameter("Content file name",
				"The name of the html file containing the main content of the index page.",
				ApplicationRepositories.CONTENT_FOLDER_REPOSITORY,
				Repositories.getIdIsFilenameWithExtensionPredicate(Repositories.HTML_FILE_EXTENSIONS));

		loginInviteParameter = stringParameter("Login invitation",
				"An invitation text that is diplayed below the maincontent to users who are not logged into the application with their accoint (should contain a link to open the login page).",
				"",
				ValueValidator.VALUE_NOT_NULL_VALIDATOR, "");

		parameters = ImmutableList.of(title, referenceName, contentPageFileName, loginInviteParameter);
	}

	@Override
	public List<Parameter<?>> getTopLevelParameters() {
		return parameters;
	}
	
	public String title() {
		return title.current();
	}
	
	public String referenceName() {
		return referenceName.current();
	}
	
	public Optional<String> contentPageFilename() {
		return contentPageFileName.current();
	}
	
	public String loginInvite() {
		return loginInviteParameter.current();
	}
	
}
