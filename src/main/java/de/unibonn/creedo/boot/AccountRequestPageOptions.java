package de.unibonn.creedo.boot;

import static de.unibonn.realkd.common.parameter.Parameters.stringParameter;

import java.util.List;

import com.google.common.collect.ImmutableList;

import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.ParameterContainer;
import de.unibonn.realkd.common.parameter.ValueValidator;

public class AccountRequestPageOptions implements ParameterContainer {

	private Parameter<String> noteParameter;
	
	private List<Parameter<?>> parameters;

	public AccountRequestPageOptions() {
		this.noteParameter = stringParameter("Account request note",
				"A note that is displayed by page together with the sign up form", "",
				ValueValidator.VALUE_NOT_NULL_VALIDATOR, "");
		this.parameters=ImmutableList.of(noteParameter);
	}
	
	@Override
	public List<Parameter<?>> getTopLevelParameters() {
		return parameters;
	}
	
	public String note() {
		return noteParameter.current();
	}

}
