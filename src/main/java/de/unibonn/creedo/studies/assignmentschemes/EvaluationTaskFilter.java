package de.unibonn.creedo.studies.assignmentschemes;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import de.unibonn.creedo.admin.users.CreedoUser;
import de.unibonn.creedo.studies.designs.TaskSpecification;
import de.unibonn.creedo.studies.state.StudyState;
import de.unibonn.creedo.studies.state.TrialSession;

/**
 * Filter that can be applied during evaluation assignment generation in order
 * to determine valid tasks, for which an evaluator is allowed to evaluate
 * results.
 * 
 * @author Mario Boley
 * 
 * @since 0.1.2
 * 
 * @verison 0.1.2
 *
 */
public enum EvaluationTaskFilter {

	ALL_TASKS {
		@Override
		public List<TaskSpecification> filter(List<TaskSpecification> tasks,
				CreedoUser user, StudyState studyState) {
			return tasks;
		}
	},

	/**
	 * Leaves only tasks the evaluator did not work on herself in this study.
	 */
	UNKNOWN_TASKS {
		@Override
		public List<TaskSpecification> filter(List<TaskSpecification> tasks,
				CreedoUser user, StudyState studyState) {
			List<TaskSpecification> result = new ArrayList<>(tasks);
			Collection<TrialSession> sessionsOfUser = studyState
					.getTrialSessionRepository().getAll(
							studyState.getTrialSessionOfUserPredicate(user));
			LOGGER.log(Level.FINE, sessionsOfUser.size()
					+ " trial sessions found for user '" + user.id()
					+ "'");

			sessionsOfUser.forEach((s) -> {
				result.remove(s.getTask());
			});
			return result;
		}
	}

	;

	public abstract List<TaskSpecification> filter(
			List<TaskSpecification> tasks, CreedoUser user, StudyState studyState);

	private static final Logger LOGGER = Logger
			.getLogger(EvaluationTaskFilter.class.getName());

}
