package de.unibonn.creedo.studies.assignmentschemes;

import static de.unibonn.realkd.common.parameter.Parameters.integerParameter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.function.Supplier;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import com.google.common.collect.ImmutableList;

import de.unibonn.creedo.admin.users.CreedoUser;
import de.unibonn.creedo.repositories.RepositoryEntry;
import de.unibonn.creedo.studies.RatingDAO;
import de.unibonn.creedo.studies.StudyUserRole;
import de.unibonn.creedo.studies.designs.SystemSpecification;
import de.unibonn.creedo.studies.designs.TaskSpecification;
import de.unibonn.creedo.studies.state.StudyState;
import de.unibonn.creedo.studies.state.TrialSession;
import de.unibonn.creedo.webapp.studies.Result;
import de.unibonn.realkd.common.parameter.DefaultRangeEnumerableParameter;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.ParameterContainer;
import de.unibonn.realkd.common.parameter.Parameters;
import de.unibonn.realkd.common.parameter.ValueValidator;

/**
 * Assignment scheme that supports two phases, in the first of which all trial
 * assignments are issued, and in the second of which all evaluations
 * assignments are issued.
 * 
 * @author Bo Kang
 * 
 * @since 0.1.0
 * 
 * @version 0.1.2.1
 *
 */
public class TwoPhaseAssignment implements AssignmentScheme, ParameterContainer {

	private static final Logger LOGGER = Logger
			.getLogger(TwoPhaseAssignment.class.getName());

	private class SetupPhase implements AssignmentPhase {

		@Override
		public ImmutableList<Assignment> getAssignments(StudyState studyState,
				CreedoUser user, StudyUserRole role, String studyName,
				List<SystemSpecification> sysSpecs,
				List<TaskSpecification> taskSpecs) {
			return ImmutableList.of();
		}

		@Override
		public String toString() {
			return "SETUP";
		}

	}

	private class TrialPhase implements AssignmentPhase {

		@Override
		public ImmutableList<Assignment> getAssignments(
				final StudyState studyState, CreedoUser user, StudyUserRole role,
				String studyName, List<SystemSpecification> systemSpecs,
				List<TaskSpecification> taskSpecs) {

			if (role != StudyUserRole.PARTICIPANT) {
				return ImmutableList.of();
			}

			// 1. check whether this user has participated.
			Supplier<Boolean> isValid = new Supplier<Boolean>() {
				@Override
				public Boolean get() {
					Collection<TrialSession> sessionsOfUser = studyState
							.getTrialSessionRepository()
							.getAll(studyState
									.getTrialSessionOfUserPredicate(user));
					return sessionsOfUser.isEmpty();
				}
			};

			// if (!sessionsOfUser.isEmpty()) {
			if (!isValid.get()) {
				LOGGER.log(Level.INFO,
						"User " + user.id() + " already participated");
				return ImmutableList.of();
			}

			// 2. list all the trials (task - system combinations) and choose
			// one with minimal number of sessions so far
			// TODO shouldn't this be an illegal state?
			if (systemSpecs.size() == 0 || taskSpecs.size() == 0) {
				return ImmutableList.of();
			}

			List<SystemSpecification> systemPartOfMinSessionComb = new ArrayList<>();
			List<TaskSpecification> taskPartOfMinSessionComb = new ArrayList<>();
			int minNumberOfSessions = Integer.MAX_VALUE;

			for (SystemSpecification system : systemSpecs) {
				for (TaskSpecification task : taskSpecs) {

					Collection<TrialSession> sessionsOfSystemTaskComb = studyState
							.getTrialSessionRepository()
							.getAll(studyState
									.getTrialSessionOfSystemPredicate(system)
									.and(studyState
											.getTrialSessionOfTaskPredicate(task)));

					if (systemPartOfMinSessionComb.isEmpty()) {
						systemPartOfMinSessionComb.add(system);
						taskPartOfMinSessionComb.add(task);
						minNumberOfSessions = sessionsOfSystemTaskComb.size();
					} else if (sessionsOfSystemTaskComb.size() == minNumberOfSessions) {
						systemPartOfMinSessionComb.add(system);
						taskPartOfMinSessionComb.add(task);
					} else if (sessionsOfSystemTaskComb.size() < minNumberOfSessions) {
						systemPartOfMinSessionComb.clear();
						taskPartOfMinSessionComb.clear();
						systemPartOfMinSessionComb.add(system);
						taskPartOfMinSessionComb.add(task);
						minNumberOfSessions = sessionsOfSystemTaskComb.size();
					}
				}
			}

			LOGGER.log(Level.FINE,
					"System/Task combination with the smallest number of session ("
							+ minNumberOfSessions + "):");
			for (int i = 0; i < systemPartOfMinSessionComb.size(); i++) {
				LOGGER.log(Level.FINE, systemPartOfMinSessionComb.get(i)
						.getName()
						+ "/"
						+ taskPartOfMinSessionComb.get(i).getName());
			}

			int selectedIndex = new Random().nextInt(systemPartOfMinSessionComb
					.size());

			return ImmutableList.of(new TrialAssignment(studyName, user,
					taskPartOfMinSessionComb.get(selectedIndex),
					systemPartOfMinSessionComb.get(selectedIndex), isValid));

			// return ImmutableList.of(getMiningDashboardLink(studyState,
			// taskPartOfMinSessionComb.get(selectedIndex),
			// systemPartOfMinSessionComb.get(selectedIndex), studyName,
			// studyDescr, imageUrl, imageCredits, user));
		}

		@Override
		public String toString() {
			return "TRIAL";
		}

	}

	private class EvaluationPhase implements AssignmentPhase {

		@Override
		public ImmutableList<Assignment> getAssignments(StudyState studyState,
				CreedoUser user, StudyUserRole role, String studyName,
				List<SystemSpecification> sysSpecs,
				List<TaskSpecification> taskSpecs) {

			if (role != StudyUserRole.EVALUATOR) {
				return ImmutableList.of();
			}

			// 1. check whether this user has participated any evaluation.
			Supplier<Boolean> isValid = new Supplier<Boolean>() {
				@Override
				public Boolean get() {
					List<Integer> performedRatingIds = RatingDAO.INSTANCE
							.getRatingIdsGivenByUserInStudy(user.id(),
									studyName);
					return performedRatingIds.size() == 0;
				}
			};

			if (!isValid.get()) {
				LOGGER.info("User participated already as evaluator in "
						+ studyName);
				return ImmutableList.of();
			}

			// 2. decide the task to be evaluated by current user based on
			// selected filter.
			List<TaskSpecification> tasksAvailableForEvaluation = evaluationTaskFilter
					.current().filter(taskSpecs, user, studyState);
			LOGGER.log(Level.FINE, "Tasks available for review are '"
					+ tasksAvailableForEvaluation + "'");

			if (tasksAvailableForEvaluation.size() == 0) {
				return ImmutableList.of();
			}
			Random rand = new Random();
			TaskSpecification selectedTask = tasksAvailableForEvaluation
					.get(rand.nextInt(tasksAvailableForEvaluation.size()));

			Collection<RepositoryEntry<Integer, Result>> resultsOfSelectedTask = studyState
					.getResultRepository().getAllEntries(
							studyState.getResultOfTaskPredicate(selectedTask));
			LOGGER.log(Level.FINE, resultsOfSelectedTask.size()
					+ " results available for review.");
			LOGGER.log(Level.FINE, "Ids: "
					+ resultsOfSelectedTask.stream().map(e -> e.getId())
							.collect(Collectors.toList()));

			// 3. check where there are at least NUM_RESULTS_FOR_EVALUATION
			// results submitted.
			int numberOfResultsForEvaluation = numberOfResultsToEvaluateParameter
					.current();
			if (resultsOfSelectedTask.size() < numberOfResultsForEvaluation) {
				return ImmutableList.of();
			}

			/*
			 * 4. Uniformly select NUM_RESULTS_FOR_EVALUATION from the available
			 * results.
			 */
			List<RepositoryEntry<Integer, Result>> resultsTaskAsList = new ArrayList<>(
					resultsOfSelectedTask);
			Collections.shuffle(resultsTaskAsList);
			List<Integer> resultIdsToEvaluate = resultsTaskAsList
					.subList(0, numberOfResultsForEvaluation).stream()
					.map(e -> e.getId()).collect(Collectors.toList());

			return ImmutableList.of(new EvaluationAssignment(studyName, user,
					selectedTask, resultIdsToEvaluate, isValid));

		}

		public String toString() {
			return "EVALUATION";
		}

	}

	private class ConclusionPhase implements AssignmentPhase {

		@Override
		public ImmutableList<Assignment> getAssignments(StudyState studyState,
				CreedoUser user, StudyUserRole role, String studyName,
				List<SystemSpecification> sysSpecs,
				List<TaskSpecification> taskSpecs) {
			return ImmutableList.of();
		}

		@Override
		public String toString() {
			return "CONCLUSION";
		}

	}

	private final ImmutableList<AssignmentPhase> phases;

	private final Parameter<Integer> numberOfResultsToEvaluateParameter;

	private final Parameter<EvaluationTaskFilter> evaluationTaskFilter;

	private final ImmutableList<Parameter<?>> parameters;

	public TwoPhaseAssignment() {
		this.evaluationTaskFilter = Parameters.rangeEnumerableParameter("Evaluation task filter", "A filter for the possible tasks, for which results are evaluated by an evaluator.", EvaluationTaskFilter.class, new DefaultRangeEnumerableParameter.RangeComputer<EvaluationTaskFilter>() {
			@Override
			public List<EvaluationTaskFilter> computeRange() {
				return ImmutableList.copyOf(EvaluationTaskFilter
						.values());
			}
		});

		this.numberOfResultsToEvaluateParameter = integerParameter(
				"Results per evaluator",
				"The number of results that every evaluator has to evaluate.",
				1, new ValueValidator.LargerThanThresholdValidator<Integer>(
						new Integer(0)), "Choose integer larger than 0.");
		this.parameters = ImmutableList.of(evaluationTaskFilter,
				numberOfResultsToEvaluateParameter);

		this.phases = ImmutableList.of(new SetupPhase(), new TrialPhase(),
				new EvaluationPhase(), new ConclusionPhase());
	}

	@Override
	public String toString() {
		return "TWO_PHASE_ASSIGNMENT";
	}

	@Override
	public List<Parameter<?>> getTopLevelParameters() {
		return parameters;
	}

	@Override
	public ImmutableList<AssignmentPhase> getAssignmentPhases() {
		return phases;
	}

}
