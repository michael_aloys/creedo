package de.unibonn.creedo.studies.assignmentschemes;

import java.util.function.Supplier;

import de.unibonn.creedo.admin.users.CreedoUser;
import de.unibonn.creedo.studies.designs.SystemSpecification;
import de.unibonn.creedo.studies.designs.TaskSpecification;

/**
 * Assignment to solve a specific study task with a specific study system
 * variant.
 * 
 * @author Mario Boley
 * 
 * @since 0.1.2
 * 
 * @version 0.1.2.1
 *
 */
public final class TrialAssignment extends Assignment {

	private final SystemSpecification system;

	public TrialAssignment(String studyName, CreedoUser user, TaskSpecification task,
			SystemSpecification system, Supplier<Boolean> isValid) {
		super(studyName, user, task, isValid);
		this.system = system;
	}

	public SystemSpecification getSystem() {
		return system;
	}

}
