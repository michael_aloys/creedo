package de.unibonn.creedo.studies.assignmentschemes;

import java.util.List;

import com.google.common.collect.ImmutableList;

import de.unibonn.creedo.admin.users.CreedoUser;
import de.unibonn.creedo.studies.StudyUserRole;
import de.unibonn.creedo.studies.designs.SystemSpecification;
import de.unibonn.creedo.studies.designs.TaskSpecification;
import de.unibonn.creedo.studies.state.StudyState;

/**
 * During a study process, a study goes through different assignment phases,
 * during each of which different assignments have to be fulfilled by users
 * acting in specific roles.
 * 
 * @author Mario Boley
 * 
 * @since 0.1.2
 * 
 * @version 0.1.2.1
 *
 */
public interface AssignmentPhase {

	public ImmutableList<Assignment> getAssignments(StudyState studyState,
			CreedoUser user, StudyUserRole role, String studyName,
			List<SystemSpecification> sysSpecs,
			List<TaskSpecification> taskSpecs);

}
