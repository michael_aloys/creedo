package de.unibonn.creedo.studies.designs;

import static de.unibonn.realkd.common.parameter.Parameters.stringParameter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import de.unibonn.creedo.ApplicationRepositories;
import de.unibonn.creedo.repositories.Repositories;
import de.unibonn.creedo.studies.assignmentschemes.AssignmentScheme;
import de.unibonn.creedo.studies.assignmentschemes.TwoPhaseAssignment;
import de.unibonn.realkd.common.parameter.DefaultParameterContainer;
import de.unibonn.realkd.common.parameter.DefaultRangeEnumerableParameter.RangeComputer;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.ParameterContainer;
import de.unibonn.realkd.common.parameter.Parameters;
import de.unibonn.realkd.common.parameter.RangeEnumerableParameter;
import de.unibonn.realkd.common.parameter.ValueValidator;

public class StudyDesignBuilder implements ParameterContainer {

	private final AssignmentScheme twoPhaseOption = new TwoPhaseAssignment();

	private final Parameter<String> nameParameter;

	private final Parameter<String> descriptionParameter;

	private final Parameter<Set<String>> systemSpecParameter;

	private final Parameter<Set<String>> taskSpecParameter;

	private final RangeEnumerableParameter<AssignmentScheme> assignmentSchemeParameter;

	private final DefaultParameterContainer defaultParameterContainer;

	public StudyDesignBuilder() {
		nameParameter = stringParameter("Name",
				"The name of this study design.", "",
				ValueValidator.VALUE_NOT_NULL_VALIDATOR, "");

		descriptionParameter = stringParameter("Description",
				"A descripton that explains the study desgin.", "",
				ValueValidator.VALUE_NOT_NULL_VALIDATOR, "");

		systemSpecParameter = Repositories
				.getIdentifierSetOfRepositoryParameter(
						"System Specifications",
						"Select a set of system specifications that specify the systems will be evaluated in study.",
						ApplicationRepositories.STUDY_SYSTEM_SPECIFICATION_REPOSITORY);

		taskSpecParameter = Repositories
				.getIdentifierSetOfRepositoryParameter(
						"Task Specifications",
						"Select a set of task specifications which specify the trial/evaluation tasks that study participants/evaluators need to perform.",
						ApplicationRepositories.STUDY_TASK_SPECIFICATION_REPOSITORY);

		assignmentSchemeParameter = Parameters.rangeEnumerableParameter("Assignment Scheme", "Select an assignment scheme that assigns trial/evaluation tasks to study participants/evaluators ", AssignmentScheme.class, new RangeComputer<AssignmentScheme>() {
			@Override
			public List<AssignmentScheme> computeRange() {
				return Arrays.asList(twoPhaseOption);
			}
		});

		defaultParameterContainer = new DefaultParameterContainer();

		defaultParameterContainer.addAllParameters(Arrays.asList(
				(Parameter<?>) nameParameter, descriptionParameter,
				systemSpecParameter, taskSpecParameter,
				assignmentSchemeParameter));
	}

	public StudyDesign build() {

		String name = nameParameter.current();

		String description = descriptionParameter.current();

		List<SystemSpecification> systemSpecifications = new ArrayList<>();
		for (String systemSpecificationId : systemSpecParameter
				.current()) {
			systemSpecifications
					.add(ApplicationRepositories.STUDY_SYSTEM_SPECIFICATION_REPOSITORY
							.getEntry(systemSpecificationId).getContent()
							.build());
		}

		List<TaskSpecification> taskSpecifications = new ArrayList<>();
		for (String taskSpecificationId : taskSpecParameter.current()) {
			taskSpecifications
					.add(ApplicationRepositories.STUDY_TASK_SPECIFICATION_REPOSITORY
							.getEntry(taskSpecificationId).getContent().build());
		}

		return new StudyDesign(name, description, systemSpecifications,
				taskSpecifications, new SystemPerformanceMetric(),
				// AssignmentSchemeImplementations
				// .valueOf(assignmentSchemeParameter.getCurrentValue())
				//
				assignmentSchemeParameter.current());
	}

	@Override
	public List<Parameter<?>> getTopLevelParameters() {
		return defaultParameterContainer.getTopLevelParameters();
	}

}
