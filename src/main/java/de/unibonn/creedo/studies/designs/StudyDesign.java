package de.unibonn.creedo.studies.designs;

import java.util.List;

import de.unibonn.creedo.studies.assignmentschemes.AssignmentScheme;

public class StudyDesign {
	private final String name;
	private final String description;
	private final List<SystemSpecification> systemSpecifications;
	private final List<TaskSpecification> taskSpecifications;
	private final SystemPerformanceMetric systemPerformanceMetric;
	private final AssignmentScheme assignmentScheme;

	public StudyDesign(String name, String description, List<SystemSpecification> systemSpecifications,
			List<TaskSpecification> taskSpecifications,
			SystemPerformanceMetric systemPerformanceMetric,
			AssignmentScheme assignmentScheme) {
		this.name = name;
		this.description = description;
		this.systemSpecifications = systemSpecifications;
		this.taskSpecifications = taskSpecifications;
		this.systemPerformanceMetric = systemPerformanceMetric;
		this.assignmentScheme = assignmentScheme;
	}
	
	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public List<SystemSpecification> getSystemSpecifications() {
		return systemSpecifications;
	}

	public List<TaskSpecification> getTaskSpecifications() {
		return taskSpecifications;
	}

	public SystemPerformanceMetric getSystemPerformanceMetric() {
		return systemPerformanceMetric;
	}

	public AssignmentScheme getAssignmentScheme() {
		return assignmentScheme;
	}

}
