package de.unibonn.creedo.studies.designs;

import java.util.List;

import de.unibonn.creedo.webapp.studies.result.ResultDefinition;

/**
 * 
 * @author Bo Kang
 * 
 * @since 0.1.0
 * 
 * @version 0.1.0.1
 *
 */
public class TaskSpecification {

	private final String name;

	private final String description;

	private final List<String> instructions;

	private final ResultDefinition resultDefinition;

	private final String dataIdentifier;

	private final EvaluationScheme evaluationScheme;

	public TaskSpecification(String name, String description,
			List<String> instructions, ResultDefinition resultDefinition,
			String dataIdentifier, EvaluationScheme evaluationScheme) {
		this.name = name;
		this.description = description;
		this.instructions = instructions;
		this.resultDefinition = resultDefinition;
		this.dataIdentifier = dataIdentifier;
		this.evaluationScheme = evaluationScheme;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	/**
	 * Provides a list of instruction pages of the task, each of which is given
	 * in the form of html.
	 * 
	 * @return sequence of instructions in html
	 * 
	 */
	public List<String> getInstructions() {
		return instructions;
	}

	public ResultDefinition getResultDefinition() {
		return resultDefinition;
	}

	public String getDataIdentifier() {
		return dataIdentifier;
	}

	public EvaluationScheme getEvaluationScheme() {
		return evaluationScheme;
	}

}
