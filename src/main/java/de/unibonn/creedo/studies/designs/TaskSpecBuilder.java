package de.unibonn.creedo.studies.designs;

import static de.unibonn.realkd.common.parameter.Parameters.stringParameter;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

import com.google.common.collect.ImmutableList;

import de.unibonn.creedo.ApplicationRepositories;
import de.unibonn.creedo.repositories.IdentifierInRepositoryParameter;
import de.unibonn.creedo.repositories.Repositories;
import de.unibonn.creedo.setup.ServerPaths;
import de.unibonn.creedo.webapp.studies.result.PatternSetResultDefinition;
import de.unibonn.creedo.webapp.studies.result.ResultDefinition;
import de.unibonn.creedo.webapp.studies.result.SinglePatternResultDefinition;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.ParameterContainer;
import de.unibonn.realkd.common.parameter.Parameters;
import de.unibonn.realkd.common.parameter.RangeEnumerableParameter;
import de.unibonn.realkd.common.parameter.SubCollectionParameter;
import de.unibonn.realkd.common.parameter.ValueValidator;
import de.unibonn.realkd.common.workspace.Workspace;

/**
 * Allows to specify a {@link TaskSpecification} based on the content of various
 * repositories.
 * 
 * @author Bo Kang
 * 
 * @since 0.1.0
 * 
 * @version 0.1.2.1
 *
 */
public class TaskSpecBuilder implements ParameterContainer {

	private final Parameter<String> nameParameter;

	private final Parameter<String> descriptionParameter;

	private final Parameter<String> inputDataParameter;

	private final SubCollectionParameter<String, List<String>> instructionParameter;

	private final Parameter<ResultDefinition> resultDefinitionParameter;

	private final RangeEnumerableParameter<String> evaluationSchemeParameter;

	private final ImmutableList<Parameter<?>> parameters;

	public TaskSpecBuilder() {
		nameParameter = stringParameter("Name",
				"The name of this task specification.", "",
				ValueValidator.VALUE_NOT_NULL_VALIDATOR, "");

		descriptionParameter = stringParameter("Description",
				"A descripton that explains the task specification.", "",
				ValueValidator.VALUE_NOT_NULL_VALIDATOR, "");

		instructionParameter = Repositories
				.getIdentifierListOfRepositoryParameter(
						"Instructions",
						"Select instruction pages that introduce the trial task to participants. Instruction will be presented in the same order as in this list.",
						ApplicationRepositories.CONTENT_FOLDER_REPOSITORY,
						Repositories
								.getIdIsFilenameWithExtensionPredicate(Repositories.HTML_FILE_EXTENSIONS));

		// oldResultDefParameter = new DefaultParameter<Integer>(
		// "Result Definition",
		// "Number of patterns that a participant needs to discovery in order to successfully submit her result.",
		// Integer.class, 0, StringParser.INTEGER_PARSER,
		// new ValueValidator<Integer>() {
		// @Override
		// public boolean valid(Integer value) {
		// return value >= 0;
		// }
		// }, "Number of patterns in result.");

		final SinglePatternResultDefinition singlePatternResultDefinition = new SinglePatternResultDefinition();
		final PatternSetResultDefinition patternSetResultDefinition = new PatternSetResultDefinition();
		resultDefinitionParameter = Parameters.rangeEnumerableParameter("Result definition", "Determines what is considered an result of a study trial performed by a participant.", ResultDefinition.class, () -> ImmutableList.of(
				singlePatternResultDefinition,
				patternSetResultDefinition));

		inputDataParameter = new IdentifierInRepositoryParameter<Supplier<Workspace>>(
				"Input", "Select the input data for this task.",
				ApplicationRepositories.DATA_REPOSITORY);

		evaluationSchemeParameter = new IdentifierInRepositoryParameter<EvaluationScheme>(
				"Evaluation Scheme",
				"Select an evaluation scheme for this task. An evaluation scheme defines the corresponding evaluation task.",
				ApplicationRepositories.STUDY_EVALUATION_SCHEME_REPOSITORY);

		this.parameters = ImmutableList.of(nameParameter, descriptionParameter,
				instructionParameter, resultDefinitionParameter,
				inputDataParameter, evaluationSchemeParameter);

	}

	public TaskSpecification build() {
		validate();
		List<String> instructionPageHtmls = new ArrayList<>();
		List<String> fileNames = instructionParameter.current();
		for (String fileName : fileNames) {
			instructionPageHtmls.add(ServerPaths
					.getContentFileContentAsString(fileName));
		}
		new ApplicationRepositories();
		return new TaskSpecification(nameParameter.current(),
				descriptionParameter.current(), instructionPageHtmls,
				resultDefinitionParameter.current(),
				inputDataParameter.current(),
				ApplicationRepositories.STUDY_EVALUATION_SCHEME_REPOSITORY
						.getEntry(evaluationSchemeParameter.current())
						.getContent());
	}

	@Override
	public List<Parameter<?>> getTopLevelParameters() {
		return parameters;
	}

}
