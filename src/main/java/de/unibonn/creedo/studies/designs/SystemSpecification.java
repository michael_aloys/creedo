package de.unibonn.creedo.studies.designs;

import de.unibonn.creedo.webapp.dashboard.mining.MiningSystemBuilder;

public class SystemSpecification {
	
	private final String name;
	
	private final String description;

	private final MiningSystemBuilder<?> miningSystemBuilder;
	
	public SystemSpecification(String name, String description, MiningSystemBuilder miningSystemBuilder) {
		this.name = name;
		this.description = description;
		this.miningSystemBuilder = miningSystemBuilder;
	}
	
	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public MiningSystemBuilder<?> getMiningSystemBuilder() {
		return miningSystemBuilder;
	}
}
