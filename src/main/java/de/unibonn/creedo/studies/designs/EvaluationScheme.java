package de.unibonn.creedo.studies.designs;

import static de.unibonn.realkd.common.parameter.Parameters.stringParameter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import com.google.common.collect.Sets;

import de.unibonn.creedo.ApplicationRepositories;
import de.unibonn.creedo.repositories.Repositories;
import de.unibonn.creedo.setup.ServerPaths;
import de.unibonn.creedo.webapp.studies.rating.RatingMetric;
import de.unibonn.realkd.common.parameter.DefaultParameterContainer;
import de.unibonn.realkd.common.parameter.DefaultSubCollectionParameter;
import de.unibonn.realkd.common.parameter.DefaultSubCollectionParameter.CollectionComputer;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.ParameterContainer;
import de.unibonn.realkd.common.parameter.SubCollectionParameter;
import de.unibonn.realkd.common.parameter.ValueValidator;

public class EvaluationScheme implements ParameterContainer {

	private final Parameter<String> nameParameter;

	private final Parameter<String> descriptionParameter;

	private final SubCollectionParameter<String, List<String>> instructionParameter;

	private final SubCollectionParameter<RatingMetric, Set<RatingMetric>> ratingMetricParameter;

	private final DefaultParameterContainer defaultParameterContainer;

	public EvaluationScheme() {

		nameParameter = stringParameter("Name",
				"The name of this evaluation scheme.", "",
				ValueValidator.VALUE_NOT_NULL_VALIDATOR, "");

		descriptionParameter = stringParameter("Description",
				"A descripton that explains this evalaution scheme.", "",
				ValueValidator.VALUE_NOT_NULL_VALIDATOR, "");

		instructionParameter = Repositories
				.getIdentifierListOfRepositoryParameter(
						"Instructions",
						"Select a list of instruction pages for the corresponding evaluation task.",
						ApplicationRepositories.CONTENT_FOLDER_REPOSITORY,
						Repositories
								.getIdIsFilenameWithExtensionPredicate(Repositories.HTML_FILE_EXTENSIONS));

		ratingMetricParameter = DefaultSubCollectionParameter
				.subSetParameter(
						"Rating Metrics",
						"Select a set of rating metrics that evaluators will use to rate the reuslts from trial tasks.",
						new CollectionComputer<Set<RatingMetric>>() {
							@Override
							public Set<RatingMetric> computeCollection() {
								return Sets.newHashSet(RatingMetric.values());
							}
						});

		defaultParameterContainer = new DefaultParameterContainer();

		defaultParameterContainer.addAllParameters(Arrays.asList(
				(Parameter<?>) nameParameter, descriptionParameter,
				instructionParameter, ratingMetricParameter));

	}

	public List<String> getInstructions() {
		List<String> instructions = new ArrayList<>();
		for (String fileName : instructionParameter.current()) {
			instructions.add(ServerPaths
					.getContentFileContentAsString(fileName));
		}
		return instructions;
	}

	public List<RatingMetric> getRatingMetrics() {
		return new ArrayList<>(ratingMetricParameter.current());

	}

	public String getName() {
		return nameParameter.current();
	}

	public String getDescription() {
		return descriptionParameter.current();
	}

	@Override
	public List<Parameter<?>> getTopLevelParameters() {
		return defaultParameterContainer.getTopLevelParameters();
	}

}
