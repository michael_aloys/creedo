package de.unibonn.creedo.studies.designs;

import static de.unibonn.realkd.common.parameter.Parameters.stringParameter;

import java.util.List;

import com.google.common.collect.ImmutableList;

import de.unibonn.creedo.ApplicationRepositories;
import de.unibonn.creedo.repositories.IdentifierInRepositoryParameter;
import de.unibonn.creedo.webapp.dashboard.mining.MiningSystemBuilder;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.ParameterContainer;
import de.unibonn.realkd.common.parameter.ValueValidator;

public class SystemSpecBuilder implements ParameterContainer {

	private final Parameter<String> nameParameter;

	private final Parameter<String> descriptionParameter;

	private final Parameter<String> analyticsDashboardIdParameter;

	private final ImmutableList<Parameter<?>> parameters;

	public SystemSpecBuilder() {
		nameParameter = stringParameter("Name",
				"The name of system specification.", "",
				ValueValidator.VALUE_NOT_NULL_VALIDATOR, "");

		descriptionParameter = stringParameter("Description",
				"A descripton that explains the system specification.", "",
				ValueValidator.VALUE_NOT_NULL_VALIDATOR, "");

		analyticsDashboardIdParameter = new IdentifierInRepositoryParameter<MiningSystemBuilder<?>>(
				"Analytics Dashboard Builder", "Mining system",
				ApplicationRepositories.MINING_SYSTEM_REPOSITORY);

		this.parameters = ImmutableList.of(nameParameter, descriptionParameter,
				analyticsDashboardIdParameter);
	}

	public SystemSpecification build() {
		return new SystemSpecification(nameParameter.current(),
				descriptionParameter.current(),
				ApplicationRepositories.MINING_SYSTEM_REPOSITORY.getEntry(
						analyticsDashboardIdParameter.current())
						.getContent());
	}

	@Override
	public List<Parameter<?>> getTopLevelParameters() {
		return parameters;
	}

}
