package de.unibonn.creedo.studies;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import de.unibonn.creedo.ApplicationRepositories;
import de.unibonn.creedo.admin.users.CreedoUser;
import de.unibonn.creedo.studies.assignmentschemes.Assignment;
import de.unibonn.creedo.studies.assignmentschemes.AssignmentPhase;
import de.unibonn.creedo.studies.assignmentschemes.EvaluationAssignment;
import de.unibonn.creedo.studies.assignmentschemes.TrialAssignment;
import de.unibonn.creedo.studies.designs.StudyDesign;
import de.unibonn.creedo.studies.state.StudyState;
import de.unibonn.creedo.studies.state.mybatisimpl.OldCreedoMyBatisBasedStudyState;
import de.unibonn.creedo.studies.ui.TrialSessionTracker;
import de.unibonn.creedo.ui.HtmlPage;
import de.unibonn.creedo.ui.core.DefaultStaticPageFrameBuilder;
import de.unibonn.creedo.ui.core.PreparedFrame;
import de.unibonn.creedo.ui.core.UiComponent;
import de.unibonn.creedo.ui.core.UiRegister.FrameCloser;
import de.unibonn.creedo.ui.core.UiRegister.IdGenerator;
import de.unibonn.creedo.ui.indexpage.DashboardLink;
import de.unibonn.creedo.ui.mining.AnalyticsDashboard;
import de.unibonn.creedo.ui.mining.AnalyticsDashboardDependentComponentBuilder;
import de.unibonn.creedo.ui.util.ModalFrameBuilder;
import de.unibonn.creedo.webapp.dashboard.mining.DefaultAnalyticsDashboardBuilder;
import de.unibonn.creedo.webapp.dashboard.multistep.MultiStepDashboardModelBuilder;
import de.unibonn.creedo.webapp.dashboard.study.RatingDashboardModelBuilder;
import de.unibonn.realkd.common.workspace.Workspace;

/**
 * A study is an aggregation of a study design, a set of users in different
 * roles, as well as the state of all artifacts that are collected during a
 * study process (i.e., results and ratings).
 * 
 * @author Björn Jacobs
 * @author Mario Boley
 * @author Bo Kang
 * 
 * @since 0.1.0
 * 
 * @version 0.3.0
 * 
 */
public class Study {
	// private final NewStudyConfiguration config;

	private final String name;
	private final String description;
	private final String imgUrl;
	private final String imgCredits;

	// private final String designName;
	private final StudyState state;
	private AssignmentPhase phase;
	private final StudyDesign design;
	private final Collection<String> participantIds;
	private final Collection<String> evaluatorIds;

	public Study(String name, String description, String imgUrl,
			String imgCredits, StudyDesign design, AssignmentPhase phase,
			Collection<String> participantIds, Collection<String> evaluatorIds) {
		this.name = name;
		this.description = description;
		this.imgUrl = imgUrl;
		this.imgCredits = imgCredits;
		this.design = design;
		// this.designName = designName;
		// this.state = state;
		this.phase = phase;
		this.participantIds = participantIds;
		this.evaluatorIds = evaluatorIds;
		this.state = new OldCreedoMyBatisBasedStudyState(name, design);
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public String getImgUrl() {
		return imgUrl;
	}

	public String getImgCredits() {
		return imgCredits;
	}

	// public String getDesignName() {
	// return designName;
	// }

	public StudyState getState() {
		return state;
	}

	public Collection<String> getParticipantIds() {
		return participantIds;
	}

	public Collection<String> getEvaluatorIds() {
		return evaluatorIds;
	}

	private DashboardLink assignmentToDashboardLink(Assignment assignment) {
		if (assignment instanceof TrialAssignment) {
			return trialAssignmentToMiningDashboardLink((TrialAssignment) assignment);
		} else if (assignment instanceof EvaluationAssignment) {
			return evaluationAssignmentToDashboardLink((EvaluationAssignment) assignment);
		}
		throw new IllegalArgumentException(
				"Assignment class must be either TrialAssignment or EvaluationAssignment.");
	}

	private DashboardLink trialAssignmentToMiningDashboardLink(
			TrialAssignment assignment) {
		List<PreparedFrame> modelBuilders = new ArrayList<>();
		List<String> nextAndCloseButtonNames = new ArrayList<>();

		// Add instruction page
		List<String> instructions = assignment.getTask().getInstructions();
		int index = 1;
		for (String instruction : instructions) {
			modelBuilders.add(new DefaultStaticPageFrameBuilder(new HtmlPage(
					"Analysis Task Instructions " + index + "/"
							+ instructions.size(), "Instructions " + index,
					instruction)).addTimeOutHandler(true));

			nextAndCloseButtonNames.add("Next");
			index++;
		}

		Supplier<Workspace> workspaceSupplier = ApplicationRepositories.DATA_REPOSITORY
				.getEntry(assignment.getTask().getDataIdentifier())
				.getContent();

		DefaultAnalyticsDashboardBuilder analyticsDashboardBuilder = new DefaultAnalyticsDashboardBuilder(
				workspaceSupplier, assignment.getSystem()
						.getMiningSystemBuilder());
		analyticsDashboardBuilder
				.addDashboardDependentComponentBuilder(new AnalyticsDashboardDependentComponentBuilder() {

					@Override
					public UiComponent build(IdGenerator idGenerator,
							FrameCloser closer,
							AnalyticsDashboard analyticsDashboard) {
						return new TrialSessionTracker(idGenerator, closer,
								analyticsDashboard, getName(), getState(),
								assignment);
					}
				});

		modelBuilders.add(analyticsDashboardBuilder);
		nextAndCloseButtonNames.add("Submit results");

		MultiStepDashboardModelBuilder assignmentFrameBuilder = new MultiStepDashboardModelBuilder(
				modelBuilders);
		assignmentFrameBuilder.setNextAndCloseNames(nextAndCloseButtonNames);
		// assignmentFrameBuilder.setCloseHandler(closeHandler);

		PreparedFrame errorFrameBuilder = new DefaultStaticPageFrameBuilder(
				new HtmlPage(
						"Error",
						"Error",
						"It seems that the link you opened is no longer valid. Please close this window and refresh the index page."));

		return new DashboardLink(getName() + " [Trial]", getDescription(),
				getImgUrl(), getImgCredits(), new ModalFrameBuilder(
						() -> assignment.isValid(), assignmentFrameBuilder,
						errorFrameBuilder));
	}

	private DashboardLink evaluationAssignmentToDashboardLink(
			EvaluationAssignment assignment) {
		// 5. build rating dashboard.
		RatingDashboardModelBuilder ratingDashboardModelBuilder = new RatingDashboardModelBuilder(
				ApplicationRepositories.DATA_REPOSITORY.get(assignment
						.getTask().getDataIdentifier()), state, assignment);

		// get from DB the metrics the evaluation scheme for the selected
		// task;
		ratingDashboardModelBuilder.setRatingMetrics(assignment.getTask()
				.getEvaluationScheme().getRatingMetrics());

		List<PreparedFrame> modelBuilders = new ArrayList<>();
		List<String> nextAndCloseButtonNames = new ArrayList<>();

		// Add rating instruction page
		List<String> instructionPages = assignment.getTask()
				.getEvaluationScheme().getInstructions();
		int index = 1;
		for (String instructionPage : instructionPages) {
			modelBuilders.add(new DefaultStaticPageFrameBuilder(new HtmlPage(
					"Evaluation Instructions " + index + "/"
							+ instructionPages.size(), "Instructions " + index,
					instructionPage)).addTimeOutHandler(true));
			nextAndCloseButtonNames.add("Next");
			index++;
		}

		modelBuilders.add(ratingDashboardModelBuilder);
		nextAndCloseButtonNames.add("Submit ratings");

		MultiStepDashboardModelBuilder multiStepBuilder = new MultiStepDashboardModelBuilder(
				modelBuilders);
		multiStepBuilder.setNextAndCloseNames(nextAndCloseButtonNames);

		PreparedFrame errorFrameBuilder = new DefaultStaticPageFrameBuilder(
				new HtmlPage(
						"Error",
						"Error",
						"It seems that the link you opened is no longer valid. Please close this window and refresh the index page."));

		return new DashboardLink(getName() + " [Evaluation]", getDescription(),
				getImgUrl(), getImgCredits(), new ModalFrameBuilder(
						() -> assignment.isValid(), multiStepBuilder,
						errorFrameBuilder));
	}

	public List<DashboardLink> getDashboardLinks(CreedoUser user) {
		List<DashboardLink> dashboardLinks = new ArrayList<>();

		if (participantIds.contains(user.id())) {
			dashboardLinks.addAll(phase
					.getAssignments(state, user, StudyUserRole.PARTICIPANT,
							name, design.getSystemSpecifications(),
							design.getTaskSpecifications()).stream()
					.map(this::assignmentToDashboardLink)
					.collect(Collectors.toList()));
		}

		if (evaluatorIds.contains(user.id())) {

			dashboardLinks.addAll(phase
					.getAssignments(state, user, StudyUserRole.EVALUATOR, name,
							design.getSystemSpecifications(),
							design.getTaskSpecifications()).stream()
					.map(this::assignmentToDashboardLink)
					.collect(Collectors.toList()));

		}

		return dashboardLinks;
	}
}
