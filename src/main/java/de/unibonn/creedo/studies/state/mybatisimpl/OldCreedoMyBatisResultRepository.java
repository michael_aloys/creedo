package de.unibonn.creedo.studies.state.mybatisimpl;

import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.apache.ibatis.exceptions.PersistenceException;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import com.google.common.collect.ImmutableList;

import de.unibonn.creedo.repositories.DefaultRepositoryEntry;
import de.unibonn.creedo.repositories.RepositoryEntry;
import de.unibonn.creedo.repositories.RepositoryWithIdGenerator;
import de.unibonn.creedo.setup.DataBackEnd;
import de.unibonn.creedo.studies.designs.TaskSpecification;
import de.unibonn.creedo.webapp.studies.Result;
import de.unibonn.creedo.webapp.studies.ResultDataContainer;
import de.unibonn.realkd.common.JsonSerialization;
import de.unibonn.realkd.patterns.PatternBuilder;

public class OldCreedoMyBatisResultRepository implements
		RepositoryWithIdGenerator<Integer, Result> {

	private static final Logger LOGGER = Logger
			.getLogger(OldCreedoMyBatisResultRepository.class.getName());

	public static class ResultOfTaskPredicate implements
			Predicate<RepositoryEntry<Integer, Result>>, SqlSpecification {

		private final TaskSpecification task;

		public ResultOfTaskPredicate(TaskSpecification task) {
			this.task = task;
		}

		public String getSqlCondition() {
			return "study__sessions.task_name='" + task.getName() + "'";
		}

		@Override
		public boolean test(RepositoryEntry<Integer, Result> t) {
			return false;
		}

	};

	private final SqlSessionFactory sqlSessionFactory;

	private final String studyName;

	public OldCreedoMyBatisResultRepository(String studyName) {
		this.studyName = studyName;
		this.sqlSessionFactory = DataBackEnd.instance().getSessionFactory();
	}

	@Override
	public String getName() {
		return studyName + " results";
	}

	@Override
	public synchronized void add(Integer id, Result content) {
		SqlSession session = sqlSessionFactory.openSession();
		OldCreedoStudyStateMapper mapper = session
				.getMapper(OldCreedoStudyStateMapper.class);
		try {
			LOGGER.log(Level.INFO, "Attempting to store " + content
					+ " under id " + id);
			mapper.addResult(id, content.getSessionId(), JsonSerialization
					.toJson(content.getPatternBuilder(), PatternBuilder.class),
					content.getSecondsInSessionUntilSaved());
			session.commit();
			LOGGER.log(Level.INFO, "Successfully stored");
		} finally {
			session.close();
			LOGGER.log(Level.FINE, "Session for results closed");
		}
	}

	@Override
	public Integer addContentAndGenerateId(Result content) {
		SqlSession session = sqlSessionFactory.openSession();
		OldCreedoStudyStateMapper mapper = session
				.getMapper(OldCreedoStudyStateMapper.class);
		Integer id = null;
		try {
			LOGGER.log(Level.INFO, "Attempting to store '" + content + "'");
			mapper.addResultWithAutoId(content.getSessionId(),
					JsonSerialization.toJson(content.getPatternBuilder(),
							PatternBuilder.class), content
							.getSecondsInSessionUntilSaved());

			session.commit();
			id = mapper.getLastInsertedId();
			LOGGER.log(Level.INFO, "Successfully stored under '" + id + "'");
			return id;
		} finally {
			session.close();
			LOGGER.log(Level.FINE, "Session for results closed");
		}
	}

	@Override
	public void delete(Integer id) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void update(RepositoryEntry<Integer, Result> entry) {
		throw new UnsupportedOperationException();
	}

	@Override
	public List<Integer> getAllIds() {
		SqlSession session = sqlSessionFactory.openSession();
		OldCreedoStudyStateMapper mapper = session
				.getMapper(OldCreedoStudyStateMapper.class);
		List<Integer> resultIds = mapper.getAllResultIds();
		session.commit();
		return resultIds;
	}

	@Override
	public RepositoryEntry<Integer, Result> getEntry(Integer id) {
		SqlSession session = sqlSessionFactory.openSession();
		OldCreedoStudyStateMapper sessionMapper = session
				.getMapper(OldCreedoStudyStateMapper.class);
		try {
			ResultDataContainer container = sessionMapper.getResult(id);
			session.commit();
			if (container == null) {
				throw new IllegalArgumentException("No result with id: " + id);
			}
			return new DefaultRepositoryEntry<Integer, Result>(id,
					toResult(container));
		} finally {
			session.close();
		}
	}

	@Override
	public Collection<RepositoryEntry<Integer, Result>> getAllEntries() {
		List<ResultDataContainer> containers = fetchAllResultContainers(() -> "true");
		return toRepositoryEntries(containers);
	}

	private List<RepositoryEntry<Integer, Result>> toRepositoryEntries(
			List<ResultDataContainer> containers) {
		return containers
				.stream()
				.map(c -> new DefaultRepositoryEntry<Integer, Result>(c
						.getResultId(), toResult(c)))
				.collect(Collectors.toList());
	}

	private Result toResult(ResultDataContainer container) {
		Object patternBuilder = JsonSerialization.fromJson(
				container.getResultBuilderContent(), PatternBuilder.class);
		if (!(patternBuilder instanceof PatternBuilder)) {
			throw new IllegalArgumentException(
					"Invalid class found for result patternBuilder");
		}
		PatternBuilder castPatternBuilder = (PatternBuilder) patternBuilder;
		Result result = new Result(castPatternBuilder,
				container.getSessionId(),
				container.getSecondsInSessionUntilSaved());
		return result;
	}

	private List<ResultDataContainer> fetchAllResultContainers(
			SqlSpecification spec) {
		SqlSession session = sqlSessionFactory.openSession();
		OldCreedoStudyStateMapper sessionMapper = session
				.getMapper(OldCreedoStudyStateMapper.class);
		List<ResultDataContainer> containers;
		try {
			containers = sessionMapper.getResultsWhere(studyName,
					spec.getSqlCondition());
		} catch (PersistenceException pe) {
			pe.printStackTrace();
			containers = ImmutableList.of();
		} finally {
			session.commit();
			session.close();
		}
		return containers;
	}

	@Override
	public Collection<RepositoryEntry<Integer, Result>> getAllEntries(
			Predicate<RepositoryEntry<Integer, Result>> predicate) {
		List<ResultDataContainer> resultContainers;
		if (predicate instanceof SqlSpecification) {
			resultContainers = fetchAllResultContainers((SqlSpecification) predicate);
			LOGGER.log(Level.FINE, "Found " + resultContainers.size()
					+ " results matching spec '"
					+ ((SqlSpecification) predicate).getSqlCondition() + "'");
			LOGGER.log(
					Level.FINE,
					"Ids '"
							+ resultContainers.stream()
									.map(c -> c.getResultId())
									.collect(Collectors.toList()) + "'");
			return toRepositoryEntries(resultContainers);
		} else {
			LOGGER.log(Level.WARNING,
					"Predicate does not specify optimized db query - might be slow");
			return RepositoryWithIdGenerator.super.getAllEntries(predicate);
		}
	}

}