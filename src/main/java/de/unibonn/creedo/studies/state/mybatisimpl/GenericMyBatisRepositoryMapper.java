package de.unibonn.creedo.studies.state.mybatisimpl;

import java.util.List;

import de.unibonn.realkd.common.RuntimeBuilder;

public interface GenericMyBatisRepositoryMapper<T, K> {

	public RuntimeBuilder<T, K> getContentBuilder(Integer id);
	
	public List<RuntimeBuilder<T, K>> getAllContentWhere(SqlSpecification spec);

}
