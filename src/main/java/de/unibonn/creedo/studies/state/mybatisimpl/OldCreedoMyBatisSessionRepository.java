package de.unibonn.creedo.studies.state.mybatisimpl;

import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.apache.ibatis.exceptions.PersistenceException;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import com.google.common.collect.ImmutableList;

import de.unibonn.creedo.admin.users.CreedoUser;
import de.unibonn.creedo.admin.users.Users;
import de.unibonn.creedo.repositories.DefaultRepositoryEntry;
import de.unibonn.creedo.repositories.RepositoryEntry;
import de.unibonn.creedo.repositories.RepositoryWithIdGenerator;
import de.unibonn.creedo.setup.DataBackEnd;
import de.unibonn.creedo.studies.designs.StudyDesign;
import de.unibonn.creedo.studies.designs.SystemSpecification;
import de.unibonn.creedo.studies.designs.TaskSpecification;
import de.unibonn.creedo.studies.state.TrialSession;

public class OldCreedoMyBatisSessionRepository implements
		RepositoryWithIdGenerator<Integer, TrialSession> {

	private static final Logger LOGGER = Logger
			.getLogger(OldCreedoMyBatisSessionRepository.class.getName());

	private final String studyName;

	private final StudyDesign design;

	private final SqlSessionFactory sqlSessionFactory;

	public OldCreedoMyBatisSessionRepository(String studyName,
			StudyDesign design) {
		this.studyName = studyName;
		this.design = design;
		this.sqlSessionFactory = DataBackEnd.instance().getSessionFactory();
	}

	@Override
	public String getName() {
		return studyName + " trial sessions";
	}

	@Override
	public synchronized void add(Integer id, TrialSession content) {
		SqlSession session = sqlSessionFactory.openSession();
		OldCreedoStudyStateMapper mapper = session
				.getMapper(OldCreedoStudyStateMapper.class);
		try {
			LOGGER.log(Level.INFO, "Trying to store " + content + " under id "
					+ id);
			mapper.addTrialSession(id, content);
			session.commit();
			LOGGER.log(Level.INFO, "Successfully stored");
		} finally {
			session.close();
			LOGGER.log(Level.FINE, "Session for trial session closed.");
		}
	}

	@Override
	public Integer addContentAndGenerateId(TrialSession content) {
		SqlSession session = sqlSessionFactory.openSession();
		OldCreedoStudyStateMapper mapper = session
				.getMapper(OldCreedoStudyStateMapper.class);
		Integer id = null;
		try {
			LOGGER.log(Level.INFO, "Trying to store " + content);
			mapper.addTrialSessionWithAutoId(content);
			session.commit();
			id = mapper.getLastInsertedId();
			LOGGER.log(Level.INFO, "Succesfully stored under id " + id);
			return id;
		} finally {
			session.close();
		}
	}

	@Override
	public void delete(Integer id) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void update(RepositoryEntry<Integer, TrialSession> entry) {
		throw new UnsupportedOperationException();
	}

	@Override
	public List<Integer> getAllIds() {
		SqlSession session = sqlSessionFactory.openSession();
		OldCreedoStudyStateMapper mapper = session
				.getMapper(OldCreedoStudyStateMapper.class);
		List<Integer> resultIds = mapper.getAllTrialSessionIds();
		session.commit();
		return resultIds;
	}

	@Override
	public RepositoryEntry<Integer, TrialSession> getEntry(Integer id) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Collection<RepositoryEntry<Integer, TrialSession>> getAllEntries() {
		List<MyBatisTrialSessionQueryResult> queryResults = fetchAllResultContainers(() -> "true");
		return toTrialSessions(queryResults);
	}

	private List<RepositoryEntry<Integer, TrialSession>> toTrialSessions(
			List<MyBatisTrialSessionQueryResult> queryResults) {
		return queryResults
				.stream()
				.map(r -> new DefaultRepositoryEntry<>(r.getSessionId(),
						toTrialSession(r))).collect(Collectors.toList());
	}

	private TrialSession toTrialSession(
			MyBatisTrialSessionQueryResult queryResult) {
		SystemSpecification system = design.getSystemSpecifications().stream()
				.filter(s -> s.getName().equals(queryResult.getSystemName()))
				.findAny().get();
		TaskSpecification task = design.getTaskSpecifications().stream()
				.filter(t -> t.getName().equals(queryResult.getTaskName()))
				.findAny().get();
		return new TrialSession(studyName, Users.getUser(
				queryResult.getUserId()).get(), system, task,
				queryResult.getEndTime());
	}

	private List<MyBatisTrialSessionQueryResult> fetchAllResultContainers(
			SqlSpecification spec) {
		SqlSession session = sqlSessionFactory.openSession();
		OldCreedoStudyStateMapper sessionMapper = session
				.getMapper(OldCreedoStudyStateMapper.class);
		List<MyBatisTrialSessionQueryResult> containers;
		try {
			containers = sessionMapper.getTrialSessionsWhere(studyName,
					spec.getSqlCondition());
		} catch (PersistenceException pe) {
			pe.printStackTrace();
			containers = ImmutableList.of();
		} finally {
			session.commit();
			session.close();
		}
		return containers;
	}

	@Override
	public Collection<RepositoryEntry<Integer, TrialSession>> getAllEntries(
			Predicate<RepositoryEntry<Integer, TrialSession>> predicate) {
		List<MyBatisTrialSessionQueryResult> resultContainers;
		if (predicate instanceof SqlSpecification) {
			resultContainers = fetchAllResultContainers((SqlSpecification) predicate);
			return toTrialSessions(resultContainers);
		} else {
			LOGGER.log(Level.WARNING,
					"Predicate does not specify optimized db query - might be slow");
			return RepositoryWithIdGenerator.super.getAllEntries(predicate);
		}
	}

	public static Predicate<RepositoryEntry<Integer, TrialSession>> getTrialSessionOfUserPredicate(
			final CreedoUser user) {
		return new PredicateWithSqlSpecification<RepositoryEntry<Integer, TrialSession>>(
				t -> user.equals(t.getContent().getUser()), () -> "user_id='"
						+ user.id() + "'");
	}

	public static Predicate<RepositoryEntry<Integer, TrialSession>> getTrialSessionOfSystemPredicate(
			final SystemSpecification system) {
		return new PredicateWithSqlSpecification<RepositoryEntry<Integer, TrialSession>>(
				t -> system.equals(t.getContent().getSystem()),
				() -> "system_name='" + system.getName() + "'");
	};

	public static Predicate<RepositoryEntry<Integer, TrialSession>> getTrialSessionOfTaskPredicate(
			final TaskSpecification task) {
		return new PredicateWithSqlSpecification<RepositoryEntry<Integer, TrialSession>>(
				t -> task.equals(t.getContent().getTask()), () -> "task_name='"
						+ task.getName() + "'");
	}

}
