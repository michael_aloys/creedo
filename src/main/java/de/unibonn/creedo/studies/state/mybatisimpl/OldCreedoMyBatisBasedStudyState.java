package de.unibonn.creedo.studies.state.mybatisimpl;

import java.util.function.Predicate;

import de.unibonn.creedo.admin.users.CreedoUser;
import de.unibonn.creedo.repositories.RepositoryEntry;
import de.unibonn.creedo.repositories.RepositoryWithIdGenerator;
import de.unibonn.creedo.studies.designs.StudyDesign;
import de.unibonn.creedo.studies.designs.SystemSpecification;
import de.unibonn.creedo.studies.designs.TaskSpecification;
import de.unibonn.creedo.studies.state.StudyState;
import de.unibonn.creedo.studies.state.TrialSession;
import de.unibonn.creedo.webapp.studies.Result;

public class OldCreedoMyBatisBasedStudyState implements StudyState {

	private final String studyName;

	private final StudyDesign design;

	private final RepositoryWithIdGenerator<Integer, Result> resultRepository;

	private final RepositoryWithIdGenerator<Integer, TrialSession> trialSessionRepository;

	public OldCreedoMyBatisBasedStudyState(String studyName, StudyDesign design) {
		this.design = design;
		this.studyName = studyName;
		this.resultRepository = new OldCreedoMyBatisResultRepository(studyName);
		this.trialSessionRepository = new OldCreedoMyBatisSessionRepository(
				studyName, design);
	}

	@Override
	public RepositoryWithIdGenerator<Integer, Result> getResultRepository() {
		return resultRepository;
	}

	@Override
	public RepositoryWithIdGenerator<Integer, TrialSession> getTrialSessionRepository() {
		return trialSessionRepository;
	}

	@Override
	public Predicate<RepositoryEntry<Integer, Result>> getResultOfTaskPredicate(
			TaskSpecification task) {
		return new OldCreedoMyBatisResultRepository.ResultOfTaskPredicate(task);
	}

	public Predicate<RepositoryEntry<Integer, TrialSession>> getTrialSessionOfUserPredicate(
			CreedoUser user) {
		return OldCreedoMyBatisSessionRepository
				.getTrialSessionOfUserPredicate(user);
	}

	@Override
	public Predicate<RepositoryEntry<Integer, TrialSession>> getTrialSessionOfSystemPredicate(
			SystemSpecification system) {
		return OldCreedoMyBatisSessionRepository
				.getTrialSessionOfSystemPredicate(system);
	}

	@Override
	public Predicate<RepositoryEntry<Integer, TrialSession>> getTrialSessionOfTaskPredicate(
			TaskSpecification task) {
		return OldCreedoMyBatisSessionRepository
				.getTrialSessionOfTaskPredicate(task);
	}

}
