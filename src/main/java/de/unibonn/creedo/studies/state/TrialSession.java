package de.unibonn.creedo.studies.state;

import java.util.Date;

import de.unibonn.creedo.admin.users.CreedoUser;
import de.unibonn.creedo.studies.designs.SystemSpecification;
import de.unibonn.creedo.studies.designs.TaskSpecification;

/**
 * Data class for representing study trial sessions, during which a study
 * participant worked on a study trial task.
 * 
 * @author Mario Boley
 *
 * @since 0.1.2
 * 
 * @version 0.1.2.1
 * 
 */
public class TrialSession {

	private final CreedoUser user;

	private final String studyName;

	private final SystemSpecification system;

	private final TaskSpecification task;

	private final Date endDate;

	public TrialSession(String studyName, CreedoUser user,
			SystemSpecification system, TaskSpecification task, Date endDate) {
		this.studyName = studyName;
		this.user = user;
		this.system = system;
		this.task = task;
		this.endDate = endDate;
	}

	public CreedoUser getUser() {
		return user;
	}

	public SystemSpecification getSystem() {
		return system;
	}

	public TaskSpecification getTask() {
		return task;
	}

	public Date getEndDate() {
		return endDate;
	}

	public String getStudyName() {
		return studyName;
	}

	public String toString() {
		return "Session of user " + user.id() + " on " + task.getName()
				+ " using " + system.getName();
	}

}
