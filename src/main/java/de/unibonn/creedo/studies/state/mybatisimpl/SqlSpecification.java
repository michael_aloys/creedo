package de.unibonn.creedo.studies.state.mybatisimpl;

public interface SqlSpecification {

	/**
	 * @return condition that can be plugged verbatim into the where part of an
	 *         sql statement
	 */
	public String getSqlCondition();

	public default SqlSpecification and(SqlSpecification other) {
		String thisCondition = this.getSqlCondition();
		String otherCondition = other.getSqlCondition();
		return new SqlSpecification() {
			@Override
			public String getSqlCondition() {
				return "(" + thisCondition + " AND " + otherCondition + ")";
			}
		};
	}

}