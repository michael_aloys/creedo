package de.unibonn.creedo.studies.state;

import java.util.function.Predicate;

import de.unibonn.creedo.admin.users.CreedoUser;
import de.unibonn.creedo.repositories.RepositoryEntry;
import de.unibonn.creedo.repositories.RepositoryWithIdGenerator;
import de.unibonn.creedo.studies.designs.SystemSpecification;
import de.unibonn.creedo.studies.designs.TaskSpecification;
import de.unibonn.creedo.webapp.studies.Result;

/**
 * <p>
 * Subsumes all contents of a study that are accumulated throughout the study
 * process, trial sessions, results, and ratings, in the form of repositories
 * and provides predicates for efficiently querying these repositories.
 * </p>
 * 
 * @author Mario Boley
 * 
 * @since 0.1.2
 * 
 * @version 0.1.2.1
 *
 */
public interface StudyState {

	public RepositoryWithIdGenerator<Integer, TrialSession> getTrialSessionRepository();

	public RepositoryWithIdGenerator<Integer, Result> getResultRepository();

	public Predicate<RepositoryEntry<Integer, Result>> getResultOfTaskPredicate(
			TaskSpecification task);

	public Predicate<RepositoryEntry<Integer, TrialSession>> getTrialSessionOfUserPredicate(
			CreedoUser user);

	public Predicate<RepositoryEntry<Integer, TrialSession>> getTrialSessionOfSystemPredicate(
			SystemSpecification system);

	public Predicate<RepositoryEntry<Integer, TrialSession>> getTrialSessionOfTaskPredicate(
			TaskSpecification task);

}
