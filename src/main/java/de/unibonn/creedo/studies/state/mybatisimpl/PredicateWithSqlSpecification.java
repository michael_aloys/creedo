package de.unibonn.creedo.studies.state.mybatisimpl;

import java.util.function.Predicate;

public class PredicateWithSqlSpecification<T> implements Predicate<T>,
		SqlSpecification {

	private final Predicate<T> predicate;
	private final SqlSpecification sqlSpecification;

	public PredicateWithSqlSpecification(Predicate<T> predicate,
			SqlSpecification sqlSpecification) {
		this.predicate = predicate;
		this.sqlSpecification = sqlSpecification;
	}

	@Override
	public boolean test(T t) {
		return predicate.test(t);
	}

	@Override
	public String getSqlCondition() {
		return sqlSpecification.getSqlCondition();
	}

	@Override
	public Predicate<T> and(Predicate other) {
		if (other instanceof SqlSpecification) {
			return new PredicateWithSqlSpecification<T>(predicate.and(other),
					sqlSpecification.and((SqlSpecification) other));
		}
		return Predicate.super.and(other);
	}

}
