package de.unibonn.creedo.studies.ui;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.validation.support.BindingAwareModelMap;

import com.google.common.collect.ImmutableList;

import de.unibonn.creedo.studies.assignmentschemes.TrialAssignment;
import de.unibonn.creedo.studies.state.StudyState;
import de.unibonn.creedo.studies.state.TrialSession;
import de.unibonn.creedo.ui.core.Action;
import de.unibonn.creedo.ui.core.UiComponent;
import de.unibonn.creedo.ui.core.UiRegister.FrameCloser;
import de.unibonn.creedo.ui.core.UiRegister.IdGenerator;
import de.unibonn.creedo.ui.mining.AnalyticsDashboard;
import de.unibonn.creedo.webapp.studies.Result;
import de.unibonn.creedo.webapp.studies.result.ResultDefinition;
import de.unibonn.realkd.patterns.PatternBuilder;

/**
 * <p>
 * UI component that keeps track of and records the user's relevant actions
 * during her work on a study trial assignment with some analytics dashboard.
 * What is a relevant action is determined by the specific task that the user
 * works on.
 * </p>
 * <p>
 * Also this component publishes an action that allows the user to submit her
 * results as an external action. That is the component does not display a
 * control element to trigger the action itself, but instead relies on the
 * container to embed the action appropriately. In fact, this is an invisible
 * component, i.e., its view is empty.
 * </p>
 * 
 * @author Mario Boley
 * 
 * @since 0.1.2
 * 
 * @version 0.1.2.1
 *
 */
public class TrialSessionTracker implements UiComponent {

	private final class SubmitAction implements Action {

		private final int id;

		private final FrameCloser closer;

		private SubmitAction(IdGenerator idGenerator,
				AnalyticsDashboard analyticsDashboard, FrameCloser closer) {
			this.id = idGenerator.getNextId();
			this.closer = closer;
		}

		@Override
		public String getReferenceName() {
			return "Submit results";
		}

		@Override
		public ResponseEntity<String> activate(String... params) {
			ResultDefinition resultDefinition = assignment.getTask()
					.getResultDefinition();
			if (!resultDefinition.taskDone(analyticsDashboard)) {
				return new ResponseEntity<String>(
						resultDefinition.getHelp(analyticsDashboard),
						HttpStatus.FORBIDDEN);
			}

			LOGGER.info("Storing results of trial session.");
			List<PatternBuilder> resultPatternBuilder = resultDefinition
					.retrieveResultBuilders(analyticsDashboard);
			List<Integer> secondsUntilSavedList = resultDefinition
					.getListOfSecondsUntilSaved(analyticsDashboard);
			for (int i = 0; i < resultPatternBuilder.size(); i++) {
				Result result = new Result(resultPatternBuilder.get(i),
						trialSessionId, secondsUntilSavedList.get(i));
				studyState.getResultRepository()
						.addContentAndGenerateId(result);
			}

			closer.requestClose();
			return new ResponseEntity<String>(HttpStatus.OK);
		}

		@Override
		public ClientWindowEffect getEffect() {
			return ClientWindowEffect.CLOSE;
		}

		@Override
		public int getId() {
			return id;
		}
	}

	private static final Logger LOGGER = Logger
			.getLogger(TrialSessionTracker.class.getName());

	private final int id;

	private final Model model;

	private final AnalyticsDashboard analyticsDashboard;

	private final String studyName;

	private final StudyState studyState;

	private final TrialAssignment assignment;

	private final Action submitResultsAction;

	private final Integer trialSessionId;

	public TrialSessionTracker(final IdGenerator idGenerator,
			final FrameCloser closer,
			final AnalyticsDashboard analyticsDashboard, String studyName,
			StudyState studyState, TrialAssignment assignment) {
		this.id = idGenerator.getNextId();
		this.model = new BindingAwareModelMap();
		this.analyticsDashboard = analyticsDashboard;
		this.studyName = studyName;
		this.assignment = assignment;
		this.studyState = studyState;

		LOGGER.info("Creating new trial session.");
		TrialSession session = new TrialSession(studyName,
				assignment.getUser(), assignment.getSystem(),
				assignment.getTask(), new Date());

		trialSessionId = studyState.getTrialSessionRepository()
				.addContentAndGenerateId(session);

		submitResultsAction = new SubmitAction(idGenerator, analyticsDashboard,
				closer);
	}

	@Override
	public int getId() {
		return id;
	}

	@Override
	public String getView() {
		return "emptyPage.jsp";
	}

	@Override
	public Model getModel() {
		return model;
	}

	@Override
	public void tearDown() {
		LOGGER.info("Closing trial session");
	}

	@Override
	public List<Action> getExternalActions() {
		return ImmutableList.of(submitResultsAction);
	}

}
