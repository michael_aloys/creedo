package de.unibonn.creedo.repositories;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

//import com.google.common.base.Predicate;
//import com.google.common.collect.Iterables;
//import com.google.common.collect.Lists;

import java.util.function.Predicate;
import java.util.stream.Collectors;

import de.unibonn.realkd.common.parameter.DefaultRangeEnumerableParameter;
import de.unibonn.realkd.common.parameter.Parameters;
import de.unibonn.realkd.common.parameter.RangeEnumerableParameter;

/**
 * Range enumarable parameter that can take on all values that match identifiers
 * in a given repository.
 * 
 * @see {@link RangeEnumerableParameter}, {@link Repository}
 * 
 * @author Mario Boley
 * 
 * @since 0.1.0
 * 
 * @version 0.1.0.1
 * 
 *          TODO: consolidate into Repositories
 *
 */
public class IdentifierInRepositoryParameter<T> extends DefaultRangeEnumerableParameter<String> {

	public static <T> RangeEnumerableParameter<String> getOptionalIdentifierInRepositoryParameter(final String name,
			final String description, final Repository<String, T> repository,
			final Predicate<RepositoryEntry<String, T>> filterPredicate) {
		return Parameters.rangeEnumerableParameter(name, description, String.class, new RangeComputer<String>() {
			@Override
			public List<String> computeRange() {
				List<String> result = new ArrayList<>();
				result.add("---");
				result.addAll(repository.getAllEntries(filterPredicate).stream().map(entry -> entry.getId())
						.collect(Collectors.toList()));
				return result;
			}
		});
	}

	public static <T> RangeEnumerableParameter<Optional<String>> optionalRepositoryIdParameter(
			final String name, final String description, final Repository<String, T> repository,
			final Predicate<RepositoryEntry<String, T>> filterPredicate) {
		return Parameters.rangeEnumerableParameter(name, description, Optional.class, new RangeComputer<Optional<String>>() {
			@Override
			public List<Optional<String>> computeRange() {
				List<Optional<String>> result = new ArrayList<>();
				result.add(Optional.empty());
				result.addAll(repository.getAllEntries(filterPredicate).stream()
						.map(entry -> Optional.of(entry.getId())).collect(Collectors.toList()));
				return result;
			}
		});
	}

	public IdentifierInRepositoryParameter(final String name, final String description,
			final Repository<String, T> repository, final Predicate<RepositoryEntry<String, T>> filterPredicate) {
		super(name, description, String.class, new RangeComputer<String>() {
			@Override
			public List<String> computeRange() {
				return repository.getAllEntries(filterPredicate).stream().map(entry -> entry.getId())
						.collect(Collectors.toList());
			}
		});
	}

	public IdentifierInRepositoryParameter(final String name, final String description,
			final Repository<String, T> repository) {
		this(name, description, repository, entry -> true);
	}

}
