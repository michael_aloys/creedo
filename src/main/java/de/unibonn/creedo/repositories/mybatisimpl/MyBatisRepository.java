package de.unibonn.creedo.repositories.mybatisimpl;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import de.unibonn.creedo.repositories.DefaultRepositoryEntry;
import de.unibonn.creedo.repositories.Repository;
import de.unibonn.creedo.repositories.RepositoryEntry;
import de.unibonn.creedo.setup.DataBackEnd;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.ParameterContainer;

/**
 * <p>
 * DB-backed repository implementation that correctly serializes and
 * deserializes objects of classes that:
 * </p>
 * <ol>
 * <li>have a zero-parameter constructor and
 * <li>have a state that is completely given by their documented parameters (in
 * terms of the contract of {@link ParameterContainer})
 * </ol>
 * 
 * @author Mario Boley
 * 
 * @since 0.1.0
 * 
 * @version 0.1.0.1
 *
 * @param <T>
 *            The content type of the repository
 */
public class MyBatisRepository<T> implements Repository<String, T> {

	private static final Logger LOGGER = Logger.getLogger(MyBatisRepository.class.getName());

	private final String name;

	private final String entriesTableName;

	private final String contentParametersTableName;

	private final SqlSessionFactory sqlSessionFactory;

	public MyBatisRepository(String name, String entriesTableName, String contentParametersTableName) {
		this.name = name;
		this.sqlSessionFactory = DataBackEnd.instance().getSessionFactory();
		this.entriesTableName = entriesTableName;
		this.contentParametersTableName = contentParametersTableName;
		createsTableIfNotExist();
	}

	private void createsTableIfNotExist() {
		SqlSession session = this.sqlSessionFactory.openSession();
		RepositoryEntryTableMapper mapper = session.getMapper(RepositoryEntryTableMapper.class);
		mapper.createEntryTableIfNotExists(entriesTableName);
		mapper.createParameterTableIfNotExists(entriesTableName, contentParametersTableName);
		session.commit();
		session.close();
	}

	@Override
	public void add(String id, T content) {
		DefaultRepositoryEntry<String, T> entry = new DefaultRepositoryEntry<String, T>(id, content);
		SqlSession session = sqlSessionFactory.openSession();
		RepositoryEntryTableMapper mapper = session.getMapper(RepositoryEntryTableMapper.class);
		mapper.insertEntryRow(entriesTableName, entry.getId(), entry.getContent().getClass().getCanonicalName());
		if (content instanceof ParameterContainer) {
			for (Parameter<?> param : ((ParameterContainer) content).getTopLevelParameters()) {
				mapper.insertEntryContentParameterRow(contentParametersTableName, entry.getId(), param.getName(),
						param.current().toString());
			}
		}
		session.commit();
		session.close();
	}

	@Override
	public void delete(String id) {
		SqlSession session = sqlSessionFactory.openSession();
		RepositoryEntryTableMapper mapper = session.getMapper(RepositoryEntryTableMapper.class);

		mapper.deleteEntryContentParameterRows(contentParametersTableName, id);
		mapper.deleteEntryRow(entriesTableName, id);

		session.commit();
		session.close();
	}

	@Override
	public void update(RepositoryEntry<String, T> entry) {
		SqlSession session = sqlSessionFactory.openSession();
		RepositoryEntryTableMapper mapper = session.getMapper(RepositoryEntryTableMapper.class);

		T content = entry.getContent();

		if (content instanceof ParameterContainer) {
			mapper.deleteEntryContentParameterRows(contentParametersTableName, entry.getId());
			for (Parameter<?> param : ((ParameterContainer) content).getAllParameters()) {
				mapper.insertEntryContentParameterRow(contentParametersTableName, entry.getId(), param.getName(),
						param.current().toString());
			}
		}
		session.commit();
		session.close();
	}

	@Override
	public RepositoryEntry<String, T> getEntry(String id) {
		SqlSession session = sqlSessionFactory.openSession();
		RepositoryEntryTableMapper mapper = session.getMapper(RepositoryEntryTableMapper.class);
		EntryTableRow entryRow = mapper.getEntryRow(this.entriesTableName, id);
		List<ParameterTableRow> parameterRows = mapper.getEntryParameterRows(this.contentParametersTableName, id);
		session.close();

		T content = getContentInstance(entryRow, parameterRows);

		return new DefaultRepositoryEntry<String, T>(entryRow.getId(), content);
	}

	private T getContentInstance(EntryTableRow entryRow, List<ParameterTableRow> parameterRows) {
		Class<?> contentClass = null;
		try {
			contentClass = Class.forName(entryRow.getContentClassName());
		} catch (ClassNotFoundException e) {
			throw new IllegalStateException("Could not find content class: " + entryRow.getContentClassName());
		}
		T content = null;
		try {
			content = (T) contentClass.newInstance();
		} catch (InstantiationException e) {
			throw new IllegalStateException("Could not instantiate class: " + entryRow.getContentClassName());
		} catch (IllegalAccessException e) {
			throw new IllegalStateException("Could not access constructor of: " + entryRow.getContentClassName());
		}
		if (content instanceof ParameterContainer) {
			for (ParameterTableRow row : parameterRows) {
				if (!((ParameterContainer) content).hasParameter(row.getParameterName())) {
					LOGGER.log(Level.WARNING, "Parameter " + row.getParameterName()
							+ " not present and is skipped for deserialization of " + content);
					continue;
				}
				((ParameterContainer) content).findParameterByName(row.getParameterName())
						.setByString(row.getParameterValue());
			}
		}
		return content;
	}

	@Override
	public List<RepositoryEntry<String, T>> getAllEntries() {
		SqlSession session = sqlSessionFactory.openSession();
		RepositoryEntryTableMapper mapper = session.getMapper(RepositoryEntryTableMapper.class);
		List<EntryTableRow> entryRows = mapper.getAllEntryRows(this.entriesTableName);

		List<RepositoryEntry<String, T>> result = new ArrayList<>();
		for (EntryTableRow row : entryRows) {
			List<ParameterTableRow> parameterRows = mapper.getEntryParameterRows(this.contentParametersTableName,
					row.getId());
			result.add(new DefaultRepositoryEntry<String, T>(row.getId(), getContentInstance(row, parameterRows)));
		}

		session.close();
		return result;
	}

	@Override
	public List<String> getAllIds() {
		SqlSession session = sqlSessionFactory.openSession();
		RepositoryEntryTableMapper mapper = session.getMapper(RepositoryEntryTableMapper.class);
		List<EntryTableRow> entryRows = mapper.getAllEntryRows(this.entriesTableName);
		session.close();

		List<String> result = new ArrayList<>();
		for (EntryTableRow row : entryRows) {
			result.add(row.getId());
		}

		return result;
	}

	@Override
	public String getName() {
		return name;
	}

}
