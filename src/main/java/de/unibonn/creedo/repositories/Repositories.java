package de.unibonn.creedo.repositories;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import de.unibonn.realkd.common.parameter.DefaultSubCollectionParameter;
import de.unibonn.realkd.common.parameter.DefaultSubCollectionParameter.CollectionComputer;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.SubCollectionParameter;

/**
 * <p>
 * Provides static methods for working with {@link Repository} objects. In
 * particular:
 * </p>
 * <ul>
 * <li>
 * It allows to obtain predicates that can be used for filtering repositories
 * via {@link Repository#getAllEntries(Predicate)}. The predicates returned by methods
 * of this class might allow the actual repository implementation to provide a
 * more efficient filtering than the default fallback of naively iterating
 * through through all repository elements.
 * <li>
 * It allows to obtain {@link Parameter} objects for parameters the valid values
 * of which are repository identifiers.
 * </ul>
 * 
 * @author Mario Boley
 * 
 * @since 0.1.0
 * 
 * @version 0.1.2.1
 *
 */
public class Repositories {

	public static String[] HTML_FILE_EXTENSIONS = { "htm", "html" };

	public static String[] IMAGE_FILE_EXTENSIONS = { "jpg", "png", "gif", "bmp" };

	public static String[] CSV_FILE_EXTENSIONS = { "csv", "txt" };

	public static <T> Predicate<RepositoryEntry<String, T>> getIdMatchingRegexPredicate(
			final Pattern regexPattern) {
		return new Predicate<RepositoryEntry<String, T>>() {

			@Override
			public boolean test(RepositoryEntry<String, T> entry) {
				return regexPattern.matcher(entry.getId()).matches();
			}
		};
	}

	public static <T> Predicate<RepositoryEntry<String, T>> getIdIsFilenameWithExtensionPredicate(
			String... extensions) {
		StringBuilder extensionPart = new StringBuilder();
		Iterator<String> extIterator = Arrays.asList(extensions).iterator();
		while (extIterator.hasNext()) {
			extensionPart.append(extIterator.next());
			if (extIterator.hasNext()) {
				extensionPart.append("|");
			}
		}
		Pattern pattern = Pattern.compile("([^\\s]+(\\.(?i)("
				+ extensionPart.toString() + "))$)");

		return getIdMatchingRegexPredicate(pattern);
	}

	public static <T> SubCollectionParameter<String, List<String>> getIdentifierListOfRepositoryParameter(
			final String name, final String description,
			final Repository<String, T> repository) {
		return Repositories.getIdentifierListOfRepositoryParameter(name,
				description, repository, entry -> true);
	}

	public static <T> SubCollectionParameter<String, List<String>> getIdentifierListOfRepositoryParameter(
			final String name, final String description,
			final Repository<String, T> repository,
			final Predicate<RepositoryEntry<String, T>> filterPredicate) {
		CollectionComputer<List<String>> collectionComputer = new CollectionComputer<List<String>>() {
			@Override
			public List<String> computeCollection() {
				return repository.getAllEntries(filterPredicate).stream()
						.map(entry -> entry.getId())
						.collect(Collectors.toList());
			}
		};
		return DefaultSubCollectionParameter.getDefaultSubListParameter(name,
				description, collectionComputer);
	}

	public static <T> SubCollectionParameter<String, Set<String>> getIdentifierSetOfRepositoryParameter(
			final String name, final String description,
			final Repository<String, T> repository) {
		return Repositories.getIdentifierSetOfRepositoryParameter(name,
				description, repository, entry -> true);
	}

	public static <T> SubCollectionParameter<String, Set<String>> getIdentifierSetOfRepositoryParameter(
			final String name, final String description,
			final Repository<String, T> repository,
			final Predicate<RepositoryEntry<String, T>> filterPredicate) {
		CollectionComputer<Set<String>> collectionComputer = new CollectionComputer<Set<String>>() {
			@Override
			public Set<String> computeCollection() {
				return repository.getAllEntries(filterPredicate).stream()
						.map(entry -> entry.getId())
						.collect(Collectors.toSet());
			}
		};
		return DefaultSubCollectionParameter.subSetParameter(name,
				description, collectionComputer);
	}

}
