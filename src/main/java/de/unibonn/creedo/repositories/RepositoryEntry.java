package de.unibonn.creedo.repositories;

public interface RepositoryEntry<K, T> {

	public K getId();

	public T getContent();

}
