<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div class="row">
	<div class="col-sm-12 content top-row" id="creedo-dataviewcontainer"
		componentId="${dataViewContainerId}"
		updatePointCloudActionId="${updatePointCloudActionId}">
		<c:set var="showTable" scope="page"
			value="${defaultDataView=='TABLE'}" />
		<c:set var="showScatter" scope="page"
			value="${defaultDataView=='SCATTER'}" />
		<c:set var="showPropositions" scope="page"
			value="${defaultDataView=='PROPOSITIONS'}" />
		<div class="content-title">
			<h3 class="pull-left" id="datasetNameElement">
				<c:out value="${datasetName}" />
			</h3>
			<ul class="nav nav-tabs actions pull-right">
				<li <c:if test="${showTable}"> class="active"</c:if>><a
					href="#content-table" data-toggle="tab" id="tab-content"
					title="Show input data in tabular form."> <span
						class="glyphicon glyphicon-th"></span> Datatable
				</a></li>
				<li <c:if test="${showScatter}"> class="active"</c:if>><a
					href="#content-point-cloud" data-toggle="tab"
					title="Show scatter plot of data using two metric dimensions of choice."
					id="tab-point-cloud"> <span class="glyphicon glyphicon-picture"></span>
						Scatter plot
				</a></li>
				<li <c:if test="${showPropositions}"> class="active"</c:if>><a
					href="#content-metadata"
					title="Show set of statements available for pattern discovery as basic data selectors."
					data-toggle="tab" id="tab-metadata"> <span
						class="glyphicon glyphicon-exclamation-sign"></span> Propositions
				</a></li>
			</ul>
			<div class="clearfix"></div>
		</div>
		<c:set var="tableClasses" scope="page"
			value="${defaultDataView=='TABLE'? 'tab-pane fade in active' : 'tab-pane fade'}" />
		<c:set var="scatterClasses" scope="page"
			value="${defaultDataView=='SCATTER'? 'tab-pane fade in active' : 'tab-pane fade'}" />
		<c:set var="propositionsClasses" scope="page"
			value="${defaultDataView=='PROPOSITIONS'? 'tab-pane fade in active' : 'tab-pane fade'}" />
		<div class="tab-content">
			<div class="${tableClasses}" id="content-table">
				<jsp:include page="../dashboard/_table.jsp" />
			</div>
			<div class="${scatterClasses}" id="content-point-cloud">
				<jsp:include page="../dashboard/_pointCloud.jsp" />
			</div>
			<div class="${propositionsClasses}" id="content-metadata">
				<jsp:include page="../dashboard/_metadata.jsp" />
			</div>
		</div>
	</div>
</div>