
LOCK TABLES `admin__options_parameters` WRITE;
INSERT INTO `admin__options_parameters` (`entry_id`, `param_name`, `param_value`) VALUES ('MainFrame','Navbar entries','[info]');
INSERT INTO `admin__options_parameters` (`entry_id`, `param_name`, `param_value`) VALUES ('MainFrame','Footer entries','[citations]');
INSERT INTO `admin__options_parameters` (`entry_id`, `param_name`, `param_value`) VALUES ('PageContainer','Primary logo','Optional[creedo_logo_horizontal_nomargin.png]'),('PageContainer','Primary logo link','https://bitbucket.org/realKD/creedo'),('PageContainer','Secondary logo','Optional.empty'),('PageContainer','Secondary logo link','');
INSERT INTO `admin__options_parameters` (`entry_id`, `param_name`, `param_value`) VALUES ('AccountRequestPage', 'Account request note', 'Provide your account credentials. Your account still has to be approved by a site administrator before it will become active. You will be notified via mail about the progress. Thank you.');
INSERT INTO `admin__options_parameters` (`entry_id`, `param_name`, `param_value`) VALUES ('IndexPage', 'Title', 'Welcome to Creedo');
INSERT INTO `admin__options_parameters` (`entry_id`, `param_name`, `param_value`) VALUES ('IndexPage', 'Content file name', 'Optional[creedo_index_content.static.html]');
INSERT INTO `admin__options_parameters` (`entry_id`, `param_name`, `param_value`) VALUES ('IndexPage', 'Login invitation', '<a href=\"login.htm\">Log in</a> to see demo analytics dashboards...');

UNLOCK TABLES;
