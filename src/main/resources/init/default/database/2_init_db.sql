
CREATE TABLE `admin__users`
(
  `id`                  VARCHAR(255) PRIMARY KEY NOT NULL,
  `content_class_name`  TEXT
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `admin__users_parameters` (
  `id`          INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `entry_id`  VARCHAR(255),
  `param_name`  TEXT,
  `param_value` TEXT,
  FOREIGN KEY (`entry_id`) REFERENCES `admin__users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


