CREATE TABLE IF NOT EXISTS `admin__options` (
  `id` varchar(255) NOT NULL,
  `content_class_name` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `admin__options_parameters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entry_id` varchar(255) DEFAULT NULL,
  `param_name` text,
  `param_value` text,
  PRIMARY KEY (`id`),
  KEY `entry_id` (`entry_id`),
  CONSTRAINT `admin__options_parameters_ibfk_1` FOREIGN KEY (`entry_id`) REFERENCES `admin__options` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

LOCK TABLES `admin__options` WRITE;
INSERT INTO `admin__options` (`id`, `content_class_name`) VALUES ('MainFrame','de.unibonn.creedo.boot.MainFrameOptions');
INSERT INTO `admin__options` (`id`, `content_class_name`) VALUES ('PageContainer','de.unibonn.creedo.ui.core.DefaultPageContainerOptions');
INSERT INTO `admin__options` (`id`, `content_class_name`) VALUES ('AccountRequestPage','de.unibonn.creedo.boot.AccountRequestPageOptions');
INSERT INTO `admin__options` (`id`, `content_class_name`) VALUES ('IndexPage','de.unibonn.creedo.boot.IndexPageOptions');
INSERT INTO `admin__options` (`id`, `content_class_name`) VALUES ('DefaultFrame','de.unibonn.creedo.ui.core.DefaultFrameOptions');
INSERT INTO `admin__options` (`id`, `content_class_name`) VALUES ('DashboardLinkBuilder','de.unibonn.creedo.ui.indexpage.DemoDashboardLinkOptions');
UNLOCK TABLES;
