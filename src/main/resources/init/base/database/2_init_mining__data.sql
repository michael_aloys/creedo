CREATE TABLE IF NOT EXISTS `mining__data`
(
  `id`                  VARCHAR(255) PRIMARY KEY NOT NULL,
  `content_class_name`  TEXT
);

CREATE TABLE `mining__data_parameters` (
  `id`          INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `entry_id`  VARCHAR(255),
  `param_name`  TEXT,
  `param_value` TEXT,
  FOREIGN KEY (`entry_id`) REFERENCES `mining__data` (`id`)
);
