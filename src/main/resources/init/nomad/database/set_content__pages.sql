INSERT INTO `content__pages` VALUES ('help_tutorial_home', 'de.unibonn.creedo.ui.ContentPageBuilder');

INSERT INTO `content__pages_parameters` (`entry_id`, `param_name`, `param_value`) VALUES
  ('help_tutorial_home', 'Title', 'Octet Binaries SGD Tutorial'),
  ('help_tutorial_home', 'Reference name', 'Help'),
  ('help_tutorial_home', 'Content file name', 'content_help_tutorial_home.html');
  