INSERT INTO `mining__data` (`id`,`content_class_name`) VALUES ('binaries_tutorial', 'de.unibonn.creedo.boot.FixedComponentWorkspaceBuilder');

INSERT INTO `mining__data_parameters` (`entry_id`,`param_name`,`param_value`) VALUES ('binaries_tutorial', 'Table name', 'Octet binaries');
INSERT INTO `mining__data_parameters` (`entry_id`,`param_name`,`param_value`) VALUES ('binaries_tutorial', 'Table description', '<p>Dataset of 82 octet binary semi-conductors described by free atom properties of components and annotated with information on energy difference between rocksalt and zincblende crystal structures.</p><br/><p><b>Source:</b> adapted from Ghiringhelli, Luca M., et al. "Big data of materials science: Critical role of the descriptor." Physical review letters 114.10 (2015): 105503</p>');
INSERT INTO `mining__data_parameters` (`entry_id`,`param_name`,`param_value`) VALUES ('binaries_tutorial', 'Data file', 'data_octed_binaries_tutorial.csv');
INSERT INTO `mining__data_parameters` (`entry_id`,`param_name`,`param_value`) VALUES ('binaries_tutorial', 'Attributes file', 'data_octed_binaries_tutorial_attributes.csv');
INSERT INTO `mining__data_parameters` (`entry_id`,`param_name`,`param_value`) VALUES ('binaries_tutorial', 'Groups file', 'data_octed_binaries_tutorial_groups.csv');
INSERT INTO `mining__data_parameters` (`entry_id`,`param_name`,`param_value`) VALUES ('binaries_tutorial', 'Delimeter', ',');
INSERT INTO `mining__data_parameters` (`entry_id`,`param_name`,`param_value`) VALUES ('binaries_tutorial', 'Missing code', '?');
INSERT INTO `mining__data_parameters` (`entry_id`,`param_name`,`param_value`) VALUES ('binaries_tutorial', 'Group mappers', '[CONSECUTIVE_CHANGE_ATTRIBUTES, DISTRIBUTION_MODE, DISTRIBUTION_SHAPE, DISTRIBUTION_MEAN]');
INSERT INTO `mining__data_parameters` (`entry_id`,`param_name`,`param_value`) VALUES ('binaries_tutorial', 'Attribute mappers', '[CATEGORIC_EQUALiTY, SMART_DISCRETE_ORDINAL, POSITIVE_AND_NEGATIVE, YEAR_MONTH_DATE_HOUR, CLUSTERING_18_CUTOFFS]');
