
LOCK TABLES `admin__options_parameters` WRITE;
INSERT INTO `admin__options_parameters` (`entry_id`, `param_name`, `param_value`) VALUES ('PageContainer','Primary logo','Optional[NOMAD_Logo2.png]'),('PageContainer','Primary logo link','https://nomad-coe.eu/'),('PageContainer','Secondary logo','Optional.empty'),('PageContainer','Secondary logo link','');
INSERT INTO `admin__options_parameters` (`entry_id`, `param_name`, `param_value`) VALUES ('IndexPage', 'Title', 'NOMAD - Subgroup Discovery Toolkit');
INSERT INTO `admin__options_parameters` (`entry_id`, `param_name`, `param_value`) VALUES ('IndexPage', 'Content file name', 'Optional[content_index_page.html]');
INSERT INTO `admin__options_parameters` (`entry_id`, `param_name`, `param_value`) VALUES ('DefaultFrame', 'Custom css', 'Optional[nomad.css]');
INSERT INTO `admin__options_parameters` (`entry_id`, `param_name`, `param_value`) VALUES ('DashboardLinkBuilder', 'Build copy on open', 'false');
UNLOCK TABLES;
