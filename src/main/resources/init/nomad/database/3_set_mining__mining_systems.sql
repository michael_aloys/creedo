INSERT INTO `mining__mining_systems` (`id`,`content_class_name`) VALUES ('nomad_sgd_binaries_tutorial_system','de.unibonn.creedo.webapp.dashboard.mining.ManualMiningSystemBuilder');

INSERT INTO `mining__mining_systems_parameters` (`entry_id`,`param_name`,`param_value`) VALUES ('nomad_sgd_binaries_tutorial_system','Algorithms','[NOMAD_OCTED_BINARY_TUTORIAL_EMM_MINER, NOMAD_OCTED_BINARY_TUTORIAL_PMM_MINER]');
INSERT INTO `mining__mining_systems_parameters` (`entry_id`,`param_name`,`param_value`) VALUES ('nomad_sgd_binaries_tutorial_system','Help pages','[help_tutorial_home]');
INSERT INTO `mining__mining_systems_parameters` (`entry_id`,`param_name`,`param_value`) VALUES ('nomad_sgd_binaries_tutorial_system','Post processor','Default ranker');
INSERT INTO `mining__mining_systems_parameters` (`entry_id`,`param_name`,`param_value`) VALUES ('nomad_sgd_binaries_tutorial_system','Default data view','SCATTER');
INSERT INTO `mining__mining_systems_parameters` (`entry_id`,`param_name`,`param_value`) VALUES ('nomad_sgd_binaries_tutorial_system','PCA as scatter default','false');
