INSERT INTO `content__demos` VALUES ('octed_binaries_tutorial', 'de.unibonn.creedo.ui.indexpage.DashboardLinkBuilder');
    
INSERT INTO `content__demos_parameters` (`entry_id`, `param_name`, `param_value`) VALUES
  ('octed_binaries_tutorial', 'Title', 'Octet Binary Crystal Structure Characterization'),
  ('octed_binaries_tutorial', 'Description', 'Use subgroup discovery to find descriptors for predicting the formation of rocksalt or zinkblende crystal structures.'),
  ('octed_binaries_tutorial', 'Image', 'octed_binaries_subgroup.png'),
  ('octed_binaries_tutorial', 'Image credits', 'Image: Ghiringhelli et al., 2015'),
  ('octed_binaries_tutorial', 'User groups', '[REGISTERED, ANONYMOUS]'),
  ('octed_binaries_tutorial', 'Analytics dashboard', 'nomad_sgd_binaries_tutorial_system'),
  ('octed_binaries_tutorial', 'Data', 'binaries_tutorial');
  